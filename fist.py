
    # let's take cisco
    # access-list bla tcp permit host 10.1.1.1 host 10.2.2.2 sq 22
    # access-list bla ip deny any any
    # access-group bla interface eth2 in
    # access-group bla interface eth1 in
    # access-group bla interface eth0 out
    #
    # rule1 = FirewallRule(bla, MatchRule(10.1.1.1/32, 10.2.2.2/32, TCP, SSH), Action(PERMIT))
    # rule2 = FirewallRule(bla, MatchRule(ANY, ANY, IP), Action(Deny))
    # FirewallRuleSet(None, bla, from_object = FromObject([eth1, eth2]), to_object = ToObject([eth0]),
    # rules = [rule1, rule2]])
    #

    # lets take Juniper:
    # security-zone inside: eth1, eth2
    # security-zone moj_kancel: eth0
    # from inside to moj_kancel:
    #     policy1 do_kanclu_pusti: match: source_address 10.1.1.1, destination_address 10.2.2.2,
    #     application ssh, action: permit
    #     policy2 inych_nepusti: match: source_address any, destination_address any, action: reject
    #
    # rule1 = FirewallRule(do_kanclu_pusti, "from inside to moj_kancel", MatchRule(10.1.1.1/32, 10.2.2.2/32, TCP, SSH),
    # Action(PERMIT))
    # rule2 = FirewallRule(inych_nepusti, "from inside to moj_kancel", MatchRule(ANY, ANY, IP), Action(Deny))
    # FirewallRuleSet("from inside to moj_kancel", from_object = FromObject([eth1, eth2]), to_object = ToObject([eth0]),
    # rules = [rule1, rule2]])

# ############# Setup args and argparser ##############
import json

from Implementation.firewall_juniper_parser import FirewallParserJuniper
from Implementation.firewall_cisco_parser import FirewallParserCisco
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.helper_functions import get_firewall_type, FirewallConfigurator, \
    FirewallTypes, AnsibleHandlePlay, open_file_as_read, parsing_parameters, define_logger, \
    update_logger
from Implementation.perun_parser import PerunParser

arg_parser, args = parsing_parameters()
log_file = args.l
logger, console_handler = define_logger(log_file)
update_logger(arg_parser, args, console_handler, logger)


def parse_firewall(firewall_configurator, logger, call_directly=False):
    """
    Parse security configuration from firewall.
    1) Get firewall type.
    2) Create specific FirewallParserBase (FirewallParserJuniper/FirewallParserCisco).
    3) Call method FirewallParserBase.parse() with current firewall configuration.
    """
    global_type, firewall_parser, rule_sets, working_dir = None, None, None, None
    """ args, arg_parser = None, None
    # TODO toto je trosku hack...to potom prerobime
    log_file = None
    if not call_directly:
        arg_parser, args = parsing_parameters()
        perun_input_file_path = args.r
        log_file = args.l"""
    exec_dir = "Implementation/manage-fw/"
    global_type = get_firewall_type(exec_dir)
    working_dir = "/tmp/perun-testing/"
    ansible_firewall_configurator = FirewallConfigurator()
    logger.info(str(global_type))
    if global_type == FirewallTypes.srx:
        firewall_configurator.ansible_handle(exec_dir, tags=AnsibleHandlePlay.get_srx.name)
        whole_config = open_file_as_read(working_dir + "addr-book.json", logger)
        print(json.dumps(whole_config, indent=4, sort_keys=False))
        route_info = open_file_as_read(working_dir + "route-info.json", logger)
        firewall_parser = FirewallParserJuniper(whole_config[0]["rpc-reply"]["configuration"]["security"]["policies"],
                                                whole_config[0]["rpc-reply"]["configuration"]["security"]["zones"],
                                                whole_config[0]["rpc-reply"]["configuration"]["applications"],
                                                whole_config[0]["rpc-reply"]["configuration"]["security"]["address-book"],
                                                whole_config[0]["rpc-reply"]["configuration"]["interfaces"]["interface"],
                                                route_info,
                                                exec_dir)
        firewall_parser.parse()
    elif global_type == FirewallTypes.asa:
        firewall_configurator.ansible_handle(exec_dir, tags=AnsibleHandlePlay.get_asa.name)
        firewall_parser = FirewallParserCisco(open_file_as_read(working_dir + "policies.json", logger),
                                              open_file_as_read(working_dir + "addr-book.json", logger),
                                              open_file_as_read(working_dir + "route-info.json", logger),
                                              open_file_as_read(working_dir + "zones.json", logger),exec_dir)
        firewall_parser.parse()

    return global_type, firewall_parser


def parse_perun(zone_objects, args):
    """
    Parse rules from Perun.
    :param zone_objects list of ZoneObject
    :param args fist arguments
    """
    with open(args.r, "r") as json_file:
        perun_json = json.load(json_file)
    perun_parser = PerunParser(perun_json, zone_objects)
    perun_parser.parse()
    return perun_parser


def parse(firewall_configurator, args, logger):
    global_type, firewall_parser = parse_firewall(firewall_configurator, logger)
    perun_parser = parse_perun(firewall_parser._zones_objects, args)
    return global_type, firewall_parser, perun_parser


def merge(firewall_parser, perun_parser, args):
    """
    Merge perun rules to firewall rules. Depends on optimization level from args.
    :param firewall_configurator: FirewallConfigurator
    :param firewall_parser: FirewallParserBase
    :param perun_parser: PerunParser
    :param global_type: FirewallTypes
    :param args: fist arguments
    :param logger: Logger
    """
    merger = RuleSetMerger()
    if args.o == 0:
        merged_rule_set, merged_application_holder, merged_net_objects_holder = \
            merger.merge_rule_sets_all(firewall_parser.parsed_rule_sets,
                                       perun_parser.rule_sets,
                                       firewall_parser.applications_holder,
                                       firewall_parser.net_object_holder,
                                       perun_parser.net_object_holder)
        return merged_rule_set, merged_application_holder, merged_net_objects_holder
    elif args.o == 1:
        merged_rule_set, merged_application_holder, merged_net_objects_holder = \
            merger.merge_rule_sets_perun(firewall_parser.parsed_rule_sets,
                                       perun_parser.rule_sets,
                                       firewall_parser.applications_holder,
                                       firewall_parser.net_object_holder,
                                       perun_parser.net_object_holder)
        return merged_rule_set, merged_application_holder, merged_net_objects_holder
    return firewall_parser.parsed_rule_sets, firewall_parser.applications_holder, firewall_parser.net_object_holder,


def deploy(firewall_configurator, firewall_parser, rule_sets, applications, net_objects, args):
    """
    Create new settings files.
    :param firewall_configurator: FirewallConfigurator
    :param firewall_parser: FirewallParserBase
    :param rule_sets: list of merged rule sets (FirewallRuleSet)
    :param applications: ApplicationHolder
    :param net_objects: NetObjectHolder
    """
    if args.o == 0:
        if global_type == FirewallTypes.srx:
            firewall_configurator.ansible_handle(firewall_parser.exec_dir,
                                                 tags=AnsibleHandlePlay.clear_srx.name)
        elif global_type == FirewallTypes.asa:
            firewall_configurator.ansible_handle(firewall_parser.exec_dir,
                                                 tags=AnsibleHandlePlay.clear_asa.name)
    firewall_parser.deploy_policies(rule_sets)
    firewall_parser.deploy_services(applications)
    firewall_parser.deploy_net_objects(net_objects)
    firewall_parser.deploy_remote_hard(firewall_configurator)


if __name__ == '__main__':
    """
    Whole FIST pipeline:
    1) Parse the firewall configuration.
    2) Parse the rules from Perun.
    3) Merge the perun rules to the firewall rule sets.
    4) Deploy the new configuration to the firewall.
    """
    firewall_configurator = FirewallConfigurator()
    global_type, firewall_parser, perun_parser = parse(firewall_configurator, args, logger)
    merged_rule_sets, merged_applications, merged_net_objects = \
        merge(firewall_parser, perun_parser, args)
    deploy(firewall_configurator, firewall_parser, merged_rule_sets, merged_applications, merged_net_objects, args)
