Systém FIST byl implementován v programovacím jazyce Python ve verzi 3.8.5 ve spojení s platformou pro konfigurační správu a řízení síťových prvků - Ansible a to ve verzi 2.9.6. Zpravování vstupných dat ze systému pro správu identit Perun, vyhodnocení aktuálních firewallových pravidel a následní vygenerovaní nových pravidel pro firewall bylo implementováno právě v jazyce Python. Načítaní aktuálních pravidel z firewallu a následná konfigurace nových pravidel bylo zabezpečováno platformou Ansible. 

Implementační část v jazyce Python byla navržena s ohledem na objektově-orientované principy programovaní tak, že byla zložená z nasledujúcich častí: 
- načítaní/zapisování firewall konfigurace (Cisco/Juniper) do/z abstraktních objektových struktur 
- - firewall_cisco_parser, firewall_juniper_parser 
- načítaní vstupu z Perunu do abstraktních objektových struktur 
- - perun_parser 
- Zapracovaní množiny abstraktních firewallových pravidel (z Perunu) do druhé množiny abstraktních firewallových pravidel (z aktuálního stavu pravidel firewallu) 
- - firewall_rule_set_ merger 

Implementace byla řízená DevOps metodikami. Byla zřízená CI/CD pipeline, která při každým commitu spustila jednotkové testování (unit testing) a funkční a integrační testování (functional&integration testing). 

**Využitie systému**

Systém dokáže pridávať a odstráňovať prístupy k sieťovým zdrojom na základe vstupných dát, ktoré špecifikujú uživateľov (IP adresy) a sieťové zdroje (IP adresy + protokol + sieťový port). Systém dokáže pracovať s firewallom Cisco ASA a Juniper SRX s možnosťou ďalšieho rozšírenia.

Keďže systém zaručuje správnosť a konzistentnosť konfigurácie, je možné systém rozšíriť o funkciu kontroly pravidiel na firewalle, detekcie stavu pravidiel pre daného uživateľa/službu alebo preklad pravidiel z jedného typu firewallu do druhého.

**Spustenie systému FIST**

Na servere musí byť nainštalovaný python3 s knižnicami: netaddr, pyyaml, coverage, ansible, paramiko, junos-eznc, jxmlease. Ďalej musí byť nainštalovaný Ansible vo verzii 2.9.6 s ansible modulom Juniper.junos a Cisco.asa.

ubuntu@ubuntu:~$ python3 fist.py -r <perun_input_file.json> -o 0|1

-o prepínač rozhoduje o vnútornom fungovaní FISTu:
- 0 - FIST vygeneruje **celú** konfiguráciu a tú predchádzajúcu z firewallu vymaže a novú nakonfiguruje
- 1 - FIST vygeneruje len **rozdielovú** konfiguráciu, ktorú vkladá do existujúcej konfigurácie na firewalle

Vstupný súbor s informáciami o nových pravidlách z Perunu je v tomto formáte (JSON):
{
   "rules" : [
      {
         "allowedIPs" : [
            "10.1.1.2",
            ...
         ],
         "rule" : "147.251.22.212:22,tcp"
      },
      ...
   ]
}


Implementačná dokumentácia je tu:
https://docs.it.muni.cz/pages/viewpage.action?pageId=17760318

Programátori:
1. Michaela Miškovská - už neaktívna
2. Nikol Hunkařová - nikolhunkarova@gmail.com
3. Ondrej Lošťák - 485162@muni.cz
4. Vojtěch Kovářík - 485151@muni.cz
5. Martin Tuleja - už neaktívny
