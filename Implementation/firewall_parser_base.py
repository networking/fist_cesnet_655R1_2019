"""
FirewallParser interface
"""
from Implementation.network_object_holder import NetObjectHolder
from Implementation.service_holder import ApplicationHolder


class FirewallParserBase(object):
    """
    Interface for creating FirewallParsers.
    """
    def __init__(self, policies, address_book, zones, exec_dir):
        self._policies = policies
        self._address_book = address_book
        self._zones = zones

        self.net_object_holder = NetObjectHolder()
        self.applications_holder = ApplicationHolder()
        self.parsed_rule_sets = []
        self.exec_dir = exec_dir

    def parse(self):
        """
        Parse firewall information to abstract structure.
        """

    def abstract_to_address_book(self, net_object_holder):
        """
        From abstract get back data about address_book in firewall format.
        :param net_object_holder: net objects in abstract structure
        """

    def abstract_to_policies(self, rule_sets):
        """
        From abstract get back data about policies in firewall format.
        :param rule_sets: rule sets in abstract structure
        """

    def deploy_policies(self, rule_sets):
        """
        Create file with commands for configure policies on firewall.
        :param rule_sets: policies in abstract structure
        """

    def deploy_services(self, applications_holder):
        """
        Create file with commands for configure services on firewall.
        :param applications_holder: services in abstract structure
        """

    def deploy_net_objects(self, net_object_holder):
        """
        Create file with commands for configure objects on firewall.
        """

    def deploy_remote_hard(self, firewall_configurator):
        """
        Run ansible to configure firewall.
        :param firewall_configurator: FirewallConfigurator
        """
