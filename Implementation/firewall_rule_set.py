from ipaddress import IPv4Interface
import netaddr
from Implementation.enums import AnyIP


class IPAddress(object):
    """Represents single IP address or AnyIP."""
    def __init__(self, ip_address=AnyIP.ANYIP):
        """
        :param ip_address: AnyIP/IPv4Interface
        """
        if ip_address == AnyIP.ANYIP.value:
            self.ip_address = AnyIP.ANYIP
        else:
            self.ip_address = IPv4Interface(ip_address)

    def __str__(self):
        if self.ip_address == AnyIP.ANYIP:
            return str(AnyIP.ANYIP.value)
        return str(self.ip_address)

    def __eq__(self, other):
        if not isinstance(other, IPAddress):
            return False
        return self.ip_address == other.ip_address

    def __hash__(self):
        return self.ip_address.__hash__()

    def get_str_with_mask(self):
        return str(self.ip_address)

    def get_str_without_mask(self):
        return str(self.ip_address.ip)

    def is_string_ip_match(self, ip_address_string):
        if self.ip_address == AnyIP.ANYIP:
            return True
        return netaddr.IPAddress(ip_address_string) in netaddr.IPNetwork(self.get_str_with_mask())

    def is_ip_object_match(self, ip_address_object):
        if self.ip_address == AnyIP.ANYIP:
            return True
        return netaddr.IPAddress(ip_address_object.get_str_without_mask()) in netaddr.IPNetwork(self.get_str_with_mask())


# collection of rules from somewhere to somewhere (from zone1 to zone2 or from interface1
# to interface2)
# For cisco it is one access-control-list. For Juniper it is from zone to zone policy collection
class FirewallRuleSet(object):
    """Main data object keeping everything about one rule set."""
    def __init__(self, rule_set_name, from_object, to_object, rules, rule_type):
        """
        :param rule_set_name: string
        :param from_object: Object/None
        :param to_object: Object/None
        :param rules: list of FirewallRule
        :param rule_type: RuleType enum(JUNIPER, CISCO, NEW_PERUN, OLD_PERUN)
        """
        self.rule_set_name = rule_set_name
        self.from_object = from_object
        self.to_object = to_object
        self.rules = rules
        self.to_delete = []
        self.rule_type = rule_type

    def __str__(self):
        return " ".join(["Name:", self.rule_set_name, "From object:", str(self.from_object),
                         "To object:", str(self.to_object), "Rule type:", str(self.rule_type)])

    def delete_rule(self, rule):
        self.rules.remove(rule)
        self.to_delete.append(rule)


class ZoneObject(object):
    def __init__(self, name, ips):
        """
        :param name: string
        :param ips: list of IPAddresses
        """
        self.name = name
        self.ips = ips

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if not isinstance(other, ZoneObject):
            return False
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()

    @staticmethod
    def is_address_in_network(ip_address, net):
        ipaddr = int(''.join(['%02x' % int(x) for x in ip_address.split('.')]), 16)
        netstr, bits = net.split('/')
        netaddr = int(''.join(['%02x' % int(x) for x in netstr.split('.')]), 16)
        mask = (0xffffffff << (32 - int(bits))) & 0xffffffff
        return (ipaddr & mask) == (netaddr & mask)


class Interface(object):
    def __init__(self, interface_name, zone=None, ip_address=None):
        """
        :param interface_name: string
        :param zone: string
        :param ip_address: None
        """
        self.interface_name = interface_name
        self.zone = zone
        self.ip_address = ip_address

    def __str__(self):
        zone_str = self.zone or ''
        ip_str = self.ip_address or ''
        return "Interface " + self.interface_name + " zone " + \
               zone_str + " ip address " + ip_str


# One firewall rule has name that is referencing to the parent object - FirewallRuleSet
# then has match_rules and action. For cisco it is just one action.
# For Juniper it is possible to set multiple actions, but only one controls the traffic
# (rule permit, deny, ....).
# Other rules are either for route manipulation
# (I do not know if we have this in UVT) and other are logging rules (the same as "remark" in Cisco)
# https://www.juniper.net/documentation/en_US/junos/topics/usagoutsidee-guidelines/policy-configuring-actions-in-routing-policy-terms.html
class FirewallRule(object):
    def __init__(self, rule_name, rule_set_name, match_rule, action, description_list=None, rule_type=None):
        """
        :param rule_name: string
        :param rule_set_name: string
        :param match_rule: MatchRule
        :param action: Action
        :param description_list: list of descriptions in juniper and remarks in cisco
        :param rule_type: RuleType enum(JUNIPER, CISCO, NEW_PERUN, OLD_PERUN)
        """
        self.rule_name = rule_name
        self.rule_set_name = rule_set_name
        self.match_rule = match_rule
        self.action = action
        if description_list is None:
            self.description_list = []
        else:
            self.description_list = description_list
        self.rule_type = rule_type

    def __str__(self):
        return "Rule " + self.rule_name + " " + self.rule_set_name + " " + \
               str(self.match_rule) + " " + str(self.action)

    def __eq__(self, other):
        if not isinstance(other, FirewallRule):
            return False
        return self.action == other.action and self.match_rule == other.match_rule


class MatchRule(object):
    def __init__(self, src_net_obj_name, dst_net_obj_name, application):
        """
        :param src_net_obj_name: string
        :param dst_net_obj_name: string
        """
        self.src_net_obj_name = src_net_obj_name
        self.dst_net_obj_name = dst_net_obj_name
        self.application = application

    def is_match(self, src_net_obj, dst_net_obj):
        return self.src_net_obj_name == src_net_obj and self.dst_net_obj_name == dst_net_obj

    def __str__(self):
        return "Match " + str(self.src_net_obj_name or '') + " " + str(self.dst_net_obj_name or '') + \
               " " + str(self.application.protocol.name or '') + " " + str(self.application.protocol.name or '')

    def __eq__(self, other):
        if not isinstance(other, MatchRule):
            return False
        if self.application != other.application:
            return False
        if isinstance(self.src_net_obj_name, dict) and isinstance(other.src_net_obj_name, dict):
            if self.src_net_obj_name.keys() != other.src_net_obj_name.keys():
                return False
        elif self.src_net_obj_name != other.src_net_obj_name:
            return False
        if isinstance(self.dst_net_obj_name, dict) and isinstance(other.dst_net_obj_name, dict):
            if self.dst_net_obj_name.keys() != other.dst_net_obj_name.keys():
                return False
        elif self.dst_net_obj_name != other.dst_net_obj_name:
            return False
        return True


class JuniperLog(object):
    """Class holding juniper logs. It is kept in Action other_functionality."""
    def __init__(self, logs):
        """
        :param logs: list of string
        """
        self.logs = logs

    def juniper_dict(self):
        to_return = {}
        for log in self.logs:
            to_return[str(log)] = ''
        return to_return


class Action(object):
    def __init__(self, action, other_functionality=None):
        """
        :param action: enum ActionsEnum
        :param other_functionality: None/Actions.LOG/JuniperLog
        """
        self.action = action
        self.other_functionality = other_functionality

    def __str__(self):
        return str(self.action)

    def juniper_dict(self):
        to_return = {}
        if isinstance(self.other_functionality, JuniperLog):
            to_return['log'] = self.other_functionality.juniper_dict()
        to_return[str(self.action.value)] = ''
        return to_return

    def __eq__(self, other):
        if not isinstance(other, Action):
            return False
        return self.juniper_dict() == other.juniper_dict()


def get_address(net_object):
    """
    Get ip address in string from dst_net_obj or src_net_obj.
    :param net_object: AnyIp/dict {name: NetObject}
    return ip address string
    """
    if net_object == AnyIP.ANYIP:
        return str(AnyIP.ANYIP.value)
    return str(net_object.address)
