from enum import Enum


class AnyIP(str, Enum):
    ANYIP = "any"
    HOST = "host"

    def __str__(self):
        return self.value


# Cisco has Remark which is used as a note
# Juniper has next-term and next-policy which are used to as continue in for cycle
# I do not know what to do about them
class ActionsEnum(Enum):
    PERMIT = "permit"
    DENY = "deny"
    LOG = "log"
    ECHO = "echo"
    REJECT = "reject"

    def __str__(self):
        return str(self.value)


class TypeOfACL(str, Enum):
    EXTENDED = "extended"
    REMARK = "remark"


class Direction(str, Enum):
    IN = 'in'
    OUT = 'out'


class RuleType(str, Enum):
    NEW_PERUN = "new_perun"
    OLD_PERUN = "old_perun"
    CISCO = "cisco"
    JUNIPER = "juniper"
