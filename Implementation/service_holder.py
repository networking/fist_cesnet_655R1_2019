import string
from enum import Enum


class Application(object):
    def __init__(self, name, protocol, service_protocol, source_port=None):
        """
        :param name: string
        :param protocol: ServiceNode network or transport protocol (3. or 4. layer)
        :param service_protocol: ServiceNode service protocol
        :param protocol: ServiceNode
        """
        self.name = name
        self.protocol = protocol
        self.service_protocol = service_protocol
        self.source_port = source_port

    def is_subset(self, other):
        if self.protocol.name == ServiceEnum.UNKNOWN or other.protocol.name == ServiceEnum.UNKNOWN:
            raise Exception("Cannot compare UNKNOWN applications")
        if self.__eq__(other):
            return True
        if self.name == "any" or self.protocol.name == ServiceEnum.ANY:
            return True
        if self.protocol.name == ServiceEnum.IP and \
            (self.service_protocol.name is None or self.service_protocol.name == ServiceEnum.ANY):
            return True
        if self.protocol != other.protocol:
            return False
        return self.protocol in other.service_protocol.parents

    def __eq__(self, other):
        if not isinstance(other, Application):
            return False
        return self.name == other.name and self.protocol == other.protocol and \
               self.service_protocol and other.service_protocol and self.source_port == other.source_port


class ApplicationGroup(object):
    """
    :param name: string
    :param applications: list of Application
    :param description: group description
    """
    def __init__(self, name, applications, description=None):
        self.name = name
        self.applications = applications
        self.description = description

    def __eq__(self, other):
        if not isinstance(other, ApplicationGroup):
            return False
        return self.name == other.name and self.applications == other.applications and \
               self.description == other.description

    def is_subset(self, other):
        return False


class ApplicationHolder(object):
    def __init__(self, application_dict=None):
        """
        :param application_dict: dictionary {<application_name>: Application}
        """
        any = service_holder.find_by_name("any")
        self.application_dict = {'any': Application("any", any, any)}
        self.application_groups_dict = {}
        if application_dict is not None:
            self.application_dict = application_dict

    def find_application_by_protocol_and_port(self, protocol, port):
        for app_name, app in self.application_dict.items():
            if app.protocol == protocol and app.service_protocol == port:
                return app
        return None

    def find_application_by_name(self, application):
        if application in self.application_dict:
            return self.application_dict[application]
        else:
            return None

    def find_application_group_by_name(self, application_group):
        if application_group in self.application_groups_dict:
            return self.application_groups_dict[application_group]
        else:
            return None

    def find_application_or_application_group_by_name(self, application):
        if application in self.application_dict:
            return self.application_dict[application]
        elif application in self.application_groups_dict:
            return self.application_groups_dict[application]
        else:
            return None

    def add_application(self, application):
        self.application_dict[application.name] = application

    def add_application_group(self, application):
        self.application_groups_dict[application.name] = application

    def remove_application(self, app_name):
        self.application_dict.pop(app_name, None)

    def remove_application_group(self, app_name):
        self.application_groups_dict.pop(app_name, None)

    def get_applications(self):
        return self.application_dict

    def get_applications_groups(self):
        return self.application_groups_dict

    def find_protocol_and_port(self, application_name):
        protocol = self.application_dict[application_name].protocol.name
        destination_port = self.application_dict[application_name].service_protocol
        source_port = self.application_dict[application_name].source_port
        return protocol, destination_port, source_port

    def find_source_port(self, application_name):
        port_number = self.application_dict[application_name].source_port
        return port_number


class FindBy(str, Enum):
    """
    Enum finding options.
    """
    PORT_NUMBER = "port_number"
    NAME = "name"


class ServiceEnum(str, Enum):
    """
    Enum services names.
    """
    IP = "IP"
    TCP_UDP = "tcp-udp"
    TCP = "TCP"
    UDP = "UDP"
    FTP_DATA = "FTP-data"
    FTP_CONTROL = "FTP-control"
    SSH = "SSH"
    TELNET = "Telnet"
    SMTP = "SMTP"
    DNS = "DNS"
    DHCP_SERVER = "DHCP-server"
    DHCP_CLIENT = "DHCP-client"
    TFTP = "TFTP"
    HTTP = "HTTP"
    KERBEROS = "Kerberos"
    POP2 = "POP2"
    POP3 = "POP4"
    SUNRPC = "sunrpc"
    NTP = "NTP"
    IMAP = "IMAP"
    SNMP = "SNMP"
    SNMPTRAP = "SNMPTRAP"
    BGP = "BGP"
    IMAPv3 = "IMAPv3"
    LDAP = "LDAP"
    HTTPS = "HTTPS"
    MICROSOFTDS = "MicrosoftDS"
    SMTPS = "SMTPS"
    IKE = "IKE"
    SYSLOG = "SYSLOG"
    IMAPS = "IMAPS"
    LDAPS = "LDAPS"
    ICMP = "ICMP"
    UNKNOWN = "Unknown"
    ANY = "Any"


class ServiceHolder:
    """
    Keep service structure. Hold root ServiceNode.
    """

    def __init__(self, root):
        self.root = root

    def find_by_name(self, name):
        return self.find_by_rec(FindBy.NAME, name, self.root)

    def find_by_port_number(self, port_number):
        service = self.find_by_rec(FindBy.PORT_NUMBER, port_number, self.root)
        if service is None:
            return ServiceNode(ServiceEnum.UNKNOWN, port_number)
        return service

    def find_by_rec(self, search_by, searched_service_node, service):
        if search_by == FindBy.NAME and service.name.value.lower() == searched_service_node.lower():
            return service
        if search_by == FindBy.PORT_NUMBER and service.port_number == int(searched_service_node):
            return service
        for child in service.get_children():
            result = self.find_by_rec(search_by, searched_service_node, child)
            if isinstance(result, ServiceNode):
                return result
        return None


def compare_service(expected_service, actual_service):
    if expected_service.isdigit():
        if actual_service.isdigit():
            return actual_service == expected_service
        else:
            return str(service_holder.find_by_name(actual_service)) == expected_service
    else:
        if not actual_service.isdigit():
            return actual_service == expected_service
        else:
            return str(service_holder.find_by_name(expected_service)) == actual_service


class ServiceNode:
    """
    Single service with name and port.
    """

    def __init__(self, name, port_number):
        self.name = name
        self.port_number = port_number
        self.parents = []
        self.children = []

    def __eq__(self, other):
        return (self is None and other is None) or (self is not None and other is not None) \
               and self.name == other.name and self.port_number == other.port_number

    def __str__(self):
        return self.name.value.lower()

    def get_name(self):
        return self.name

    def get_port_number(self):
        return self.port_number

    def get_parents(self):
        return self.parents

    def add_parent(self, parent):
        self.parents.append(parent)
        if self not in parent.get_children():
            parent.add_child(self)

    def get_children(self):
        return self.children

    def add_child(self, child):
        self.children.append(child)
        if self not in child.get_parents():
            child.add_parent(self)


any = ServiceNode(ServiceEnum.ANY, None)

ip = ServiceNode(ServiceEnum.IP, None)
any.add_child(ip)

icmp = ServiceNode(ServiceEnum.ICMP, None)
ip.add_child(icmp)

tcp_udp = ServiceNode(ServiceEnum.TCP_UDP, None)
ip.add_child(tcp_udp)

tcp = ServiceNode(ServiceEnum.TCP, None)
tcp_udp.add_child(tcp)

udp = ServiceNode(ServiceEnum.UDP, None)
tcp_udp.add_child(udp)


def create_service_leaf(name, port, parents):
    node = ServiceNode(name, port)
    for parent in parents:
        parent.add_child(node)


create_service_leaf(ServiceEnum.FTP_DATA, 20, [tcp])
create_service_leaf(ServiceEnum.FTP_CONTROL, 21, [tcp])
create_service_leaf(ServiceEnum.SSH, 22, [tcp])
create_service_leaf(ServiceEnum.TELNET, 23, [tcp])
create_service_leaf(ServiceEnum.SMTP, 25, [tcp])
create_service_leaf(ServiceEnum.DNS, 53, [tcp, udp])
create_service_leaf(ServiceEnum.DHCP_SERVER, 67, [udp])
create_service_leaf(ServiceEnum.DHCP_CLIENT, 68, [udp])
create_service_leaf(ServiceEnum.TFTP, 69, [udp])
create_service_leaf(ServiceEnum.HTTP, 80, [tcp])
create_service_leaf(ServiceEnum.KERBEROS, 88, [udp])
create_service_leaf(ServiceEnum.POP2, 109, [tcp])
create_service_leaf(ServiceEnum.POP3, 110, [tcp])
create_service_leaf(ServiceEnum.SUNRPC, 111, [tcp, udp])
create_service_leaf(ServiceEnum.NTP, 123, [udp])
create_service_leaf(ServiceEnum.IMAP, 143, [tcp])
create_service_leaf(ServiceEnum.SNMP, 161, [udp])
create_service_leaf(ServiceEnum.SNMPTRAP, 162, [udp])
create_service_leaf(ServiceEnum.BGP, 179, [tcp])
create_service_leaf(ServiceEnum.IMAPv3, 220, [tcp])
create_service_leaf(ServiceEnum.LDAP, 389, [tcp])
create_service_leaf(ServiceEnum.HTTPS, 443, [tcp])
create_service_leaf(ServiceEnum.MICROSOFTDS, 445, [tcp, udp])
create_service_leaf(ServiceEnum.SMTPS, 465, [tcp])
create_service_leaf(ServiceEnum.IKE, 500, [udp])
create_service_leaf(ServiceEnum.SYSLOG, 514, [udp])
create_service_leaf(ServiceEnum.IMAPS, 993, [tcp])
create_service_leaf(ServiceEnum.LDAPS, 636, [tcp])

service_holder = ServiceHolder(any)
