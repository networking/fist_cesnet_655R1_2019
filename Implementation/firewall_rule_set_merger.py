import copy
import re

from Implementation.enums import RuleType, ActionsEnum, AnyIP
from Implementation.firewall_rule_set import MatchRule, Action, FirewallRule
from Implementation.service_holder import Application


class RuleSetMerger(object):
    def find_similar_rule_set(self, rule_sets, rule_set):
        """
        Method for find similar rule_set according from and to zone.
        Searching is different for Juniper and Cisco, because Juniper rule set have to have both from and to zone.
        Cisco has only to_zone or from_zone so we have to check only one of them. First of all is
        checked whether exist ruleset with matching both (from zone and to zone), if isn't check
        rule set with matching to zone and after from zone.

        :param rule_sets: lis of original FirewallRuleSet
        :param rule_set: FirewallRuleSet for which we are searching similar one
        :return FirewallRuleSet/None
        """
        if rule_set.rule_type == RuleType.JUNIPER:
            return self.find_rule_set(rule_sets, rule_set.from_object, rule_set.to_object)
        elif rule_set.rule_type == RuleType.CISCO:
            if rule_set.to_object is not None:
                return self.find_rule_set_to_object(rule_sets, rule_set.to_object)
            return self.find_rule_set_from_object(rule_sets, rule_set.from_object)
        return None

    @staticmethod
    def find_rule_set(rule_sets, from_object, to_object):
        """
        For Juniper.
        :param rule_sets: list of original rule sets
        :param from_object: ZoneObject
        :param to_object: ZoneObject
        :return: matching rule set
        :rtype: FirewallRuleSet or None
        """
        for rule_set in rule_sets:
            if from_object == rule_set.from_object and to_object == rule_set.to_object:
                return rule_set
        return None

    @staticmethod
    def find_rule_set_from_object(rule_sets, from_object):
        """
        For Cisco.
        :param rule_sets: list of original rule sets
        :param from_object: ZoneObject
        :return: matching rule set
        :rtype: FirewallRuleSet or None
        """
        for rule_set in rule_sets:
            if rule_set.from_object is not None and from_object == rule_set.from_object:
                return rule_set
        return None

    @staticmethod
    def find_rule_set_to_object(rule_sets, to_object):
        """
        For Cisco.
        :param rule_sets: list of original rule sets
        :param to_object: ZoneObject
        :return: matching rule set
        :rtype: FirewallRuleSet or None
        """
        for rule_set in rule_sets:
            if rule_set.to_object is not None and to_object == rule_set.to_object:
                return rule_set
        return None

    def copy_rules_between_rule_sets(self, original_rule_set, rule_set_to_add, net_object_holder):
        """
        Copy rules from rule_set2 to rule_set1. Check duplicities and in case of duplicity raise Exception.

        :param original_rule_set: original FirewallRuleSet
        :param rule_set_to_add: new FirewallRuleSet
        """
        default_deny = original_rule_set.rules.pop()
        if default_deny.action.action != ActionsEnum.DENY or default_deny.match_rule.src_net_obj_name != AnyIP.ANYIP or \
                default_deny.match_rule.dst_net_obj_name != AnyIP.ANYIP:
            original_rule_set.rules.append(default_deny)
            default_deny = None
        match_rule = False
        for rule_to_add in rule_set_to_add.rules:
            if rule_to_add in original_rule_set.rules:
                for x in original_rule_set.rules:
                    if x == rule_to_add:
                        if x.rule_type == RuleType.OLD_PERUN and rule_to_add.rule_type == RuleType.NEW_PERUN:
                            match_rule = True
                        else:
                            print("MATCH " + str(x))
                            raise Exception("DUPLICITY")
            elif is_subset(rule_to_add, original_rule_set, net_object_holder):
                print("The rule " + str(rule_set_to_add) + " is already covered in " + str(original_rule_set))
                raise Exception("DUPLICITY")
            if not match_rule:
                rule_to_add.rule_set_name = original_rule_set.rule_set_name
                rule_to_add.rule_name = self.rename_rule(rule_to_add.rule_name, original_rule_set)
                print("Merging a new RULE " + str(rule_to_add) + " " + str(
                    len(original_rule_set.rules)))
                original_rule_set.rules.append(rule_to_add)
        if default_deny:
            original_rule_set.rules.append(default_deny)

    @staticmethod
    def delete_redundant_perun_rules(original_rule_set, rule_set_to_add):
        """
        Deletes perun rules, which are not used anymore
        (Perun rules which are in original rules but aren't in Perun rules).

        :param original_rule_set: FirewallRuleSet from firewall
        :param rule_set_to_add: FirewallRuleSet from identity system
        """
        for original_rule in original_rule_set.rules:
            remove_rule = True
            if original_rule.rule_type == RuleType.OLD_PERUN:
                for adding_rule in rule_set_to_add.rules:
                    if original_rule == adding_rule:
                        remove_rule = False
                        break
                if remove_rule:
                    original_rule_set.delete_rule(original_rule)

    @staticmethod
    def find_service(application_holder, application):
        """
        Finds similar application, when similar application doesn't exist creates new application
        and adds it to application holder.

        :param application_holder: ApplicationHolder
        :param application: Application
        """
        protocol = application.protocol
        port = application.service_protocol
        similar_application = application_holder.find_application_by_protocol_and_port(protocol,
                                                                                       port)
        if similar_application is not None:
            return similar_application
        application_holder.add_application(application)
        return application

    def reset_applications(self, rule_sets2, application_holder):
        """
        Resets applications by original applications and add new to application holder
        when doesn't exist.

        :param rule_sets2: list of FirewallRuleSet with new rules
        :param application_holder: original ApplicationHolder
        :return: modified ApplicationHolder
        """
        for rule_set in rule_sets2:
            for rule in rule_set.rules:
                application = rule.match_rule.application
                application = self.find_service(application_holder, application)
                rule.match_rule.application = application

    @staticmethod
    def find_net_object(net_object_holder1, net_object_holder2, searched_net_object):
        """
        Finds similar NetObject, when similar NetObject.

        :param net_object_holder1: NetObjectHolder
        :param net_object_holder2: NetObjectHolder
        :param searched_net_object: Service
        :return: NetObject or None
        """
        if searched_net_object in net_object_holder2.get_net_objects():
            net_obj = net_object_holder2.get_net_objects()[searched_net_object]
            address = net_obj.address
            return net_object_holder1.find_net_object_name_by_ip_address_objects(address)
        elif searched_net_object in net_object_holder2.get_net_object_groups():
            net_obj_group = net_object_holder2.find_net_obj_group(searched_net_object)
            net_object_names = net_obj_group.get_net_object_names()
            ips = []
            if len(net_object_names) == 1:
                address = net_object_holder2.get_net_objects()[net_object_names[0]].address
                net_object = net_object_holder1.find_net_object_name_by_ip_address_objects(address)
                if net_object is not None:
                    return net_object
            for net_object_name in net_object_names:
                ips.append(net_object_holder2.get_net_objects()[net_object_name].address)
            return net_object_holder1.find_net_object_group_name_by_ip_address_objects(ips)
        else:
            return None

    def reset_net_objects(self, rule_sets2, net_object_holder1, net_object_holder2):
        """
        Resets net objects by original net objects and add new to net object holder
        when doesn't exist.

        :param rule_sets2: list of FirewallRuleSet with new rules
        :param net_object_holder1: original NetObjectHolder
        :param net_object_holder2: NetObjectHolder from identity system
        :return: modified NetObjectHolder
        """
        for rule_set in rule_sets2:
            for rule in rule_set.rules:
                similar_dst_net_object = self.find_net_object(net_object_holder1,
                                                              net_object_holder2,
                                                              rule.match_rule.dst_net_obj_name)
                if similar_dst_net_object:
                    rule.match_rule.dst_net_obj_name = similar_dst_net_object
                else:
                    name = rule.match_rule.dst_net_obj_name
                    new_name = self.add_net_object_to_holder(name, net_object_holder1,
                                                             net_object_holder2)
                    rule.match_rule.dst_net_obj_name = new_name

                similar_src_net_object = self.find_net_object(net_object_holder1,
                                                              net_object_holder2,
                                                              rule.match_rule.src_net_obj_name)
                if similar_src_net_object:
                    rule.match_rule.src_net_obj_name = similar_src_net_object
                else:
                    name = rule.match_rule.src_net_obj_name
                    new_name = self.add_net_object_to_holder(name, net_object_holder1,
                                                             net_object_holder2)
                    rule.match_rule.src_net_obj_name = new_name

    def add_net_object_to_holder(self, name, net_obj_holder1, net_obj_holder2):
        """
        If similar net object doesn't exist in original NetObjectHolder, then will be created.
        Unique NetObject's and NetObjectGroup's name is important, so the objects are renamed.
        """
        if name in net_obj_holder2.get_net_objects():
            new_net_object = net_obj_holder2.get_net_objects()[name]
            if name in net_obj_holder1.get_net_objects():
                name = self.rename_net_object(name, net_obj_holder1)
                new_net_object.name = name
            net_obj_holder1.add_net_obj(name, new_net_object)
        elif name in net_obj_holder2.get_net_object_groups():
            new_net_object_group = net_obj_holder2.get_net_object_groups()[name]
            names = new_net_object_group.get_net_object_names()
            if len(names) == 1:
                return self.add_net_object_to_holder(names[0], net_obj_holder1, net_obj_holder2)
            if name in net_obj_holder1.get_net_object_groups():
                name = self.rename_net_object_group(name, net_obj_holder1)
                new_net_object_group.name = name
            for net_object_name in new_net_object_group.get_net_object_names():
                if net_object_name not in net_obj_holder1.get_net_objects():
                    net_object = net_obj_holder2.find_net_obj_by_name(net_object_name)
                    net_obj_holder1.add_net_obj(net_object_name, net_object)
            net_obj_holder1.add_net_obj_group(name, new_net_object_group.get_net_object_names())
        return name

    def rename_net_object(self, name, net_object_holder):
        """
        Suppose that the name contains a number. Find the number and increase it until, the new name
        is not already used. Method replaces only the last occurrence of the number.
        """
        numbers_in_name = re.findall(r'\d+', name)
        if len(numbers_in_name):
            number = int(numbers_in_name[0])
            while name in net_object_holder.get_net_objects():
                name, number = self.get_new_name(number, name)
        return name

    def rename_net_object_group(self, name, net_object_holder):
        """
        Suppose that the name contains a number. Find the number and increase it until, the new name
        is not already used. Method replaces only the last occurrence of the number.
        """
        numbers_in_name = re.findall(r'\d+', name)
        if len(numbers_in_name):
            number = int(numbers_in_name[0])
            while name in net_object_holder.get_net_object_groups():
                name, number = self.get_new_name(number, name)
        return name

    def rename_rule(self, name, rule_set):
        """
        Suppose that the name contains a number. Find the number and increase it until, the new name
        is not already used. Method replaces only the last occurrence of the number.
        """
        numbers_in_name = re.findall(r'\d+', name)
        rule_names = []
        for rule in rule_set.rules:
            rule_names.append(rule.rule_name)
        if len(numbers_in_name):
            number = int(numbers_in_name[0])
            while name in rule_names:
                name, number = self.get_new_name(number, name)
        return name

    @staticmethod
    def get_new_name(number, name):
        """
        Replace the last number occurrence in the name by increased number.
        """
        new_number = number + 1
        # replace the last occurrence of the number
        tmp = name.rsplit(str(number), 1)
        name = str(new_number).join(tmp)
        return name, new_number

    def merge_rule_sets_all(self, rule_sets1, rule_sets2, applications_holder, net_object_holder1,
                            net_object_holder2):
        """
        Run merging sequence:
        1) reset applications
        2) reset net objects
        3) merge rule sets

        :param rule_sets1: original FirewallRuleSet
        :param rule_sets2: new FirewallRuleSet
        :param applications_holder: original ApplicationHolder
        :param net_object_holder1: original NetObjectHolder
        :param net_object_holder2: identity system's NetObjectHolder
        :return: modified FirewallRuleSet, ApplicationHolder, NetObjectHolder
        """
        self.reset_applications(rule_sets2, applications_holder)
        self.reset_net_objects(rule_sets2, net_object_holder1, net_object_holder2)
        rule_type = self.get_rule_set_type(rule_sets1)
        for rule_set in rule_sets2:
            if rule_type == RuleType.JUNIPER:
                # Juniper firewall has always both from object and to object or
                # it is global rule set.
                matching_rule_set = self.find_rule_set(rule_sets1, rule_set.from_object,
                                                       rule_set.to_object)
                self.merge_rules(rule_sets1, matching_rule_set, copy.deepcopy(rule_set),
                                 net_object_holder1)
            elif rule_type == RuleType.CISCO:
                # Cisco has always only from object or to object,
                # so there are usually two matching rule sets.
                from_matching_rule_set = self.find_rule_set_from_object(rule_sets1,
                                                                        rule_set.from_object)
                from_rule_set = copy.deepcopy(rule_set)
                from_rule_set.to_object = None
                self.rename_rule_set(from_rule_set, "from_" + str(from_rule_set.from_object))
                self.merge_rules(rule_sets1, from_matching_rule_set, from_rule_set,
                                 net_object_holder1)
                # to object
                to_matching_rule_set = self.find_rule_set_to_object(rule_sets1, rule_set.to_object)
                to_rule_set = copy.deepcopy(rule_set)
                to_rule_set.from_object = None
                self.rename_rule_set(to_rule_set, "to_" + str(to_rule_set.to_object))
                self.merge_rules(rule_sets1, to_matching_rule_set, to_rule_set, net_object_holder1)
        return rule_sets1, applications_holder, net_object_holder1

    def merge_rule_sets_perun(self, rule_sets1, rule_sets2, applications_holder, net_object_holder1,
                              net_object_holder2):
        """
        Run merging sequence:
        1) reset applications
        2) reset net objects
        3) merge rule sets

        :param rule_sets1: original FirewallRuleSet
        :param rule_sets2: new FirewallRuleSet
        :param applications_holder: original ApplicationHolder
        :param net_object_holder1: original NetObjectHolder
        :return: modified FirewallRuleSet, ApplicationHolder, NetObjectHolder
        """
        original_rule_set = copy.deepcopy(rule_sets1)
        original_applications_holder = copy.deepcopy(applications_holder)
        original_net_object_holder = copy.deepcopy(net_object_holder1)
        new_rule_sets, new_application_holder, new_net_object_holder = \
            self.merge_rule_sets_all(original_rule_set, rule_sets2, applications_holder,
                                     net_object_holder1, net_object_holder2)
        diff_rule_sets = self.get_difference_rule_sets(rule_sets1, new_rule_sets)
        diff_app_holder = self.get_difference_app_holder(original_applications_holder,
                                                         new_application_holder)
        diff_net_obj_holder = self.get_difference_net_obj_holder(original_net_object_holder,
                                                                 new_net_object_holder)
        return diff_rule_sets, diff_app_holder, diff_net_obj_holder

    def get_difference_rule_sets(self, original_rule_sets, new_rule_sets):
        difference = []
        for rule_set in new_rule_sets:
            similar_rule_set = self.find_similar_rule_set(original_rule_sets, rule_set)
            if similar_rule_set is not None:
                rules = []
                for new_rule in rule_set.rules:
                    is_original_rule = False
                    for original_rule in similar_rule_set.rules:
                        if new_rule == original_rule:
                            is_original_rule = True
                            break
                    if not is_original_rule and new_rule.rule_type != RuleType.JUNIPER and \
                            new_rule.rule_type != RuleType.CISCO:
                        rules.append(new_rule)
                if len(rules) != 0:
                    rule_set.rules = rules
                    difference.append(rule_set)
            else:
                difference.append(rule_set)
        return difference

    @staticmethod
    def get_difference_app_holder(original_app_holder, new_app_holder):
        difference = copy.deepcopy(new_app_holder)
        for app_name, _ in new_app_holder.get_applications().items():
            similar_app = original_app_holder.find_application_by_name(app_name)
            if similar_app:
                difference.remove_application(app_name)
        for app_group_name, _ in new_app_holder.get_applications_groups().items():
            similar_app_group = original_app_holder.find_application_group_by_name(app_group_name)
            if similar_app_group:
                difference.remove_application_group(app_group_name)
        return difference

    @staticmethod
    def get_difference_net_obj_holder(original_net_obj_holder, new_net_obj_holder):
        difference = copy.deepcopy(new_net_obj_holder)
        for net_obj_name, net_obj in new_net_obj_holder.get_net_objects().items():
            similar_net_obj = \
                original_net_obj_holder.find_net_object_name_by_ip_address_objects(net_obj.address)
            if similar_net_obj:
                difference.remove_net_obj(net_obj_name)
        for net_obj_group_name, net_obj_group in new_net_obj_holder.get_net_object_groups().items():
            searched_ips = []
            for net_obj_name in net_obj_group.get_net_object_names():
                if new_net_obj_holder.find_net_obj_group(net_obj_name):
                    searched_ips.append(net_obj_name)
                else:
                    searched_ips.append(
                        new_net_obj_holder.find_net_obj_by_name(net_obj_name).address)
            similar_net_obj = \
                original_net_obj_holder.find_net_object_group_name_by_ip_address_objects(
                    searched_ips)
            if similar_net_obj:
                difference.remove_net_obj_group(net_obj_group_name)
        for tmp_net_obj_name, tmp_net_obj in new_net_obj_holder.get_tmp_net_objects().items():
            similar_tmp_net_obj = \
                original_net_obj_holder.find_tmp_net_obj_name_by_ip_address_object(
                    tmp_net_obj.address)
            if similar_tmp_net_obj:
                difference.remove_tmp_net_obj(tmp_net_obj_name)
        return difference

    def merge_rules(self, original_rule_sets, related_rule_set, rule_set_to_add, net_object_holder):
        if related_rule_set is None:
            rule_type = self.get_rule_set_type(original_rule_sets)
            rule_set_to_add.rule_type = rule_type
            for rule in rule_set_to_add.rules:
                rule.new_rule = True
            if not self.find_global_implicit_deny(original_rule_sets):
                self.add_implicit_deny(rule_set_to_add)
            original_rule_sets.append(rule_set_to_add)
        else:
            self.delete_redundant_perun_rules(related_rule_set, rule_set_to_add)
            self.copy_rules_between_rule_sets(related_rule_set, rule_set_to_add, net_object_holder)

    @staticmethod
    def find_global_implicit_deny(rule_sets):
        for rule_set in rule_sets:
            if rule_set.from_object is None and rule_set.to_object is None:
                for rule in rule_set.rules:
                    if (
                            rule.action.action == ActionsEnum.DENY or rule.action.action == ActionsEnum.REJECT) and \
                            rule.match_rule.src_net_obj_name == AnyIP.ANYIP and \
                            rule.match_rule.dst_net_obj_name == AnyIP.ANYIP and \
                            rule.match_rule.application.name == "any":
                        return True
        return False

    @staticmethod
    def get_rule_set_type(rule_sets):
        if rule_sets is not None and len(rule_sets) > 0:
            return rule_sets[0].rule_type
        return None

    @staticmethod
    def rename_rule_set(rule_set, name):
        rule_set.rule_set_name = name
        for rule in rule_set.rules:
            rule.rule_set_name = name

    @staticmethod
    def add_implicit_deny(rule_set):
        from Implementation.service_holder import service_holder
        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        action = Action(ActionsEnum.DENY)
        implicit_deny = FirewallRule("implicit_deny", rule_set.rule_set_name, match_rule, action)
        rule_set.rules.append(implicit_deny)


def is_subset(rule_to_add, original_rule_set, net_object_holder):
    for x in original_rule_set.rules:
        if is_match_rule_subset(x.match_rule, rule_to_add.match_rule, net_object_holder):
            if x.action.action != rule_to_add.action.action:
                raise Exception("Unable to add a rule. Rule set" + str(
                    original_rule_set) + " denies new Perun rule.")
            print("WE HAVE SUBSET")
            return True


def is_match_rule_subset(first_match_rule, second_match_rule, net_object_holder):
    if net_object_holder.is_net_objects_subset(first_match_rule.src_net_obj_name,
                                               second_match_rule.src_net_obj_name):
        if net_object_holder.is_net_objects_subset(first_match_rule.dst_net_obj_name,
                                                   second_match_rule.dst_net_obj_name):
            print("MATCH RULE SUBSET")
            return first_match_rule.application.is_subset(second_match_rule.application)
    return False
