from copy import deepcopy

import yaml
from Implementation.enums import Direction, TypeOfACL, AnyIP, RuleType, ActionsEnum
from Implementation.firewall_parser_base import FirewallParserBase
from Implementation.firewall_rule_set import FirewallRuleSet, Action, FirewallRule, MatchRule, \
    IPAddress, ZoneObject
from Implementation.helper_functions import netmask_to_slash, AnsibleHandlePlay
from Implementation.network_object_holder import NetObject, NetObjectGroup
from Implementation.service_holder import service_holder, ServiceEnum, Application, \
    ApplicationGroup


class FirewallParserCisco(FirewallParserBase):
    """
        Class for working witch Cisco Firewall.
        Parse rules to abstract classes structure.
        Write rules to output config files.
    """

    def __init__(self, cisco_policies, cisco_address_book, routes, zones, exec_dir):
        """
        :param cisco_policies: policies.json
        :param cisco_address_book: address-book.json
        :param routes: route-info.json
        :param zones: zones.json
        :param exec_dir: execution directory
        """
        super().__init__(policies=cisco_policies, address_book=cisco_address_book, zones=zones,
                         exec_dir=exec_dir)
        self._routes = routes
        self._zones_objects = []

    def parse(self):
        """
        Fills self.cisco_parsed_rule_set from parsed policies.json, address_book.json,
        route-info.json and zone.json.
        :return: list self.cisco_parsed_rule_set full of FirewallRuleSets
        """
        # make self.zone_objects
        self.__parse_routes()
        # make self.net_object_holder for parsed net_objects from address-book.json
        self.__parse_address_book()
        # make self.parsed_rule_sets from cisco_policies.json
        self.__parse_acls()
        return self.parsed_rule_sets

    def __parse_acls(self):
        """
        Parsing cisco_policies.json to make self.parsed_rule_sets dictionary.
        """
        # first list access_groups, second list access_list
        access_groups = self._policies[0]
        access_lists = self._policies[1]
        # every line (one line is one rule_set) in access_group
        for group in access_groups:
            # reset list rules
            rules = []
            # get information for filling FirewallRuleSet and other objects in it
            group_name, zone_name, group_direction = self.__parse_group(group)
            from_object, to_object = self.__get_from_to_object(zone_name,
                                                               group_direction)
            # make FirewallRule list from access_list lines
            remarks = []
            rule_type = RuleType.CISCO
            for acl in access_lists:
                rules, rule_type = self.__make_firewall_rules_list(group_name, acl.split(' '), rules, remarks, rule_type)

            # fill list self.cisco_parsed_rule_set with FirewallRuleSets
            self.parsed_rule_sets.append(FirewallRuleSet(rule_set_name=group_name,
                                                         from_object=from_object,
                                                         to_object=to_object,
                                                         rules=rules, rule_type=RuleType.CISCO))
        return self.parsed_rule_sets

    def __parse_group(self, group):
        """
        Gives parsed interface, name and direction.
        :param group: one line of access group
        :return: name of group, direction "in" or "out",
                 zone_name name of from/to zone
        """
        # split group into list of strings according to the words
        splited_group = group.split(' ')
        # if splited group has the required length
        if len(splited_group) == 5:
            # get required variables
            name = splited_group[1]
            direction = splited_group[2]
            zone_name = splited_group[4]
        # if splitted group hasen't the required length
        else:
            raise Exception("ACL is not correct!")

        return name, zone_name, direction

    def __get_from_to_object(self, zone_name, group_direction):
        """
        Gets Object from_object or Object to_object.
        :param group_interface: name of the interface in string
        :param group_direction: information obout direction, "in" or "out"
        :return: Object from_object if direction is "in", Object to_object if direction is "out"
        """
        from_object = None
        to_object = None
        # from_object is made if direction is "in"
        if group_direction == Direction.IN:
            from_object = self.__get_zone_by_name(zone_name)
        # to_object is made if direction is "out"
        elif group_direction == Direction.OUT:
            to_object = self.__get_zone_by_name(zone_name)
        return from_object, to_object

    def __get_zone_by_name(self, name):
        """
        Gets zone name by interface.
        :param name: name of zone
        :return: string with zone name, from second column of zones.json
        """
        for zone in self._zones_objects:
            if zone.name == name:
                return zone
        return None

    def __make_firewall_rules_list(self, group_name, splitted_list, rules, remarks, rule_type):
        """
        Makes FirewallRule list used in FirewallRuleSet parameter rules.
        :param group_name: name of the access_group in string
        :param rules: list of FirewallRules
        :param splitted_list: ACL splited into words
        :param remarks: list of remarks from acl
        :return: list of FirewallRules
        """

        # last element in ACL is gap, needs to be deleted
        last_element = splitted_list[-1]
        if last_element == '':
            splitted_list.pop()

        list_name = splitted_list[1]  # get name of ACL
        # if acl type is extended
        if splitted_list[2] == TypeOfACL.EXTENDED.value:
            # rules with the same name as FirewallRuleSet are made
            if list_name == group_name:
                # get required variables
                action = ActionsEnum(splitted_list[3])
                other_functionality = None
                if splitted_list[-1] == "log":
                    other_functionality = ActionsEnum.LOG
                elif splitted_list[-1] == "echo":
                    other_functionality = ActionsEnum.ECHO
                list_action = Action(action=action, other_functionality=other_functionality)
                protocol_name, source_name, destination_name, port_name = \
                    self.__get_protocol_src_dst_port_names(splitted_list)
                application = self.__get_application(protocol_name, port_name)
                # check if objects exists
                self.__check_net_object(destination_name)
                self.__check_net_object(source_name)

                # make match rule
                match_rules = MatchRule(src_net_obj_name=source_name,
                                        dst_net_obj_name=destination_name,
                                        application=application)

                if rule_type == RuleType.OLD_PERUN \
                        or "access-list " + group_name + " remark Perun generated rules will follow" in remarks:
                    rule_type = RuleType.OLD_PERUN
                if "access-list " + group_name + " remark Perun generated rules end" in remarks:
                    rule_type = RuleType.CISCO
                # fill list rules with FirewallRule
                rules.append(FirewallRule(rule_name=list_name, rule_set_name=group_name,
                                          match_rule=match_rules,
                                          action=list_action, description_list=remarks.copy(),
                                          rule_type=rule_type))
            remarks.clear()
        elif splitted_list[2] == TypeOfACL.REMARK.value:
            remark = " ".join(splitted_list)
            remarks.append(remark)
        else:
            raise Exception("Unsupported operation: " + splitted_list[2])
        return rules, rule_type

    def __check_net_object(self, name):
        """ Checks if NetObject exist or add them to temporary holder if they are in type of ip
        address
        :param name: NetObject or NetObjectGroup name
        :return: throw exeption on wrong name
        """
        if self.net_object_holder.find_net_obj_by_name(name) is None:
            if len(name.split(".")) == 4:
                self.net_object_holder.add_tmp_net_obj(
                    name, NetObject(name, IPAddress(name)))
            else:
                raise Exception("Network object " + name + " not found in configuration.")

    def __get_protocol_src_dst_port_names(self, splitted_group):
        """
        Get list of source name, destination name and service_name from ACL
        :param splitted_group: one line (acl) from access_group splitted by " "
                into individual words
        :return: protocol name, source name, destination name and port name
        """
        # This is position where protocol should be
        index = 4

        # get names from splitted group
        protocol_name, index = self.__acl_get_name_by_position(splitted_group, index)
        source_name, index = self.__acl_get_name_by_position(splitted_group, index)
        destination_name, index = self.__acl_get_name_by_position(splitted_group, index)
        port_name = None
        if len(splitted_group) > index:
            port_name, index = self.__acl_get_name_by_position(splitted_group, index)

        return protocol_name, source_name, destination_name, port_name

    def __acl_get_name_by_position(self, splitted_group, index):
        """
        Parsing acl to get source or destination ip.
        :param splitted_group: one line (acl) from access_group splitted by " "
                into individual words
        :param index: index of guessed name
        :return: name and expected position of next name
        """
        # get destination_ip from splitted_group line and known position
        name = splitted_group[index]
        # if index is "any" leave it this way
        if name == AnyIP.ANYIP:
            pass
        # if index is "host" than the next element is ip and this element is mask 32
        elif name == AnyIP.HOST:
            index += 1
            name = splitted_group[index] + "/32"
        # if index is "object" or "object-group" than the next element is name of the obj
        elif name == 'object' or name == 'object-group':
            index += 1
            name = splitted_group[index]
        # if index is ip address than the next element is mask
        elif len(name.split('.')) == 4:
            name = splitted_group[index]
            net_mask = splitted_group[index + 1]
            name = name + "/" + str(netmask_to_slash(net_mask))
            index += 1
        # if index is "eq" than the next element is port
        elif name == "eq":
            index += 1
            name = splitted_group[index]
        # else it is the name
        else:
            name = splitted_group[index]
        return name, index + 1

    def __get_application(self, protocol_name, port_name):
        """
        Method finds Aplication by protocol and port name from ACL
        :param protocol_name:
        :param port_name:
        :return: Aplication object holding protocol (ServiceNode or Aplication)
            and port (ServiceNode or AplicationGroup)
        """
        if port_name is None:
            # When port is None there are two possible forms. In first case service object is found.
            # "access-list From_Vlan404 extended permit object DHCP-RENEW
            # object Vlan404 object-group D_UKB-DHCP-SERVERS"
            application = self.applications_holder.find_application_by_name(protocol_name)
            if application is not None:
                return application
            # Else we have to find protocol in ServiceHolder.
            # "access-list From_Vlan433 extended permit ip object Vlan433 object Vlan435"
            port = service_holder.find_by_name(ServiceEnum.ANY.value)
        else:
            # Similar logic is used here, first try to find object-group in holder.
            # "access-list To_Vlan437 extended deny tcp any object Vlan437 object-group RDP"
            application = self.applications_holder.find_application_group_by_name(port_name)
            if application is not None:
                # When we find AppGroup with tcp-udp protocol, we have to store a copy of
                # the AppGroup with protocol from ACL.
                if application.applications[0].protocol.name == ServiceEnum.TCP_UDP:
                    new_application = ApplicationGroup(application.name,
                                                       deepcopy(application.applications),
                                                       application.description)
                    for app in new_application.applications:
                        app.protocol = self.__find_service(protocol_name)
                    return new_application
                else:
                    return application
            # "access-list From_Vlan433 extended permit tcp object Vlan433 any eq www"
            port = self.__find_service(port_name)

        protocol = self.__find_service(protocol_name)
        application = Application(name=None, protocol=protocol, service_protocol=port)
        return application

    @staticmethod
    def __find_service(port_number):
        """
        Tries to find service from port_number and protocol using function getservbyport
        from socket library.
        :param port_number: port number in string
        :return: found service in string
        """
        # handle special case for cisco
        if port_number == 'h323':
            port_number = '1720'
        elif port_number == 'nfs':
            port_number = '2049'
        if port_number.isdigit():
            service = service_holder.find_by_port_number(int(port_number))
        elif port_number.isalnum() or port_number == "tcp-udp":
            # handle special case for cisco
            if port_number == 'bootps':
                port_number = ServiceEnum.DHCP_SERVER.value
            elif port_number == 'bootpc':
                port_number = ServiceEnum.DHCP_CLIENT.value
            elif port_number == 'domain':
                port_number = ServiceEnum.DNS.value
            # www is same as http so we store them as one in cisco
            elif port_number == 'www':
                port_number = 'http'
            service = service_holder.find_by_name(port_number)
        # if port_number is composed of numbers, example "6633"
        else:
            raise Exception("Service contain special characters: " + port_number)
        return service

    def __parse_routes(self):
        """
        Parsing route-info.json to make self.zone_objects dictionary.
        """
        route_file = self._routes

        # there are two ways of file, with or without sign in variable FIRST_LINE_TO_SKIP
        FIRST_LINE_TO_SKIP = "Gateway"
        SECOND_LINE_TO_SKIP = ""
        first_line = 0
        counter = 0
        for line in route_file[0]:
            if FIRST_LINE_TO_SKIP in line:
                first_line = counter
                break
            counter += 1

        if first_line != 0:
            # strip the info lines about flags and keep only routes
            route_lines = route_file[0][first_line + 2:]
        else:
            # strip the info lines about flags and keep only routes
            route_lines = route_file[0][route_file[0].index(SECOND_LINE_TO_SKIP) + 1:]

        # make object ZoneObject from every line and add it into self.zone_objects dictionary
        dict_zone_to_ips = {}
        for route in route_lines:
            route_type = route.split()[0]
            if route_type == "L":
                # this route type is local interface - not a network
                # a C-type route is associated with this interface
                # thus continuing - otherwise we have duplicates
                continue
            zone = route.split()[-1]
            found_zone = self.__parse_zones(zone)
            if route_type == '*S':
                if found_zone in dict_zone_to_ips:
                    dict_zone_to_ips[found_zone].append(AnyIP.ANYIP)
                else:
                    dict_zone_to_ips[found_zone] = [AnyIP.ANYIP]
                continue
            address = route.split()[1]
            netmask = route.split()[2]
            ip_address = IPAddress(address + "/" + str(netmask_to_slash(netmask)))
            if found_zone in dict_zone_to_ips:
                dict_zone_to_ips[found_zone].append(ip_address)
            else:
                dict_zone_to_ips[found_zone] = [ip_address]
        for zone, ips in dict_zone_to_ips.items():
            self._zones_objects.append(ZoneObject(zone, ips))

    def __parse_zones(self, searched_zone):
        """
        Parse zones.json into object Interface that is in object Zones and is connected with object
        Routes.
        :param searched_zone: name of the searched zone
        :return:
        """
        # save list zones.json into variable zones
        zones = self._zones[0]
        # for every line in zones
        for line in zones:
            # delete empty strings and splits lines into individual words
            splitted_line = [item for item in line.split(" ") if item != ""]
            # skip first line with column names
            if splitted_line != ['Interface', 'Name', 'Security'] and splitted_line is not None \
                    and searched_zone == splitted_line[1]:
                return splitted_line[1]
        return None

    def __parse_address_book(self):
        """
        Parsing address_book.json by parsing simple object first and group objects second.
        :return: makes self.net_object_holder and self.application_holder
        """
        self.__parse_simple_objects()
        self.__parse_group_objects()

    def __parse_simple_objects(self):
        """
        Parsing simple objects from address-book.json to make self.net_object_holder.
        :return: self.net_object_holder dictionary
        """
        # saving simple objects from address-book.json into variable
        simple_objects = self._address_book[0]
        # counter is used to make NetObject because there is network name
        # as first element and ip address as next element
        counter_objects = 0
        for obj_line in simple_objects:
            # delete empty strings and splits line into individual words
            splitted_obj_line = [x for x in obj_line.split(' ') if x != ""]
            # if line starts with 'object network', it represents name of the new network
            if not splitted_obj_line:
                return
            if splitted_obj_line[0] == "object" and splitted_obj_line[1] == "network":
                # ip is always the next element in simple_objects, thats why the counter
                address_line = [x for x in simple_objects[counter_objects + 1].split(' ')
                                if x != ""]
                # add new NetObject into self.net_object_holder
                if address_line[0] == "host":
                    self.update_net_objects(IPAddress(address_line[1]), splitted_obj_line[2])
                if address_line[0] == "subnet":
                    self.update_net_objects(IPAddress(address_line[1] + "/" + address_line[2])
                                            , splitted_obj_line[2])
            # if line starts with 'object service', it represents service
            elif splitted_obj_line[0] == "object" and splitted_obj_line[1] == "service":
                # delete empty strings and splits next line of simple_objects into individual words
                application_split = [x for x in simple_objects[counter_objects + 1].split(' ')
                                     if x != ""]
                self.update_application_dict(application_split, splitted_obj_line)
            elif splitted_obj_line[0] in ['host', 'subnet', 'service']:
                # no action needed here
                pass
            else:
                raise Exception("Unsupported simple object: " + str(splitted_obj_line))

            counter_objects = counter_objects + 1

    def update_net_objects(self, net_object_address, net_object_name):
        """
        Adds new NetObject into self.net_object_holder
        :param net_object_address: ip address in IPAddress object
        :param net_object_name: name of net object
        :return: updated dictionary self.net_object_holder
        """
        # make new NetObject
        net_object = NetObject(name=net_object_name, address=net_object_address)
        # adds new NetObject into dictionary self.net_object_holder
        self.net_object_holder.add_net_obj(net_object_name, net_object)

    def update_application_dict(self, application_split, splitted_obj_line):
        """
        Adds new Application into dictionary self.application_holder.
        :param application_split spllited service line from address book
        :param splitted_obj_line splitted object service line from address book
        """
        # if application has source it means that it is in form:
        # "object service DHCP-RENEW", " service udp source eq bootpc destination eq bootps"
        if "source" in application_split:
            self.applications_holder.add_application(
                Application(name=splitted_obj_line[2],
                            protocol=self.__find_service(application_split[1]),
                            service_protocol=self.__find_service(
                                application_split[application_split.index('destination') + 2]),
                            source_port=self.__find_service(
                                application_split[application_split.index('source') + 2])))
        # if application doesnt have source it means that it is in form:
        # "object service peruntcp22", " service tcp destination eq ssh ",
        else:
            self.applications_holder.add_application(
                Application(name=splitted_obj_line[2],
                            protocol=self.__find_service(application_split[1]),
                            service_protocol=self.__find_service(
                                application_split[application_split.index('destination') + 2])))

    def __parse_group_objects(self):
        """
        Parsing group objects from address-book.json to make self.net_object_holder.
        :return: self.net_object_holder dictionary
        """
        group_objects = self._address_book[1]
        temporary_objects_holder = []
        description = None
        # reverse list of group_objects because it is easier to parse it
        for grp_line in reversed(group_objects):
            # delete empty strings and splits line into individual words
            splitted_grp_line = [x for x in grp_line.split(' ') if x != ""]
            if len(splitted_grp_line) == 0:
                continue
            # if line starts with 'object-group', it represents name of the new group
            if splitted_grp_line[0] == 'object-group':
                self.__parse_object_group(temporary_objects_holder, splitted_grp_line, description)
                # temporary_objects_holder list reset
                temporary_objects_holder = []
                description = None
            elif splitted_grp_line[0] == 'network-object':
                self.__parse_network_object(temporary_objects_holder, splitted_grp_line)
            elif splitted_grp_line[0] == 'port-object':
                self.__parse_port_object(temporary_objects_holder, splitted_grp_line)
            elif splitted_grp_line[0] == 'group-object':
                self.__parse_group_object(temporary_objects_holder, splitted_grp_line)
            elif splitted_grp_line[0] == 'description':
                splitted_grp_line.pop(0)
                description = ' '.join(splitted_grp_line)

    def __parse_object_group(self, objects, splitted_grp_line, description):
        """
        Update holders of NetObjects and Applications
        :param description: description of object-group
        :param objects: list of ServiceNode depends on type
        :param splitted_grp_line: splitted line from address book
        """
        if splitted_grp_line[1] == 'network':
            net_object_group = NetObjectGroup(splitted_grp_line[2], objects[::-1], description)
            self.net_object_holder.update_net_object_groups(
                {splitted_grp_line[2]: net_object_group})
        elif splitted_grp_line[1] == 'service':
            if len(objects) == 0:
                raise Exception("Empty application group: " + splitted_grp_line[2])
            applications = []
            # we have just list of ports here so we need to create applications a store protocol
            # into them
            for obj in objects[::-1]:
                applications.append(Application(name=None,
                                                protocol=self.__find_service(splitted_grp_line[3]),
                                                service_protocol=obj))
            self.applications_holder.add_application_group(
                ApplicationGroup(name=splitted_grp_line[2],
                                 applications=applications, description=description))
        else:
            raise Exception("Unsupported operation: " + " ".join(splitted_grp_line))

    def __parse_network_object(self, net_objects, splitted_grp_line):
        """
        Adds net object to net objects
        :param net_objects: list of NetObject
        :param splitted_grp_line: splitted line from address book
        """
        # skip form: network-object 2001:718:801::/48, because they are not used in perun
        if len(splitted_grp_line) == 2:
            return
        # network object in form: network-object object perunObj10.1.1.2slash32
        if splitted_grp_line[1] == 'object':
            if self.net_object_holder.find_net_obj_by_name(splitted_grp_line[2]) is None:
                raise Exception("Object: " + splitted_grp_line[2] + " is not in Net Objects")
            net_objects.append(splitted_grp_line[2])
        # network object in form: network-object host 147.251.106.73
        elif splitted_grp_line[1] == 'host':
            net_object = NetObject(splitted_grp_line[2] + "/32", IPAddress(splitted_grp_line[2]))
            self.net_object_holder.add_tmp_net_obj(net_object.name, net_object)
            net_objects.append(net_object.name)
        # network object in form: network-object 147.251.0.0 255.255.0.0
        else:
            net_object = NetObject(splitted_grp_line[1] + '/' +
                                   str(netmask_to_slash(splitted_grp_line[2]))
                                   , IPAddress(splitted_grp_line[1] + '/' + splitted_grp_line[2]))
            self.net_object_holder.add_tmp_net_obj(net_object.name, net_object)
            net_objects.append(net_object.name)

    def __parse_port_object(self, port_objects, splitted_grp_line):
        """
        Adds port object to port objects
        :param port_objects: list of ports (Service)
        :param splitted_grp_line: splitted line from address book
        """
        # skip form: port-object range 3230 3248
        if splitted_grp_line[1] == 'range':
            # we dont care about port range because there is no port range in perun
            return
        # port number or name in form: port-object eq www
        else:
            service = self.__find_service(splitted_grp_line[2])
            port_objects.append(service)

    @staticmethod
    def __parse_group_object(group_objects, splitted_grp_line):
        """
        Adds group object to group objects
        :param group_objects: list of group objects (NetObject)
        :param splitted_grp_line: splitted line from address book
        """
        group_objects.append(splitted_grp_line[1])

    def __parse_cisco_rule_set_to_policies_and_address_book(self):
        """
        Reversed parsing from list self.cisco_parsed_rule_set into json files
        (policies.json and address-book.json)
        :return: policies.json and address-book.json
        """
        # make cisco policies from list self.cisco_parsed_rule_set
        access_group, access_list = self.abstract_to_policies()

        # make simple objects with application and
        # group objects from list self.cisco_parsed_rule_set
        simple_objects_with_app, group_objects = self.abstract_to_address_book()
        return [access_group, access_list], [simple_objects_with_app, group_objects]

    def abstract_to_policies(self, rule_sets=None):
        """
        Appends list access_group and list access_list with string from
        list self.cisco_parsed_rule_set
        :return: list of access_group and list of access_list
        """
        if rule_sets is None:
            rule_sets = self.parsed_rule_sets
        access_list = []
        access_group = []
        for ruleset in rule_sets:
            # get variables from firewall rule set
            acl_direction, acl_interfaces, acl_name = \
                self.__get_name_interface_direction_from_rule_set(ruleset)
            # append list with string in shape of access group
            access_group.append(
                ' '.join(['access-group', acl_name, acl_direction, 'interface', acl_interfaces]))

            last_type = RuleType.CISCO
            for acl in ruleset.rules:
                # for every rule in rule set make one line of access list
                one_line = self.__get_one_line_of_access_list(acl)

                if acl.rule_type == RuleType.NEW_PERUN:
                    if last_type == RuleType.OLD_PERUN or last_type == RuleType.NEW_PERUN:
                        if access_list[-1] == "access-list " + acl_name + " remark Perun generated rules end":
                            access_list.pop()
                    if last_type == RuleType.CISCO:
                        if self.parsed_rule_sets == rule_sets or \
                                "access-list " + acl_name + " remark Perun generated rules will follow" not in self.get_all_remarks():
                            access_list.append(
                                "access-list " + acl_name + " remark Perun generated rules will follow")

                # if the rule has any remarks, add them before rule
                for remark in acl.description_list:
                    if "," in remark or " #" in remark or ":" in remark:
                        raise Exception("Unsupported characters by ansible in: " + remark
                                        + " check comma, #, :")
                    if "Perun generated rules end" in remark:
                        if 'access-list ' + remark.split(" ")[1] + ' remark Perun generated rules will follow' not in access_list:
                            continue
                    access_list.append(remark)
                # append list with string in shape of access list
                access_list.append(' '.join(one_line))

                if acl.rule_type == RuleType.NEW_PERUN:
                    if self.parsed_rule_sets == rule_sets or \
                            "access-list " + acl_name + " remark Perun generated rules end" not in self.get_all_remarks():
                        access_list.append(
                            "access-list " + acl_name + " remark Perun generated rules end")
                last_type = acl.rule_type

        return access_group, access_list

    def get_all_remarks(self):
        remarks = []
        for rule_set in self.parsed_rule_sets:
            for rule in rule_set.rules:
                remarks.extend(rule.description_list)
        return remarks

    @staticmethod
    def __get_name_interface_direction_from_rule_set(rule_set):
        """
        Finds name of interface, direction and name from FirewallRuleSet
        :param rule_set: object FirewallRuleSet
        :return: direction, interface and name
        """
        # if rule set Object called from_object exist
        if rule_set.from_object is not None:
            # get variables from from_object
            acl_name = rule_set.rule_set_name
            acl_interfaces = rule_set.from_object.name
            acl_direction = Direction.IN
        # if rule set Object called to_object exist
        elif rule_set.to_object is not None:
            # get variables from to_object
            acl_name = rule_set.rule_set_name
            acl_interfaces = rule_set.to_object.name
            acl_direction = Direction.OUT
        else:
            raise Exception("Interface not found: " + str(rule_set))
        return acl_direction, acl_interfaces, acl_name

    def __get_one_line_of_access_list(self, acl):
        """
        Create one line of access list in string
        by shape: ['access-list <name> extended <action> <protocol> <source_ip> <destination_ip>
        <service>']
        :param acl: one element from list rules in object FirewallRule
        :return: one line of access list in string
        """
        # get variables from acl (object FirewallRule)
        name, action, protocol, source_ip, destination_ip, port = \
            self.__get_name_action_protocol_ip_service_ap_from_rules(acl)
        one_list = ['access-list', name, 'extended', str(action), protocol, source_ip,
                    destination_ip]
        if port != "":
            one_list.append(port)
        if acl.action.other_functionality is not None:
            one_list.append(acl.action.other_functionality.value)
        return one_list

    def __get_name_action_protocol_ip_service_ap_from_rules(self, acl):
        """
        Parse acl (object FirewallRule) into variables: action, destination_ip, name, protocol,
        service, source_ip
        :param acl: one element from list rules in object FirewallRule
        :return: action, destination_ip, name, protocol, port, source_ip
        """
        name = acl.rule_set_name
        action = acl.action.action
        source = self.__net_object_to_acl(acl.match_rule.src_net_obj_name)
        destination = self.__net_object_to_acl(acl.match_rule.dst_net_obj_name)
        # get protocol and port as name, because we try to find them in holders first
        protocol = acl.match_rule.application.name
        port = acl.match_rule.application.name

        if self.applications_holder.find_application_by_name(protocol) is not None:
            if self.applications_holder.find_application_by_name(protocol).protocol \
                    == self.__find_service("any"):
                protocol = "any"
            else:
                protocol = "object " + protocol
            port = ""
        elif self.applications_holder.find_application_group_by_name(port) is not None:
            protocol = str(acl.match_rule.application.applications[0].protocol)
            port = "object-group " + port
        # when there are no objects used, set protocol and port from application
        else:
            protocol = str(acl.match_rule.application.protocol)
            if acl.match_rule.application.service_protocol is None or \
                    str(acl.match_rule.application.service_protocol) == "any":
                port = ""
            else:
                port = "eq " + str(acl.match_rule.application.service_protocol.port_number)
        return name, action, protocol, source, destination, port

    def __net_object_to_acl(self, name):
        """
        Returns net object in string as it should look like in acl
        :param name: of net object
        :return: network object should look like in acl
        """
        # get source and destination ip from net_objects
        address = ""
        if name == "any":
            address = name
        elif self.net_object_holder.find_net_obj(name) is not None and address == "":
            address = "object " + name
        elif self.net_object_holder.find_net_obj_group(name) is not None and address == "":
            address = "object-group " + name
        elif self.net_object_holder.find_tmp_net_obj(name) is not None and address == "":
            address = self.__get_ip_with_netmask(self.net_object_holder.find_tmp_net_obj(name))
        else:
            address = "object-group " + name
            # raise Exception("Cannot convert net object to acl: " + name)
        return address

    @staticmethod
    def __get_ip_with_netmask(net_obj):
        """
        Gets ip with netmask from object NetObject parameter addresses
        :param net_obj: NetObject from dictionary
        :return: string of ip and netmask or host and ip for netmask 32
        """
        ip_address = net_obj.address.ip_address
        if ip_address == AnyIP.ANYIP:
            return AnyIP.ANYIP
        # 'host <ip>'
        elif netmask_to_slash(str(ip_address.netmask)) == int(32):
            ip_address = 'host ' + str(ip_address.network.network_address)
        # '<ip> <netmask>'
        else:
            ip_address = str(ip_address.network.network_address) + ' ' + str(ip_address.netmask)
        return ip_address

    def abstract_to_address_book(self, net_obj_holder=None, app_holder=None):
        """
        Makes simple objects with application and group objects from list self.cisco_parsed_rule_set
        :param net_obj_holder: holder from juniper or perun, None for cisco
        :param app_holder: holder from juniper or perun, None for cisco
        :return: append group_objects and simple_objects with string
        """
        if net_obj_holder is None:
            net_obj_holder = self.net_object_holder
        if app_holder is None:
            app_holder = self.applications_holder

        simple_objects = self.abstract_to_cisco_simple_objects(net_obj_holder)
        group_objects = self.abstract_to_cisco_group_objects(net_obj_holder)

        # parse applications into end of list simple_objects
        simple_objects_with_app = self.__parse_applications_to_simple_objects(app_holder)
        group_objects_with_app = self.__parse_applications_to_group_objects(app_holder)
        return simple_objects + simple_objects_with_app, group_objects + group_objects_with_app

    def abstract_to_cisco_simple_objects(self, net_obj_holder):
        """
        Append simple objects with object network and ip address.
        :param net_obj_holder: NetObjectHolder
        :return: updated list simple_objects
        """
        simple_objects = []
        for net_object_name in net_obj_holder.get_net_objects():
            if AnyIP.ANYIP == net_object_name:
                continue
            ip_address = self.__get_ip_with_netmask(
                net_obj_holder.get_net_objects()[net_object_name])
            # add subnet before  <ip> <netmask> form
            if len(ip_address.split(".")) == 7:
                ip_address = 'subnet ' + ip_address
            # make list in structure of "object network <name>" and " <ip>" in simple objects
            simple_name = ["object network", str(net_object_name)]
            simple_address = [str(ip_address)]
            # append simple_objects with name and address
            simple_objects.append(" ".join(simple_name))
            simple_objects.append(" ".join(simple_address))
        return simple_objects

    def abstract_to_cisco_group_objects(self, net_obj_holder):
        """
        Append group objects with object-group network (name) and
        network-object object (ip address).
        :param net_obj_holder:
        :return: updated list group_objects
        """
        group_objects = []
        for net_object_group_name in reversed(net_obj_holder.get_net_object_groups()):
            net_object_group = net_obj_holder.find_net_obj_by_name(net_object_group_name)
            if not net_object_group.get_net_object_names():
                continue
            # append group_objects with "object-group network <name>"
            group_objects.append(" ".join(["object-group network", net_object_group.name]))

            if net_object_group.description is not None:
                group_objects.append(" ".join(["description", str(net_object_group.description)]))

            if len(net_object_group.get_net_object_names()) == 0:
                raise Exception("Empty network object group: " + net_object_group.name)
            # go through all objects in holder and its components
            for simple_net_object in net_object_group.get_net_object_names():
                if simple_net_object in self.net_object_holder.get_net_objects().keys():
                    simple_net_object = self.net_object_holder.find_net_obj(simple_net_object)
                    group_objects.append(
                        " ".join(["network-object object", simple_net_object.name]))
                elif simple_net_object in self.net_object_holder.get_net_object_groups().keys():
                    simple_net_object = self.net_object_holder.find_net_obj_group(simple_net_object)
                    group_name = simple_net_object.name
                    group_objects.append(" ".join(["group-object", str(group_name)]))
                elif simple_net_object in self.net_object_holder.get_tmp_net_objects().keys():
                    simple_net_object = self.net_object_holder.find_tmp_net_obj(simple_net_object)
                    ip_address = self.__get_ip_with_netmask(simple_net_object)
                    # append group_objects with " network-object object <address_name>"
                    group_objects.append(" ".join(["network-object", str(ip_address)]))
                else:
                    raise Exception("Object not found in config: " + simple_net_object)
        return group_objects

    def __parse_applications_to_simple_objects(self, app_holder):
        """
        Parse applications from dictionary self.application_holder into end of simple objects
        :param simple_objects: simple_objects with object network and ip addresses
        :param app_holder: ApplicationHolder
        :return: simple objects with applications
        """
        simple_objects = []
        # go through every app in dictionary self.application_holder
        for app in app_holder.application_dict:
            if AnyIP.ANYIP == app:
                continue
            # find protocol and ports in dictionary self.application_holder according to key app
            protocol, destination_port, source_port = app_holder.find_protocol_and_port(app)

            protocol = self.__service_node_to_abstract_protocol(protocol)
            destination_port = self.__service_node_to_abstract_port(destination_port)
            source_port = self.__service_node_to_abstract_port(source_port)
            # append simple_objects with object service in shape: "object service <app>"
            simple_objects.append(" ".join(["object service", str(app)]))
            # append simple_objects with service in shape:
            if source_port == "":
                # " service <protocol> destination eq <port_number> "
                simple_objects.append(
                    " ".join(["service", protocol, "destination eq", destination_port]))
            else:
                # " service <protocol> source eq <port_number> destination eq <port_number> "
                simple_objects.append(
                    " ".join(["service", protocol, "source eq", source_port,
                              "destination eq", destination_port]))
        return simple_objects

    @staticmethod
    def __service_node_to_abstract_port(port):
        """
        Resolve special cases to abstract
        :param port: ServiceNode object
        :return: input values as they should look like in address book
        """
        if port is not None:
            port = str(port.port_number)
        else:
            port = ""

        if port == "dhcp-client":
            port = "bootpc"
        elif port == "http":
            port = "www"
        elif port == "dhcp-server":
            port = "bootps"
        elif port == "http":
            port = "www"
        elif port == '1720':
            port = 'h323'
        elif port == '2049':
            port = 'nfs'

        return port

    @staticmethod
    def __service_node_to_abstract_protocol(protocol):
        """
        Resolve special cases to abstract
        :param protocol: ServiceNode object
        :return: input values as they should look like in address book
        """
        protocol_value = protocol.value.lower()
        if protocol_value == "http":
            protocol = "www"
        else:
            protocol = protocol_value

        return protocol

    @staticmethod
    def __parse_applications_to_group_objects(application_holder):
        """
        Append group objects with object-group service (name), protocol and
        port-object object (service node).
        :param application_holder:
        :return: updated list group_objects
        """
        group_objects = []
        for application_group_name in reversed(application_holder.get_applications_groups()):
            application_group = application_holder.find_application_group_by_name(
                application_group_name)

            # append group_objects with "object-group service <name>"
            group_objects.append(" ".join(["object-group service", str(application_group.name),
                                           str(application_group.applications[0].protocol)]))

            if application_group.description:
                group_objects.append(" ".join(["description", str(application_group.description)]))

            # go through all ports in this application_group_name
            for app in application_group.applications:
                port = FirewallParserCisco.__service_node_to_abstract_port(app.service_protocol)
                group_objects.append("port-object eq " + port)
        return group_objects

    def deploy_policies(self, rule_sets):
        """
        Write cisco policies to final output configuration rules.yml.
        :param rule_sets: rule sets to deploy
        """
        original_groups, original_acls = self.abstract_to_policies(self.parsed_rule_sets)
        groups, policies = self.abstract_to_policies(rule_sets)
        policies = self.__add_line_numbers(original_groups, original_acls, policies)

        to_write = {'all_groups': groups, 'all_policies': policies}
        with open(self.exec_dir + 'roles/deploy-all-asa/vars/rules.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False, width=1000)

        to_delete = []
        for rule_set in rule_sets:
            for acl in rule_set.to_delete:
                if self.parsed_rule_sets != rule_sets:
                    to_delete.append("no " + " ".join(self.__get_one_line_of_access_list(acl)))

        to_write = {'delete_commands': to_delete}
        with open(self.exec_dir + 'roles/deploy-all-asa/vars/to_delete.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False, width=1000)

    def deploy_services(self, applications_holder):
        """
        Write services to final output configuration services.yml.
        :param applications_holder: applications to deploy
        """
        simple_objects_with_app = self.__parse_applications_to_simple_objects(applications_holder)
        group_objects_with_app = self.__parse_applications_to_group_objects(applications_holder)
        services = self.__convert_objects_to_yml(simple_objects_with_app, group_objects_with_app)
        to_write = {'services': services}
        with open(self.exec_dir + 'roles/deploy-all-asa/vars/services.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False)

    def deploy_net_objects(self, net_object_holder):
        """
        Write address book to final output configuration net_objs.yml.
        :param net_object_holder: network objects to deploy
        """
        simple_objects = self.abstract_to_cisco_simple_objects(net_object_holder)
        group_objects = self.abstract_to_cisco_group_objects(net_object_holder)
        objects = self.__convert_objects_to_yml(simple_objects, group_objects)

        to_write = {'perun_processed_net_objs': objects}
        with open(self.exec_dir + 'roles/deploy-all-asa/vars/net_objs.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False)

    @staticmethod
    def __convert_objects_to_yml(simple_objects, group_objects):
        """
        Converts arrays of objects and group objects into array suitable for deploying
        :param simple_objects: simple objects or applications
        :param group_objects: group objects or applications
        :return:
        """
        objects = []

        if len(simple_objects) % 2 == 1:
            raise Exception("Odd length of simple services: " + str(simple_objects))
        for i in range(0, len(simple_objects), 2):
            objects.append({'lines': [simple_objects[i + 1]], 'parents': [simple_objects[i]]})

        lines = []
        parents = None
        for group_object in group_objects:
            if group_object.split(" ")[0] == "object-group":
                if parents is not None:
                    objects.append({'lines': lines, 'parents': [parents]})
                parents = group_object
                lines = []
            else:
                lines.append(group_object)
        if parents is not None:
            objects.append({'lines': lines, 'parents': [parents]})
        return objects

    def __add_line_numbers(self, original_groups, original_acls, diff_acls):
        """
        Adds line numbers for inserting
        :param original_acls: list of acls in string
        :param diff_acls: list of acls in string
        :return: list of acls in string
        """
        acls_with_lines = []
        # opt 0
        if original_acls == diff_acls:
            # opt 0
            rule_sets = []
            for group in original_groups:
                rules = []
                for rule in original_acls:
                    if group.split(" ")[1] == rule.split(" ")[1]:
                        rules.append(rule)
                rule_sets.append(rules)
            for rule_set in rule_sets:
                if "Perun generated rules end" in rule_set[-1]:
                    i = len(rule_set) - 1
                    while "any any" not in rule_set[i]:
                        splitted_acl = rule_set[i].split(" ")
                        splitted_acl.insert(2, "line " + str(i))
                        rule_set[i] = " ".join(splitted_acl)
                        i -= 1
                acls_with_lines.extend(rule_set)
        else:
            # opt 1
            for acl in diff_acls:
                splitted_acl = acl.split(" ")
                acl_count = self.__count_acls_with_same_name(original_acls, splitted_acl[1])
                if acl not in original_acls:
                    temp = [s for s in original_groups if splitted_acl[1] in s]
                    if "access-list " + splitted_acl[1] + " remark Perun generated rules end" in original_acls:
                        acl_count -= 2
                    if len(temp) != 0:
                        splitted_acl.insert(2, "line " + str(acl_count))
                    acls_with_lines.append(" ".join(splitted_acl))
                    original_acls.append(acls_with_lines[-1])
                else:
                    acls_with_lines.append(acl)
        return acls_with_lines

    @staticmethod
    def __count_acls_with_same_name(rule_set, name):
        count = 0
        for acl in rule_set:
            if acl.split(" ")[1] == name:
                count += 1
        return count

    def deploy_remote_hard(self, firewall_configurator):
        firewall_configurator.ansible_handle(self.exec_dir, tags=AnsibleHandlePlay.deploy_asa.name)
