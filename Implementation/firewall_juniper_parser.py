"""
FirewallParserJuniper implementation
"""
import json

import yaml
from Implementation.enums import ActionsEnum, RuleType, AnyIP
from Implementation.firewall_parser_base import FirewallParserBase
from Implementation.firewall_rule_set import FirewallRuleSet, Action, FirewallRule, JuniperLog, \
    IPAddress, MatchRule, ZoneObject
from Implementation.helper_functions import AnsibleHandlePlay
from Implementation.network_object_holder import NetObject, NetObjectGroup
from Implementation.service_holder import service_holder, ServiceEnum, Application, \
    ApplicationGroup, ApplicationHolder


class FirewallParserJuniper(FirewallParserBase):
    """
    Class for working witch Juniper Firewall.
    Parse rules to abstract classes structure.
    Write rules to output config files.
    """

    def __init__(self, json_policies, json_zones, json_applications, json_address_book,
                 json_interfaces, json_route_info, exec_dir):
        """
        :param json_policies : json (multilevel dictionary) - Current firewall settings.
        """
        super().__init__(policies=json_policies, address_book=json_address_book,
                         zones=json_zones, exec_dir=exec_dir)
        self.__route_info = json_route_info
        self.__interfaces = json_interfaces
        self._zones_objects = []
        self.__json_applications = json_applications
        self.__predefined_application_holder = ApplicationHolder()

    def parse(self):
        """
        Main function for policies parsing, where rule_sets are creating.
        1. Zones and address books are parsed to self.zones_object and self.net_object_holder
        2. Applications, protocols and services are parsed
        to dictionary self.protocol_service_by_application
        3. Rules for each rule set are parsed
        4. All rule sets are returned
        :return rule_sets: list of all rule sets
        """
        self.__get_net_objects_holder()
        self.__get_zones()
        self.__add_predefined_applications()
        self.__get_applications_and_application_sets()
        rule_sets = []
        if 'global' in self._policies:
            policies = self._policies['global']
            if isinstance(policies, list):
                for policy in policies:
                    rule_sets.append(self.__parse_global_rule_set(policy))
            else:
                rule_sets.append(self.__parse_global_rule_set(policies))
        if 'policy' in self._policies:
            policies = self._policies['policy']
            if isinstance(policies, list):
                for policy in policies:
                    rule_sets.append(self.__parse_rule_set(policy))
            else:
                rule_sets.append(self.__parse_global_rule_set(policies))
        self.parsed_rule_sets = rule_sets
        return rule_sets

    def __parse_rule_set(self, rule_set):
        """
        Parse single rule set.
        :param rule_set: json segment with one rule set
        :return FirewallRuleSet
        """
        rule_type = RuleType.JUNIPER
        from_zone = self.__find_zone_object(rule_set['from-zone-name'])
        to_zone = self.__find_zone_object(rule_set['to-zone-name'])
        if from_zone is None:
            raise Exception("None existing zone " + rule_set['from-zone-name'] + ".")
        if to_zone is None:
            raise Exception("None existing zone " + rule_set['to-zone-name'] + ".")
        rule_set_name = "from_" + str(from_zone) + "_to_" + str(to_zone)
        rules = []
        rule_list = rule_set['policy']
        if isinstance(rule_list, list):
            for rule in rule_list:
                rules.append(self.__parse_rule(rule, rule_set_name))
        else:
            rules.append(self.__parse_rule(rule_list, rule_set_name))
        return FirewallRuleSet(rule_set_name, from_zone, to_zone, rules, rule_type)

    def __parse_global_rule_set(self, rule_set):
        """
        Parse single rule set.
        :param rule_set: json segment with one rule set
        :return FirewallRuleSet
        """
        rule_type = RuleType.JUNIPER
        from_zone = None
        to_zone = None
        if 'from-zone-name' in rule_set:
            from_zone = self.__find_zone_object(rule_set['from-zone-name'])
        if 'to-zone-name' in rule_set:
            to_zone = self.__find_zone_object(rule_set['to-zone-name'])
        rule_set_name = "from_" + str(from_zone) + "_to_" + str(to_zone)
        rules = []
        rule_list = rule_set['policy']
        if isinstance(rule_list, list):
            for rule in rule_list:
                rules.append(self.__parse_rule(rule, rule_set_name))
        else:
            rules.append(self.__parse_rule(rule_list, rule_set_name))
        return FirewallRuleSet(rule_set_name, from_zone, to_zone, rules, rule_type)

    def __parse_rule(self, rule, rule_set_name):
        """
        Method for parse one single rule from json segment.
        :param rule: json segment with one single rule
        :param rule_set_name: string name in format "from <zone> to <zone>"
        :return new FirewallRule
        """
        name = rule['name']
        description_list = []
        if 'description' in rule:
            description_list = [rule['description']]
        match_rule = self.__parse_match_rule(rule['match'])
        then = self.__parse_actions(rule['then'])
        rule_type = RuleType.JUNIPER
        if "perun" in name.lower():
            rule_type = RuleType.OLD_PERUN
        return FirewallRule(name, rule_set_name, match_rule, then, description_list, rule_type)

    @staticmethod
    def __parse_actions(then):
        """
        Parse actions from json. Other functionality can be JuniperLog.
        :param then: part of json (dictionary) with actions
        :return new Action
        """
        actions = None
        log = None
        for key, value in then.items():
            if key == ActionsEnum.LOG.value:
                logs = list(value.keys())
                log = JuniperLog(logs)
            else:
                actions = ActionsEnum(key)
        return Action(actions, log)

    def __parse_match_rule(self, match):
        """
        Parse match rule for FirewallRule from json segment.
        :param match: json segment with match rule.
        :return new MatchRule
        """
        source_net_obj = match['source-address']
        destination_net_obj = match['destination-address']
        application = match['application']
        application = self.__find_application_by_name(application)
        return MatchRule(source_net_obj, destination_net_obj, application)

    @staticmethod
    def __get_addresses_from_address_book(address_book):
        """
        Create NetObjects from addresses in address_book. Also return dictionary
        {"<name>": NetObject} for better searching NetObject by name.
        :param address_book: part of json with one address_book information'
        :return tuple(net_objects, address_dict)
        """
        net_objects = {}
        address_dict = {}
        addresses = address_book['address']
        if isinstance(addresses, list):
            for address in addresses:
                net_object_name = address['name']
                net_object_address = IPAddress(address['ip-prefix'])
                net_obj = NetObject(net_object_name, net_object_address)
                net_objects[net_object_name] = net_obj
                address_dict[net_object_name] = net_obj
        else:
            net_object_name = addresses['name']
            net_object_address = IPAddress(addresses['ip-prefix'])
            net_obj = NetObject(net_object_name, net_object_address)
            net_objects[net_object_name] = net_obj
        return net_objects

    @staticmethod
    def __get_address_sets_from_address_book(address_book, addresses_dict):
        """
        Create NetObjects from address_sets in address_book.
        :param address_book: part of json with one address_book information'
        :param addresses_dict: dictionary {"<address_name>" : NetObject}
        """
        net_objects = {}
        address_sets = address_book.get('address-set')
        for address_set in address_sets:
            net_object_name = address_set['name']
            addresses = []
            if isinstance(address_set['address'], list):
                for address in address_set['address']:
                    address_name = address['name']
                    if address_name in addresses_dict:
                        addresses.append(address_name)
            else:
                address_name = address_set['address']['name']
                if address_name in addresses_dict:
                    addresses.append(address_name)
            net_objects[net_object_name] = NetObjectGroup(net_object_name, addresses)
        return net_objects

    def __get_net_objects_holder(self):
        """
        Get NetObjectHolder from address book and save it to class attribute net_objects.
        :return NetObjectHolder
        """
        address_book = self._address_book
        address_net_objects = {}
        if isinstance(address_book, list):
            raise Exception("Don't support another address-books then global.")
        if 'address' in address_book:
            address_net_objects = self.__get_addresses_from_address_book(address_book)
            self.net_object_holder.update_net_objects(address_net_objects)
        # Parse address-sets
        if 'address-set' in address_book:
            address_sets_net_objects = \
                self.__get_address_sets_from_address_book(address_book, address_net_objects)
            self.net_object_holder.update_net_object_groups(address_sets_net_objects)

    def __get_applications_and_application_sets(self):
        """
        Get applications and application sets from self.json_policies
        and save them to self.application_holder.
        """
        applications = self.__json_applications['application']
        for application in applications:
            service_port = application['destination-port']
            application_name = application['name']
            protocol = service_holder.find_by_name(application['protocol'])
            port = service_holder.find_by_port_number(int(service_port))
            application = Application(application_name, protocol, port)
            self.applications_holder.add_application(application)
        # If json contains application-sets then parse application sets to application holder.
        if 'application-set' in self.__json_applications:
            self.__get_application_sets()

    def __get_application_sets(self):
        """
        Parse application sets from json segment to application holder.
        """
        application_sets = self.__json_applications['application-set']
        for application_set in application_sets:
            description = None
            if 'description' in application_set:
                description = application_set['description']
            application_set_name = application_set['name']
            applications = []
            if isinstance(application_set['application'], list):
                for application in application_set['application']:
                    found_app = self.applications_holder.find_application_by_name(
                        application['name'])
                    if found_app is None:
                        found_app = self.__predefined_application_holder.find_application_by_name(
                            application['name'])
                    applications.append(found_app)
            else:
                found_app = self.applications_holder.find_application_by_name(
                    application_set['application']['name'])
                if found_app is None:
                    found_app = self.__predefined_application_holder.find_application_by_name(
                        application_set['application']['name'])
                applications.append(found_app)
            new_application_group = ApplicationGroup(application_set_name, applications,
                                                     description)
            self.applications_holder.add_application_group(new_application_group)

    @staticmethod
    def __add_zone_to_ips(rt_destination, rt_entry, int_to_zone, zone_to_ips):
        """
        Add zone to dictionary {zone_name: list(ips)}. This dictionary is used for
        creating ZoneObjects.
        :param rt_destination: destination ip address
        :param rt_entry: json segment with info about rout
        :param int_to_zone: dictionary {interface : zone}
        :param zone_to_ips: dictionary {zone_name: list(ips)}
        """
        if rt_entry["protocol-name"] == "Local":
            return
        interface = rt_entry["nh"]["via"] if "via" in rt_entry["nh"] else \
            rt_entry["nh"]["nh-local-interface"]
        if interface in int_to_zone:
            ip_address = IPAddress(rt_destination)
            zone_name = int_to_zone[interface]
            print("Juniper: Resolving IP " + rt_destination + " and zone " + zone_name)
            if zone_name in zone_to_ips:
                if rt_destination == "0.0.0.0/0":
                    zone_to_ips[zone_name].append(AnyIP.ANYIP)
                else:
                    zone_to_ips[zone_name].append(ip_address)
            elif zone_name is not None:
                if rt_destination == "0.0.0.0/0":
                    zone_to_ips[zone_name] = [AnyIP.ANYIP]
                else:
                    zone_to_ips[zone_name] = [ip_address]

    def __get_zones(self):
        """
        Parse objects/zones and save them in list self.zones_objects.
        """
        int_to_zone = self.__get_interfaces_to_zones()
        zone_to_ips = {}
        print(json.dumps(self.__route_info, indent=4, sort_keys=False))
        # Parse interfaces
        for info in self.__route_info[0]["rpc-reply"]["route-information"]["route-table"]["rt"]:
            if isinstance(info["rt-entry"], list):
                for entry in info["rt-entry"]:
                    self.__add_zone_to_ips(info["rt-destination"], entry, int_to_zone, zone_to_ips)
            else:
                self.__add_zone_to_ips(info["rt-destination"],
                                       info["rt-entry"], int_to_zone, zone_to_ips)
        # Parse vlans
        for interface in self.__interfaces:
            if interface['name'] == "vlan":
                for unit in interface['unit']:
                    interface_name = interface['name'] + "." + unit['name']
                    if interface_name in int_to_zone:
                        zone = int_to_zone[interface_name]
                        address = unit["family"]["inet"]["address"]
                        addresses = []
                        if isinstance(address, list):
                            for single_address in address:
                                ip_address = IPAddress(single_address['name'])
                                addresses.append(ip_address)
                        else:
                            ip_address = IPAddress(address['name'])
                            addresses.append(ip_address)
                        zone_to_ips[zone] = addresses
        for zone, ips in zone_to_ips.items():
            self._zones_objects.append(ZoneObject(zone, ips))

    def __get_interfaces_to_zones(self):
        """
        Get dictionary {interface_name : zone_name}.
        """
        int_to_zone = {}
        for zone in self._zones["security-zone"]:
            zone_name = zone["name"]
            if zone_name not in ("trust", "untrust"):
                zone_interface = zone["interfaces"]
                if isinstance(zone_interface, list):
                    for interface in zone_interface:
                        int_to_zone[interface["name"]] = zone_name
                else:
                    int_to_zone[zone_interface["name"]] = zone_name
        return int_to_zone

    def __get_interface_name(self, zone_name):
        """
        Find interface by zone_name.
        :param zone_name: string/None
        """
        for zone in self._zones:
            if zone["name"] == zone_name and 'interfaces' in dict.keys:
                return zone["interfaces"]["name"]
        return None

    def __find_application_by_name(self, application_name):
        """
        Find application by name. If doesn't exist create new.
        : return Application / ApplicationGroup
        """
        if isinstance(application_name, list):
            applications = []
            for app_name in application_name:
                application = self.applications_holder.find_application_by_name(app_name)
                if application is None:
                    application = self.__predefined_application_holder.find_application_by_name(
                        app_name)
                applications.append(application)
            new_application = ApplicationGroup(None, applications)
        else:
            new_application = self.applications_holder.find_application_by_name(application_name)
            if new_application is None:
                new_application = self.__predefined_application_holder.find_application_by_name(
                    application_name)
        return new_application

    def __find_zone_object(self, object_name):
        """
        Find object (zone) by name.
        :return Object/None
        """
        for obj in self._zones_objects:
            if obj.name == object_name:
                return obj
        return None

    @staticmethod
    def __get_address(address):
        """
        Get address from dictionary.
        :param address: dictionary
        """
        if address == AnyIP.ANYIP:
            return "any"
        return address.name

    def abstract_to_policies(self, rule_sets):
        """
        Write rule_sets back to json. Only policies.
        :param rule_sets: list of FirewallRuleSet
        :return json_policies: rule sets in json
        """
        json_policies = {'policies': {}}
        global_policies = []
        policies = []
        for rule_set in rule_sets:
            json_rules = []
            for rule in rule_set.rules:
                json_rule = {}
                application = []
                if isinstance(rule.match_rule.application, ApplicationGroup) and \
                        rule.match_rule.application.name is None:
                    for app in rule.match_rule.application.applications:
                        application.append(app.name)
                else:
                    application = rule.match_rule.application.name
                for description in rule.description_list:
                    json_rule['description'] = description
                json_rule["match"] = {"application": application,
                                      "destination-address": rule.match_rule.dst_net_obj_name,
                                      "source-address": rule.match_rule.src_net_obj_name}
                json_rule["name"] = rule.rule_name
                json_rule["then"] = rule.action.juniper_dict()
                json_rules.append(json_rule)
            if len(json_rules) == 1:
                json_rules = json_rules[0]
            if rule_set.from_object is not None and rule_set.to_object is not None:
                json_policy = {"from-zone-name": rule_set.from_object.name,
                               "policy": json_rules,
                               "to-zone-name": rule_set.to_object.name
                               }
                policies.append(json_policy)
            else:
                json_policy = {
                    "policy": json_rules
                }
                global_policies.append(json_policy)
        if global_policies:
            if len(global_policies) > 1:
                json_policies['policies']['global'] = global_policies
            else:
                json_policies['policies']['global'] = global_policies[0]
        if policies:
            if len(policies) > 1:
                json_policies['policies']['policy'] = policies
            else:
                json_policies['policies']['policy'] = policies[0]
        return json_policies

    @staticmethod
    def abstract_to_applications(rule_sets):
        """
        Get back from rule set used applications.
        :param rule_sets: list of FirewallRuleSet
        """
        applications = {}
        applications_json = {"applications": {"application": []}}
        for rule_set in rule_sets:
            for rule in rule_set.rules:
                applications[rule.match_rule.application.name] = rule.match_rule.application
        for name, application in applications.items():
            application_name = name
            protocol_name = str(application.protocol)
            service = application.service_protocol
            if service.name == ServiceEnum.ANY:
                destination_port = "any"
            else:
                destination_port = service.port_number
            if protocol_name is not None and destination_port is not None:
                if application_name is None:
                    application_name = protocol_name + "_" + str(destination_port)
                if protocol_name != "any" and destination_port != "any":
                    application_json = {"name": application_name, "protocol": protocol_name,
                                        "destination-port": str(destination_port)}
                    applications_json["applications"]["application"].append(application_json)
        return applications_json

    def abstract_to_address_book(self, net_object_holder):
        """
        Get json of address books from net_object_holder.
        :param net_object_holder: NetObjectHolder (can be external: from cisco...)
        """
        address_book_json = {"address-book": [{"name": "global"}]}
        addresses = []
        address_sets = []
        for _, value in net_object_holder.get_net_objects().items():
            if value.name != AnyIP.ANYIP:
                addresses.append({
                    "name": value.name,
                    "ip-prefix": str(value.address.ip_address)})
        for _, value in net_object_holder.get_net_object_groups().items():
            net_object_names = value.get_net_object_names()
            if len(net_object_names) == 1:
                set_addresses = {"name": net_object_names[0]}
            else:
                set_addresses = []
                for net_object_name in net_object_names:
                    set_addresses.append({"name": net_object_name})
            address_set = {"name": value.name, "address": set_addresses}
            address_sets.append(address_set)
        if len(addresses) != 0:
            address_book_json["address-book"][0]["address"] = addresses
        if len(address_sets) != 0:
            address_book_json["address-book"][0]["address-set"] = address_sets
        return address_book_json

    @staticmethod
    def deploy_policy(policy, default_deny=None):
        """
        One single rule to string for deploy configuration.
        :param policy: FirewallRule
        :param default_deny: name of default deny role (default value None)
        """
        application_name = policy.match_rule.application.name
        applications = []
        if isinstance(policy.match_rule.application, ApplicationGroup) and application_name is None:
            for application in policy.match_rule.application.applications:
                applications.append(application.name)
        else:
            applications.append(application_name)
        rules = []
        for description in policy.description_list:
            rules.append(" policy " + policy.rule_name +
                         " description " + "\"" + description + "\"")
        for application in applications:
            if application is None:
                application = "any"
            rules.append(" policy " + str(policy.rule_name) +
                         " match source-address " + policy.match_rule.src_net_obj_name +
                         " destination-address " + policy.match_rule.dst_net_obj_name +
                         " application " + application)
        rules.append(" policy " + policy.rule_name + " then " + str(policy.action))
        if policy.action.other_functionality is not None and isinstance(
                policy.action.other_functionality, JuniperLog):
            logs = policy.action.other_functionality.logs
            logs.reverse()
            for log in logs:
                rules.append(" policy " + policy.rule_name + " then log " + log)
        if default_deny is not None and policy.rule_name != default_deny:
            rules.append(" policy " + policy.rule_name + " before policy " + default_deny)
        return rules

    def deploy_policies(self, rule_sets):
        """
        Write rule sets to final output configuration rules.yml.
        :param rule_sets: FirewallRuleSet
        :return string
        """
        settings = []
        to_delete = []
        for rule_set in rule_sets:
            default_deny = self.__get_default_deny_rule(self.parsed_rule_sets, rule_set.from_object,
                                                        rule_set.to_object)
            for policy in rule_set.rules:
                rules_for_zones = self.deploy_policy(policy, default_deny)
                insert_rule = None
                if default_deny is not None and policy.rule_name != default_deny:
                    insert_rule = rules_for_zones.pop(len(rules_for_zones) - 1)
                if rule_set.from_object is not None and rule_set.to_object is not None:
                    for command in rules_for_zones:
                        settings.append(
                            "set security policies from-zone " + rule_set.from_object.name
                            + " to-zone "
                            + rule_set.to_object.name + command)
                    if insert_rule is not None:
                        settings.append("insert security policies from-zone "
                                        + rule_set.from_object.name + " to-zone "
                                        + rule_set.to_object.name + insert_rule)
                else:
                    for command in rules_for_zones:
                        settings.append("set security policies global" + command)
                    if insert_rule is not None:
                        settings.append("insert security policies global " + insert_rule)
            for policy in rule_set.to_delete:
                to_delete.append("delete security policies from-zone " + rule_set.from_object.name
                                 + " to-zone " + rule_set.to_object.name
                                 + " policy " + policy.rule_name)
        to_write = {'all_policies': settings, 'delete_policies': to_delete}
        with open(self.exec_dir + 'roles/deploy-all-srx/vars/rules.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False)

    @staticmethod
    def __get_default_deny_rule(rule_sets, from_object, to_object):
        """
        Find default deny rule if exist.
        :param rule_sets: FirewallRuleSet
        :param from_object: ZoneObject from identify rule set
        :param to_object: ZoneObject to identify rule set
        :return string/None
        """
        for rule_set in rule_sets:
            if rule_set.from_object == from_object and rule_set.to_object == to_object:
                for i in range(len(rule_set.rules)):
                    rule = rule_set.rules[i]
                    if rule.action.action == ActionsEnum.DENY and rule.match_rule.src_net_obj_name \
                            == "any" and rule.match_rule.dst_net_obj_name == "any":
                        default_deny = rule_set.rules.pop(i)
                        rule_set.rules.insert(0, default_deny)
                        return rule.rule_name
        return None

    def deploy_services(self, applications_holder):
        """
        Write services to final output configuration services.yml.
        """
        all_services = []
        for application, value in applications_holder.get_applications().items():
            if application != ServiceEnum.ANY.value.lower():
                protocol = value.protocol
                service = value.service_protocol
                all_services.append("set applications application " + application
                                    + " protocol " + str(protocol)
                                    + " destination-port " + str(service.port_number))
        for application_set_name, app_group in \
                applications_holder.get_applications_groups().items():
            if app_group.description is not None:
                all_services.append("set applications application-set " + application_set_name +
                                    " description " + "\"" + app_group.description + "\"")
            for application in app_group.applications:
                if application is None:
                    print(None)
                all_services.append("set applications application-set " + application_set_name
                                    + " application " + application.name)
        to_write = {'services': all_services}
        with open(self.exec_dir + 'roles/deploy-all-srx/vars/services.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False)

    def deploy_net_objects(self, net_object_holder):
        """
        Write address book to final output configuration net_objs.yml.
        :param net_object_holder: NetObjectHolder from which we want to create configuration.
        """
        all_net_objects = []
        for _, net_object in net_object_holder.get_net_objects().items():
            if net_object.name == AnyIP.ANYIP.value:
                continue
            all_net_objects.append("set security address-book global address " + net_object.name
                                   + " " + str(net_object.address))
        for _, net_object in net_object_holder.get_net_object_groups().items():
            for address in net_object.get_net_object_names():
                all_net_objects.append("set security address-book global address-set "
                                       + net_object.name + " address " + address)
        to_write = {'perun_processed_net_objs': all_net_objects}
        with open(self.exec_dir + 'roles/deploy-all-srx/vars/net_objs.yml', 'w') as yaml_file:
            yaml.dump(to_write, yaml_file, default_flow_style=False)

    def deploy_remote_hard(self, firewall_configurator):
        """
        Set new configuration on Juniper firewall. Call ansible play book deploy_srx.
        :param firewall_configurator: FirewallConfigurator
        """
        firewall_configurator.ansible_handle(self.exec_dir, tags=AnsibleHandlePlay.deploy_srx.name)

    # TODO add all predefined applications
    def __add_predefined_applications(self):
        udp = service_holder.find_by_name('udp')
        dns = service_holder.find_by_name('dns')
        application = Application("junos-dns-udp", udp, dns)
        self.__predefined_application_holder.add_application(application)
        application = Application("junos-discard", None, None)
        self.__predefined_application_holder.add_application(application)
        icmp = service_holder.find_by_name('icmp')
        ping = service_holder.find_by_name('ping')
        application = Application("junos-icmp-ping", icmp, ping)
        self.__predefined_application_holder.add_application(application)
        application = Application("junos-bootps", None, None)
        self.__predefined_application_holder.add_application(application)
        application = Application("junos-bootpc", None, None)
        self.__predefined_application_holder.add_application(application)
