from Implementation.enums import AnyIP
from Implementation.firewall_rule_set import IPAddress


class NetObjectHolder(object):

    def __init__(self):
        """
        net_objects_dict: dictionary {<name> : NetObject}
        net_object_group_dict dictionary {<name> : NetObjectGroup}
        """
        any_net_object = NetObject(AnyIP.ANYIP.value, IPAddress(AnyIP.ANYIP))
        self.__net_objects_dict = {'any': any_net_object}
        self.__net_object_groups_dict = {}
        # for cisco - an IP can be assigned to ACL without actually creating an network object
        self.__temporary_net_objects_dict = {}

    # net object dict
    def get_net_objects(self):
        return self.__net_objects_dict

    def get_tmp_net_objects(self):
        return self.__temporary_net_objects_dict

    # net object group
    def get_net_object_groups(self):
        return self.__net_object_groups_dict

    def update_net_objects(self, net_objects_dict):
        self.__net_objects_dict.update(net_objects_dict)

    def update_net_object_groups(self, net_object_groups_dict):
        self.__net_object_groups_dict.update(net_object_groups_dict)

    def add_net_obj(self, name, net_object):
        self.__net_objects_dict[name] = net_object

    def add_tmp_net_obj(self, name, net_object):
        self.__temporary_net_objects_dict[name] = net_object

    def add_net_obj_group(self, net_object_group_name, net_object_names):
        if net_object_group_name in self.__net_object_groups_dict:
            raise Exception("Object group with name " + net_object_group_name + " already exists.")
        group_obj_to_add = NetObjectGroup(net_object_group_name, [])
        for net_obj_name in net_object_names:
            if net_obj_name not in self.__net_objects_dict:
                raise Exception("Cannot add net object group. "
                                "All net objects must be inserted in holder."
                                "Net object " + net_obj_name + " not found.")
            group_obj_to_add.add_net_object_name(net_obj_name)
        self.__net_object_groups_dict[net_object_group_name] = group_obj_to_add

    def remove_net_obj(self, name):
        self.__net_objects_dict.pop(name, None)

    def remove_tmp_net_obj(self, name):
        self.__temporary_net_objects_dict.pop(name, None)

    def remove_net_obj_group(self, name):
        self.__net_object_groups_dict.pop(name, None)

    def find_net_obj(self, name):
        if name in self.__net_objects_dict:
            return self.__net_objects_dict[name]
        return None

    def find_tmp_net_obj(self, name):
        if name in self.__temporary_net_objects_dict:
            return self.__temporary_net_objects_dict[name]
        return None

    def find_net_obj_group(self, name):
        if name in self.__net_object_groups_dict:
            return self.__net_object_groups_dict[name]
        return None

    def find_net_obj_by_name(self, name):
        if name in self.__net_objects_dict:
            return self.__net_objects_dict[name]
        if name in self.__net_object_groups_dict:
            return self.__net_object_groups_dict[name]
        if name in self.__temporary_net_objects_dict:
            return self.__temporary_net_objects_dict[name]
        return None

    def find_net_obj_or_group_name_by_ip_address_objects(self, searched_ip_address_objects):
        """
        Args:
            searched_ip_address_objects:
        Returns: an net object or net object group name that matches IPs
        """
        if isinstance(searched_ip_address_objects, list):
            return self.find_net_object_group_name_by_ip_address_objects(searched_ip_address_objects)
        else:
            return self.find_net_object_name_by_ip_address_objects(searched_ip_address_objects)

    def find_net_object_group_name_by_ip_address_objects(self, searched_ip_address_objects):
        for x in self.__net_object_groups_dict:
            group = self.__net_object_groups_dict[x]
            groups_ips = []
            for net_object_name in group.get_net_object_names():
                if net_object_name in self.__net_objects_dict:
                    groups_ips.append(self.__net_objects_dict[net_object_name].address)
                elif net_object_name in self.__net_object_groups_dict:
                    groups_ips.append(net_object_name)
                else:
                    groups_ips.append(self.__temporary_net_objects_dict[net_object_name].address)
            if compare_lists_of_objects(searched_ip_address_objects, groups_ips):
                return group.name

    def find_net_object_name_by_ip_address_objects(self, searched_ip_address_object):
        for x in self.__net_objects_dict:
            net_object = self.__net_objects_dict[x]
            if net_object.address == searched_ip_address_object:
                return net_object.name
        return None

    def find_tmp_net_obj_name_by_ip_address_object(self, address):
        for x in self.__temporary_net_objects_dict:
            net_object = self.__temporary_net_objects_dict[x]
            if net_object.address == address:
                return net_object.name
        return None

    def is_net_objects_subset(self, net_obj_name1, net_obj_name2):
        first_obj = self.find_net_obj_by_name(net_obj_name1)
        if first_obj is None:
            raise Exception("Network object name " + net_obj_name1 + " not found.")
        second_obj = self.find_net_obj_by_name(net_obj_name2)
        if second_obj is None:
            raise Exception("Network object name " + net_obj_name1 + " not found.")
        if isinstance(second_obj, NetObjectGroup) and isinstance(first_obj, NetObjectGroup):
            if len(second_obj.get_net_object_names()) == 0 or len(first_obj.get_net_object_names()) == 0:
                raise Exception("Cannot compare empty net group objects.")
            for sec_obj_name in second_obj.get_net_object_names():
                # every inner net object must have a match in a other net group object
                second_inner_obj = self.find_net_obj_by_name(sec_obj_name)
                is_match = False
                for first_obj_name in first_obj.get_net_object_names():
                    first_inner_obj = self.find_net_obj_by_name(first_obj_name)
                    if first_inner_obj.is_subset(second_inner_obj.address):
                        is_match = True
                        break
                if not is_match:
                    return False
            return True
        elif isinstance(second_obj, NetObjectGroup) and isinstance(first_obj, NetObject):
            if len(second_obj.get_net_object_names()) == 0:
                raise Exception("Cannot compare empty net group objects.")
            for sec_obj_name in second_obj.get_net_object_names():
                # every inner net object must have a match in a net object
                second_inner_obj = self.find_net_obj_by_name(sec_obj_name)
                if not first_obj.is_subset(second_inner_obj):
                    return False
        elif isinstance(second_obj, NetObject) and isinstance(first_obj, NetObjectGroup):
            for name in first_obj.get_net_object_names():
                obj = self.find_net_obj_by_name(name)
                if obj.is_subset(second_obj.address):
                    return True
        elif isinstance(second_obj, NetObject) and isinstance(first_obj, NetObject):
            return first_obj.is_subset(second_obj.address)
        else:
            raise Exception("It shout not have come here")
        return False

    def __str__(self):
        response = [str(x) + " " + str(self.__net_objects_dict[x]) for x in
                    self.__net_objects_dict.keys()]
        return str(response)


class NetObjectBase(object):
    """Abstract class for net objects."""
    def __init__(self, name):
        self.name = name


class NetObjectGroup(NetObjectBase):
    def __init__(self, name, net_object_names, description=None):
        """
        :param name string
        :param net_objects list of net object names
        """
        super().__init__(name)
        self.__net_object_names = net_object_names
        self.description = description

    def add_net_object_name(self, name_to_add):
        if name_to_add in self.__net_object_names:
            raise Exception("Cannot add net object name " + name_to_add + ". Already there.")
        self.__net_object_names.append(name_to_add)

    def get_net_object_names(self):
        # returning a copy of list
        return list(self.__net_object_names)

    def __str__(self):
        return self.name + " " + str([str(netobj) for netobj in self.__net_object_names])

    def __eq__(self, other):
        if not isinstance(other, NetObjectGroup):
            return False
        return compare_lists_of_objects(self.__net_object_names, other.get_net_object_names())


class NetObject(NetObjectBase):
    def __init__(self, name, address):
        """
        :param name: string
        :param address: IP address object
        """
        super().__init__(name)
        self.address = address

    def __str__(self):
        return self.name + " " + self.address.get_str_with_mask()

    def __eq__(self, other):
        if not isinstance(other, NetObject):
            return False
        return self.address == other.address

    def is_subset(self, ip_address_object):
        """
        Args:
            ip_address_object: IP object

        Returns: True if IPs are same or are a subset
        """
        if self.address is None or not isinstance(ip_address_object, IPAddress):
            return False
        if ip_address_object is None:
            raise Exception("Cannot compare an empty ip address!")
        return self.address.is_ip_object_match(ip_address_object)

    def is_string_ips_match(self, ip_address):
        """
        Args:
            ip_address: IP string

        Returns: True if IPs are same or are a subset
        """
        if self.address is None:
            return False
        if ip_address is None:
            raise Exception("Cannot compare an empty ip address!")
        return self.address.is_string_ip_match(ip_address)


def compare_lists_of_objects(fist_list, second_list):
    if fist_list is None or second_list is None:
        raise Exception("Cannot compare None objects.")
    if not isinstance(fist_list, list) or not isinstance(second_list, list):
        raise Exception("Cannot compare objects other than lists!")
    if len(fist_list) != len(second_list):
        return False
    for x in fist_list:
        if x not in second_list:
            return False
    return True
