import logging

from Implementation.firewall_rule_set import IPAddress, MatchRule, FirewallRule, Action, \
    FirewallRuleSet
from Implementation.network_object_holder import NetObjectHolder, NetObject
from Implementation.service_holder import service_holder, ServiceEnum, Application
from Implementation.enums import ActionsEnum, RuleType, AnyIP


class PerunParser(object):
    """
        Class for working witch Perun rules.
        Parse rules to abstract classes structure.
    """

    def __init__(self, perun_rules, zone_objects):
        self.perun_rules = perun_rules
        self.rules_to_src_net_objs = {}
        self.rules_to_dst_net_objs = {}
        self.net_object_holder = NetObjectHolder()
        self.zone_objects = zone_objects
        self.rule_sets = []

    def parse(self):
        # first go through all rules and parse them
        for one_rule in self.perun_rules["rules"]:
            logging.info("Parsing perun rule " + str(one_rule))
            rule = one_rule['rule']
            dst_ip_address, port_service = rule.split(':')
            dst_net_obj = NetObject(name='perunObject' + str(len(self.rules_to_dst_net_objs)), address=IPAddress(dst_ip_address))
            if rule in self.rules_to_dst_net_objs:
                raise Exception("Perun rules contain duplicity of the rule: " + rule)
            self.rules_to_dst_net_objs[rule] = dst_net_obj
            self.net_object_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
            self.resolve_src_net_objs(one_rule)
        # now lets create rule objects
        cnt_rule = 0
        for rule in self.rules_to_src_net_objs:
            logging.info("Creating firewall rule set object from rule " + str(rule))
            dst_ip_address, port_service = rule.split(':')
            dst_ip_address = IPAddress(dst_ip_address)
            application = self.get_application(port_service, rule)
            for src_net_obj in self.rules_to_src_net_objs[rule]:
                logging.info("Creating firewall rule and match object from rule " + str(rule))
                rule_set = self.resolve_rule_set(dst_ip_address, src_net_obj.get_net_object_names())
                rule_to_add = MatchRule(src_net_obj_name=src_net_obj.name,
                                        dst_net_obj_name=self.rules_to_dst_net_objs[rule].name,
                                        application=application)

                rule_set.rules.append(FirewallRule("Perun_rule" + str(cnt_rule),
                                                   rule_set.rule_set_name,
                                                   rule_to_add,
                                                   Action(action=ActionsEnum.PERMIT),
                                                   rule_type=RuleType.NEW_PERUN))
                cnt_rule += 1

    def get_application(self, port_service, rule):
        port, transport_protocol = port_service.split(',')
        service = service_holder.find_by_port_number(int(port))
        check_transport_protocol(service, transport_protocol, rule)
        protocol = service_holder.find_by_name(transport_protocol)
        application_name = "perun" + transport_protocol + str(port)
        return Application(name=application_name, protocol=protocol, service_protocol=service)

    def resolve_src_net_objs(self, one_rule):
        src_ips = [IPAddress(ip) for ip in sorted(one_rule["allowedIPs"])]
        logging.info("Resolving source IPs " + str([str(x) for x in src_ips]))
        zones_to_src_ips = {}
        for ip in src_ips:
            zone = self.get_zone_by_ip(ip)
            if zone is None:
                raise Exception("Perun input incorrect: source IP " + str(ip) + " not in a firewall zone.")
            # one zone object may have multiple net objects
            # for example inside zone may have two networks 10.1.1.0 and 10.1.2.0
            if zone not in zones_to_src_ips:
                zones_to_src_ips[zone] = [ip]
            else:
                zones_to_src_ips[zone].append(ip)
        src_net_obj_groups_per_zones_for_one_address_group = []
        logging.info("Creating source net objects from source IPs " + str([str(x) for x in src_ips]))
        for zone in zones_to_src_ips:
            # using len to number the obj names; using sorting for easier array comparisons
            src_net_obj_for_one_zone = self.resolve_src_net_obj_group(zones_to_src_ips[zone])
            src_net_obj_groups_per_zones_for_one_address_group.append(src_net_obj_for_one_zone)
        self.rules_to_src_net_objs[one_rule['rule']] = src_net_obj_groups_per_zones_for_one_address_group

    def resolve_src_net_obj_group(self, src_ips_per_zone):
        # if net_obj and net_obj_group with same addresses exists, return it
        # if not, create a new one
        found_object_group_name = self.net_object_holder.find_net_object_group_name_by_ip_address_objects(src_ips_per_zone)
        if found_object_group_name is not None:
            return self.net_object_holder.find_net_obj_group(found_object_group_name)
        net_object_names = self.get_src_net_objects(src_ips_per_zone)
        name = 'perunObjectGroup' + str(len(self.net_object_holder.get_net_object_groups()))
        self.net_object_holder.add_net_obj_group(name, net_object_names)
        return self.net_object_holder.find_net_obj_by_name(name)

    def get_src_net_objects(self, src_ips):
        src_net_objs_names = []
        for src_ip in src_ips:
            found_net_obj_name = self.net_object_holder.find_net_object_name_by_ip_address_objects(src_ip)
            if found_net_obj_name is not None:
                # add only if not already there - we cannot have duplicate net objects
                if found_net_obj_name not in src_net_objs_names:
                    src_net_objs_names.append(found_net_obj_name)
                continue
            name = "perunObject" + str(src_ip).split("/")[0] + "slash" + str(src_ip.ip_address.max_prefixlen)
            new_net_obj = NetObject(name=name, address=src_ip)
            self.net_object_holder.add_net_obj(name, new_net_obj)
            src_net_objs_names.append(name)
        return src_net_objs_names

    def resolve_rule_set(self, dst_ip_address, source_net_object_names):
        src_obj, dst_obj = self.resolve_src_and_dst_objs(dst_ip_address, source_net_object_names)
        for rule_set in self.rule_sets:
            if rule_set.from_object == src_obj and rule_set.to_object == dst_obj:
                return rule_set
        new_ruleset = FirewallRuleSet(rule_set_name="Perun_rule_set" + str(len(self.rule_sets)),
                                      from_object=src_obj, to_object=dst_obj, rules=[],
                                      rule_type=RuleType.NEW_PERUN)
        self.rule_sets.append(new_ruleset)
        return new_ruleset

    def resolve_src_and_dst_objs(self, dst_ip_address, source_net_object_names):
        dst_zone = self.get_zone_by_ip(dst_ip_address)
        if dst_zone is None:
            raise Exception("Destination IP address " + str(dst_ip_address) + " does not have a zone in firewall.")
        src_net_obj = self.net_object_holder.find_net_obj_by_name(source_net_object_names[0])
        # get zone out of the first IP. All net objects from 1 group should be from same zone
        src_zone = self.get_zone_by_ip(src_net_obj.address)
        if src_zone is None:
            raise Exception("Destination IP address " + source_net_object_names[0] + " does not have a zone in firewall.")
        # let's check if all net objects are from the same zone
        for src_name in source_net_object_names:
            src_obj = self.net_object_holder.find_net_obj_by_name(src_name)
            if self.get_zone_by_ip(src_obj.address) != src_zone:
                raise Exception("Perun input incorrect: 1 group of IPs cannot be from different zones")
        return src_zone, dst_zone

    def get_zone_by_ip(self, ip_to_match):
        default_zone = None
        for zone in self.zone_objects:
            for ip in zone.ips:
                if ip == AnyIP.ANYIP:
                    default_zone = zone
                elif ip == IPAddress("0.0.0.0/0"):
                    pass
                elif ip != AnyIP.ANYIP and ip.is_ip_object_match(ip_to_match):
                    return zone

        for zone in self.zone_objects:
            for ip in zone.ips:
                if IPAddress("0.0.0.0/0") == ip:
                    return zone
        return default_zone


def check_transport_protocol(service, perun_transport_protocol, whole_rule):
    try:
        ServiceEnum(perun_transport_protocol.upper())
    except ValueError:
        raise Exception("Perun input transport protocol not exist in rule: "
                        + str(whole_rule))

    if service.get_name() == ServiceEnum.UNKNOWN:
        # if it is unknown we have nothing to check here
        return 0
    parents = service.get_parents()
    first_parent_match = perun_transport_protocol == parents[0].get_name().name.lower()
    second_parent_match = len(parents) > 1 and perun_transport_protocol == parents[1].get_name()
    if not first_parent_match and not second_parent_match:
        raise Exception("Perun input contains incorrect transport protocol assignment in rule: "
                        + str(whole_rule))
