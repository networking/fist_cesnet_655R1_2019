"""
Helper functions for manipulating with files and firewalls.
"""
import enum
from netaddr import IPAddress
import argparse
import json
import os
import logging.handlers

def netmask_to_slash(netmask):
    """
    Convert netmask to slash form.
    :param netmask: network mask in shape [0..255].[0..255].[0..255].[0..255]
    :return: slash form of netmask, e.g.: 255.255.255.255 -> 32
    """

    return IPAddress(netmask).netmask_bits()


class CommunicationError(Exception):
    """Exception raised for errors in communication

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        super(CommunicationError, self).__init__(message)
        self.message = message


class FirewallConfigurator(object):
    """
    Object for manipulation with firewall.
    """

    # TODO aky ma vyznam to ze je static? podla mna to netreba, ci?
    @staticmethod
    def ansible_handle(directory, tags):
        """Run ansible-playbook with specified tags, return the return value.

        directory -- the root directory of the playbooks - where manage-fw.yml lives
        tags -- which tags should be run, see manage-fw.yml to determine This
        """
        exit_code = os.system('ansible-playbook -vvv -i ' + directory
                         + '../ansible-setting/ansible_inventory '
                         + directory + 'manage-fw.yml -t ' + tags)
        if tags == AnsibleHandlePlay.version_asa or tags == AnsibleHandlePlay.version_srx:
            # those are used for determining firewall type, so it is expected to fail sometimes
            return exit_code
        if exit_code != 0:
            raise Exception("Error: ansible failed. Look to logs to find out more.")


class AnsibleHandlePlay(str, enum.Enum):
    """
    Name of the play such as 'version_asa' used in ansible_handle. Tags from manage-fw.yml.
    """
    get_asa = 'get_asa'
    deploy_asa = 'deploy_asa'
    get_srx = 'get_srx'
    deploy_srx = 'deploy_srx'
    get_ostack = 'get_ostack'
    deploy_ostack = 'deploy_ostack'
    version_asa = 'version_asa'
    version_srx = 'version_srx'
    version_ostack = 'version_ostack'
    cleanup = 'cleanup'
    always = 'always'
    prepare_asa = 'prepare_asa'
    clear_asa = 'clear_asa'
    clear_srx = 'clear_srx'


class FirewallTypes(str, enum.Enum):
    """
    Enum of firewall types
    asa = Cisco
    srx = Juniper
    """
    asa = 'asa'
    srx = 'srx'


def get_firewall_type(directory):
    """
    Try to communicate with the FW, requires to have prepared files in
    order to determine how to connect to the FW.
    :param directory: directory of logger, e.g.:"Implementation/manage-fw/"
    :return: enum from class FirewallTypes, e.g.: FirewallTypes.asa, FirewallTypes.srx,
     FirewallTypes.ostack
    """
    configurator = FirewallConfigurator()
    if configurator.ansible_handle(directory, AnsibleHandlePlay.version_asa.name) == 0:
        return FirewallTypes.asa
    elif configurator.ansible_handle(directory, AnsibleHandlePlay.version_srx.name) == 0:
        return FirewallTypes.srx

    raise CommunicationError("Error unsupported FW, or communication was not successful")

# TODO try a except by tu nemali byt....program ma padnut ak mame zly vstup
def open_file_as_read(path, logger):
    """
    Open file for reading.
    """
    try:
        with open(path, "r") as source:
            opened_file = json.load(source)
        logger.info("Read file " + path)
        logger.debug("File content: " + str(opened_file))
        return opened_file
    except json.JSONDecodeError as ex:
        print("ERROR " + ex.msg)
        return None


def parsing_parameters():
    arg_parser = argparse.ArgumentParser(description='FIST - "Firewall rules from Identity - Simple Tranlator"')
    arg_parser.add_argument('-o', default=1, type=int, help='Integer value of the optimization level,'
                                                            ' the higher the better, currently only 1 is supported.'
                                                            ' Values should be in range <0-4> ', metavar="OPT_LEVEL")
    arg_parser.add_argument('-l', help='Usage "-l LOGFILE" logging into specific file', type=str, metavar="LOGFILE")
    arg_parser.add_argument('-r', required=True, help='Usage "-r INPUT_FILE" file containing data from Perun (json)',
                            type=str, metavar="INPUT_FILE")
    arg_parser.add_argument('-d', help='Debug messages are printed', action='store_true')
    arg_parser.add_argument('-w', help='sleep', action='store_true')  # used for kill test
    args = arg_parser.parse_args()
    return arg_parser, args


def read_input_data(file_path, logger):
    with open(file_path, "r") as source:
        objects = json.load(source)
        logger.info("Input file is loaded")
        logger.debug("File content: " + str(objects))
        return objects


def define_logger(log_file):
    logger = logging.getLogger('fist')
    logger.setLevel(logging.DEBUG)
    if log_file is not None:
        # always write everything to the rotating log files
        log_file_handler = logging.handlers.TimedRotatingFileHandler(log_file, when='M', interval=2)
        log_file_handler.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s](%(name)s:%(funcName)s:%(lineno)d)'
                                                        ': %(message)s'))
        log_file_handler.setLevel(logging.DEBUG)
        logger.addHandler(log_file_handler)
    # also log to the console at a level determined by the --verbose flag
    console_handler = logging.StreamHandler()  # sys.stderr
    return logger, console_handler


def update_logger(arg_parser, args, console_handler, logger):
    if args is not None and args.w:
        os.system('sleep 10000')
    if args is not None and args.d:
        console_handler.setLevel(logging.DEBUG)
    else:
        console_handler.setLevel(logging.INFO)
    # -1 for no optimization
    if args is not None and (args.o < -1 or args.o > 4):
        arg_parser.error("Exiting due to not supported value of argument")
    console_handler.setFormatter(logging.Formatter('[%(levelname)s](%(name)s): %(message)s'))
    logger.addHandler(console_handler)
    #logger.info("The optimization level is %d", args.o)