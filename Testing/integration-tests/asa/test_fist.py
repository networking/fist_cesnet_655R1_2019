import logging
import os
import re
import sys


def parse_acls_output(acls_json):
    acls_json = re.sub("\], \[", "\n", acls_json)
    acls_json = re.sub(" \", ", "\n", acls_json)
    acls_json = re.sub("\", ", "\n", acls_json)
    acls_json = re.sub("\",", "\n", acls_json)
    acls_json = re.sub("\[", "", acls_json)
    acls_json = re.sub("\]", "", acls_json)
    acls_json = re.sub("<", "", acls_json)
    acls_json = re.sub("\"", "", acls_json)
    acls_json = re.sub("- ", "", acls_json)
    return acls_json


def parse_objects_output(objects_json):
    objects_json = parse_acls_output(objects_json)
    objects_json = re.sub("  ", "", objects_json)
    return objects_json


def compare_acls(acls_json_output_file_path, expected_output_file_path):
    logger = logging.getLogger('fist')
    logger.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()  # sys.stderr
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('[%(levelname)s](%(name)s): %(message)s'))
    logger.addHandler(console_handler)

    with open(acls_json_output_file_path, 'r') as file:
        acls_json_output = file.read()
        print("Reading from: " + acls_json_output_file_path)
        print("Actual before parse: " + str(acls_json_output))
    parsed_acls_json_output = parse_acls_output(acls_json_output).split("\n")
    remove_line_from_parsed(parsed_acls_json_output, "")
    print("Actual: " + str(parsed_acls_json_output))

    #going to expected result
    with open(expected_output_file_path, 'r') as file:
        expected_data = file.read().split("\n")
    remove_line_from_parsed(expected_data, '')
    print("Expected: " + str(expected_data))

    for i in range(0, len(expected_data)):
        logger.info("Checking line " + str(i))
        logger.info("expected acl: " + expected_data[i])
        logger.info("output acl:   " + parsed_acls_json_output[i])
        assert expected_data[i] == parsed_acls_json_output[i]
    logger.info("Checking length")
    logger.info("Length expected acl: " + str(len(expected_data)))
    logger.info("Length output acl:   " + str(len(parsed_acls_json_output)))
    assert len(expected_data) == len(parsed_acls_json_output)


def compare_objects(actual_applications_path, expected_applications_path):
    logger = logging.getLogger('fist')

    with open(actual_applications_path, 'r') as file:
        actual_objects = file.read()
        print("Reading from: " + actual_applications_path)
        print("Actual before parse: " + str(actual_objects))
    actual_objects_parsed = parse_objects_output(actual_objects).split("\n")
    remove_line_from_parsed(actual_objects_parsed, "")
    print("Actual: " + str(actual_objects_parsed))

    #going to expected result
    with open(expected_applications_path, 'r') as file:
        expected_data = file.read().split("\n")
    remove_line_from_parsed(expected_data, '')
    print("Expected: " + str(expected_data))

    for i in range(0, len(expected_data)):
        logger.info("Checking line " + str(i))
        logger.info("expected object: " + expected_data[i])
        logger.info("actual object:   " + actual_objects_parsed[i])
        assert expected_data[i] == actual_objects_parsed[i]
    logger.info("Checking length")
    logger.info("Length expected objects: " + str(len(expected_data)))
    logger.info("Length output objects:   " + str(len(actual_objects_parsed)))
    assert len(expected_data) == len(actual_objects_parsed)


def remove_line_from_parsed(actual_objects_parsed, to_remove):
    while to_remove in actual_objects_parsed:
        actual_objects_parsed.pop(actual_objects_parsed.index(to_remove))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        os.system('ansible-playbook -i ./Testing/integration-tests/asa/ansible_inventory ./Testing/integration-tests/asa/get_acls.yml')
        compare_acls("./Testing/integration-tests/asa/acl_output.txt", sys.argv[1])
    if len(sys.argv) > 2:
        os.system('ansible-playbook -i ./Testing/integration-tests/asa/ansible_inventory ./Testing/integration-tests/asa/get_objects.yml')
        compare_objects("./Testing/integration-tests/asa/objects_and_group_objects.txt", sys.argv[2])

