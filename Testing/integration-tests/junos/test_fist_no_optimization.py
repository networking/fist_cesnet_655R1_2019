import os
import re
import logging
import json
import fist

from Implementation.helper_functions import FirewallConfigurator, AnsibleHandlePlay

def parse_policies_output(acls_json):
    acls_json = re.sub(" \", ", "\n", acls_json)
    acls_json = re.sub("\", ", "\n", acls_json)
    acls_json = re.sub("\",", "\n", acls_json)
    acls_json = re.sub("\[", "", acls_json)
    acls_json = re.sub("\]", "", acls_json)
    acls_json = re.sub("<", "", acls_json)
    acls_json = re.sub("\"", "", acls_json)
    return acls_json


def compare_acls(policies_json_output_file_path, expected_output_file_path):
    logger = logging.getLogger('fist')
    logger.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()  # sys.stderr
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('[%(levelname)s](%(name)s): %(message)s'))
    logger.addHandler(console_handler)

    with open(policies_json_output_file_path, 'r') as file:
        policies_json_output = file.read()

    parsed_policies_json_output = parse_policies_output(policies_json_output).split("\n")

    # going to expected result
    with open(expected_output_file_path, 'r') as file:
        expected_data = file.read().split("\n")
    # assert len(parsed_policies_json_output) == len(expected_data)
    # in the output from juniper, there is an empty first line
    for x in range(1, len(parsed_policies_json_output)):
        logger.info("Checking line " + str(x))
        logger.info("Actual: " + parsed_policies_json_output[x])
        logger.info("Expected: " + expected_data[x-1])

        assert parsed_policies_json_output[x] == expected_data[x-1]

if __name__ == '__main__':
    os.system('echo PREPARING JUNOS CONFIG')
    os.system('ansible-playbook -i ./Testing/integration-tests/junos/ansible_inventory ./Testing/integration-tests/junos/provision_global.yml')
    #os.system('ansible-playbook -i hosts get_acls.yml')
    os.system('echo PARSING THE INPUT FROM FIREWALL')
    firewall_configurator = FirewallConfigurator()
    global_type, firewall_parser = fist.parse_firewall(firewall_configurator, fist.logger)
    with open('/tmp/perun-testing/route-info.json') as json_file:
        routes_json = json.load(json_file)
        print("Juniper route input is " + str(routes_json))

    print(json.dumps(firewall_parser.abstract_to_policies(firewall_parser.parsed_rule_sets),
                     indent=4, sort_keys=False))
    os.system('echo DEPLOYING RULES')
    firewall_parser.deploy_services(firewall_parser.applications_holder)
    firewall_parser.deploy_net_objects(firewall_parser.net_object_holder)
    firewall_parser.deploy_policies(firewall_parser.parsed_rule_sets)
    ansible_firewall_configurator = FirewallConfigurator()
    firewall_configurator.ansible_handle(firewall_parser.exec_dir,
                                         tags=AnsibleHandlePlay.clear_srx.name)
    firewall_parser.deploy_remote_hard(ansible_firewall_configurator)
    print("Current policies:")
    with open("./Implementation/manage-fw/roles/deploy-all-srx/vars/rules.yml", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)

    os.system('ansible-playbook -i ./Testing/integration-tests/junos/ansible_inventory ./Testing/integration-tests/junos/get_policies.yml')
    with open("policies_output", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)
    compare_acls("policies_output", "./Testing/integration-tests/junos/expected_policies_without_changes")