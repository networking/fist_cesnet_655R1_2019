import json
import os
import fist
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.helper_functions import FirewallConfigurator, AnsibleHandlePlay
from Implementation.firewall_rule_set import Interface, ZoneObject, IPAddress
from Implementation.perun_parser import PerunParser
from test_fist_no_optimization import compare_acls
from Implementation.firewall_juniper_parser import FirewallParserJuniper

if __name__ == '__main__':
    os.system('echo PARSING THE INPUT FROM FIREWALL')
    print("Current policies:")
    with open("./Implementation/manage-fw/roles/deploy-all-srx/vars/rules.yml", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)

    os.system(
        'ansible-playbook -i ./Testing/integration-tests/junos/ansible_inventory ./Testing/integration-tests/junos/get_policies.yml')
    with open("policies_output", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)
    compare_acls("policies_output", "./Testing/integration-tests/junos/expected_merged_policies")
