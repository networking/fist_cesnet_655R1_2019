import json
import os

import fist
from Implementation.helper_functions import FirewallConfigurator, AnsibleHandlePlay, \
    parsing_parameters, define_logger, update_logger
from test_fist_no_optimization import compare_acls

if __name__ == '__main__':
    print("Current policies:")
    with open("./Implementation/manage-fw/roles/deploy-all-srx/vars/rules.yml", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)

    os.system('ansible-playbook -i ./Testing/integration-tests/junos/ansible_inventory ./Testing/integration-tests/junos/get_policies.yml')
    with open("policies_output", 'r') as file:
        policies_json_output = file.read()
        print(policies_json_output)
    compare_acls("policies_output", "./Testing/integration-tests/junos/expected_policies_from_real_production")