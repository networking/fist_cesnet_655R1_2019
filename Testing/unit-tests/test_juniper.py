import copy
import json
import logging
import os
import pathlib
import re
import unittest

import yaml

from Implementation.enums import AnyIP, ActionsEnum
from Implementation.firewall_juniper_parser import FirewallParserJuniper
from Implementation.firewall_rule_set import get_address, IPAddress, \
    ZoneObject
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.network_object_holder import NetObject
from Implementation.perun_parser import PerunParser
from Implementation.firewall_cisco_parser import FirewallParserCisco


class NewFirewallJuniperTests(unittest.TestCase):
    juniper_parser = None
    json_policies = None
    this_folder = os.path.dirname(os.path.abspath(__file__))

    def setUp(self):
        with open(os.path.join(self.this_folder, 'resources/juniper_policies.json')) as json_file:
            self.json_policies = json.load(json_file)
        with open(os.path.join(self.this_folder, './resources/juniper_zones.json')) as json_file:
            self.json_zones = json.load(json_file)
        with open(os.path.join(self.this_folder,
                               './resources/juniper_applications.json')) as json_file:
            self.json_applications = json.load(json_file)
        with open(os.path.join(self.this_folder,
                               './resources/juniper_address_book.json')) as json_file:
            self.json_address_book = json.load(json_file)
        with open(os.path.join(self.this_folder,
                               './resources/juniper_routes_info.json')) as json_file:
            self.json_routes_info = json.load(json_file)
        with open(os.path.join(self.this_folder,
                               './resources/juniper_interfaces.json')) as json_file:
            self.json_interfaces = json.load(json_file)
        exec_dir = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        self.juniper_parser = FirewallParserJuniper(self.json_policies, self.json_zones,
                                                    self.json_applications, self.json_address_book,
                                                    self.json_interfaces, self.json_routes_info,
                                                    exec_dir)

    def prepare_cisco(self):
        with open(os.path.join(self.this_folder, './resources/cisco_policies.json')) as json_file:
            cisco_policies = json.load(json_file)
        with open(os.path.join(self.this_folder, './resources/route-info.json')) as json_file:
            cisco_routes = json.load(json_file)
        with open(os.path.join(self.this_folder, './resources/address-book.json')) as json_file:
            cisco_address_book = json.load(json_file)
        with open(os.path.join(self.this_folder, './resources/zones.json')) as json_file:
            cisco_zones = json.load(json_file)
        exec_dir = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        return FirewallParserCisco(cisco_policies, cisco_address_book, cisco_routes, cisco_zones,
                                   exec_dir)

    #  Destination and source address is probably name in address-book here changed.
    def test_parse_policies(self):
        self.setUp()
        parsed_policies = self.juniper_parser.parse()
        self.assertEqual(len(parsed_policies),
                         len(self.json_policies['policy']))
        self.assertEqual(parsed_policies[0].from_object.name, 'outside')
        self.assertEqual(parsed_policies[0].from_object.ips, [IPAddress("10.1.2.0/24")])
        self.assertEqual(parsed_policies[0].to_object.name, 'dmz2')
        self.assertEqual(len(parsed_policies[0].to_object.ips), 2)
        self.assertEqual(parsed_policies[0].to_object.ips, [IPAddress("147.251.50.0/24"),
                                                            IPAddress("147.251.54.0/24")])
        self.assertEqual(len(parsed_policies[0].rules), 4)
        self.assertEqual(parsed_policies[0].rules[0].action.action, ActionsEnum.PERMIT)
        self.assertEqual(parsed_policies[0].rules[0].match_rule.dst_net_obj_name, 'perunObj10.1.1.5slash32')
        self.assertEqual(str(parsed_policies[0].rules[0].match_rule.application.protocol), 'tcp')
        self.assertEqual(get_address(parsed_policies[0].rules[0].match_rule.src_net_obj_name),
                         'any')
        self.assertEqual(str(parsed_policies[0].rules[0].rule_name), 'theSuperPolicy2')

    def test_parse_objects(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_zones()
        # - 2 because there are trust and untrast zones
        self.assertEqual(len(self.juniper_parser._zones_objects),
                         len(self.json_zones["security-zone"]) - 2)
        self.assertEqual(self.juniper_parser._zones_objects[0].name, 'inside')
        self.assertEqual(self.juniper_parser._zones_objects[0].ips,
                         [IPAddress("10.1.1.0/24"), IPAddress("10.1.3.0/24")])

    def test_parse_actions(self):
        self.setUp()
        then = {"permit": "",
                "log": {"session-init": "",
                        "session-close": ""}}
        action = self.juniper_parser._FirewallParserJuniper__parse_actions(then)
        self.assertEqual(action.action, ActionsEnum.PERMIT)
        self.assertEqual(action.other_functionality.logs, ["session-init", "session-close"])

    def test_find_object(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_zones()
        self.assertEqual(self.juniper_parser._FirewallParserJuniper__find_zone_object("inside"),
                         self.juniper_parser._zones_objects[0])

    def test_parse_juniper_single_rule(self):
        single_rule = {"name": "theSuperPolicy",
                       "match": {"source-address": "any",
                                 "destination-address": "142.0.2.20",
                                 "application": "peruntcp22"},
                       "then": {"permit": "",
                                "log": {"session-init": "",
                                        "session-close": ""}}}
        rule_set_name = "from inside to outside"
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_applications_and_application_sets()
        rule = self.juniper_parser._FirewallParserJuniper__parse_rule(single_rule, rule_set_name)
        self.assertEqual(rule.rule_set_name, rule_set_name)
        self.assertEqual(rule.rule_name, single_rule['name'])
        self.assertEqual(rule.action.action, ActionsEnum.PERMIT)
        self.assertEqual(rule.action.other_functionality.logs, ["session-init", "session-close"])
        self.assertEqual(get_address(rule.match_rule.src_net_obj_name), AnyIP.ANYIP)
        self.assertEqual(str(rule.match_rule.application.name), "peruntcp22")
        self.assertEqual(str(rule.match_rule.application.protocol), "tcp")

    def test_convert_rule_set_to_json(self):
        self.setUp()
        parsed_policies = self.juniper_parser.parse()
        self.assertEqual(len(parsed_policies), 12)
        back_to_json = self.juniper_parser.abstract_to_policies(parsed_policies)
        # ip addresses are in slash form in origin json not
        self.assertEqual(back_to_json['policies']['policy'],
                         self.json_policies['policy'])

    def test_deploy_policies(self):
        self.setUp()
        parsed_policies = self.juniper_parser.parse()
        self.juniper_parser.deploy_policies(parsed_policies)
        with open(
                self.juniper_parser.exec_dir + "/roles/deploy-all-srx/vars/rules.yml") as rules_yaml:
            parsed_rules = yaml.load(rules_yaml, Loader=yaml.FullLoader)
        list_of_settings = parsed_rules.get("all_policies")

        self.assertEqual(
            "set security policies from-zone outside to-zone dmz2 policy theSuperPolicy2 match source-address any" +
            " destination-address perunObj10.1.1.5slash32 application myApp",
            list_of_settings[2])
        self.assertEqual(
            "set security policies from-zone outside to-zone dmz2 policy theSuperPolicy2 then permit",
            list_of_settings[3])
        self.assertEqual(
            "insert security policies from-zone outside to-zone dmz2 policy theSuperPolicy2 before policy defaultDeny",
            list_of_settings[4])
        self.assertEqual(
            "set security policies from-zone outside to-zone dmz2 policy theSuperPolicy then log session-close",
            list_of_settings[7])
        self.assertEqual(
            "set security policies from-zone outside to-zone dmz2 policy theSuperPolicy then log session-init",
            list_of_settings[8])

    def test_deploy_perun_services(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_applications_and_application_sets()
        self.juniper_parser.deploy_services(self.juniper_parser.applications_holder)
        with open(
                self.juniper_parser.exec_dir + "/roles/deploy-all-srx/vars/services.yml") as rules_yaml:
            parsed_services = yaml.load(rules_yaml, Loader=yaml.FullLoader)
        list_of_settings = parsed_services.get("services")
        self.assertEqual(
            "set applications application myApp protocol tcp destination-port 6633",
            list_of_settings[0])
        self.assertEqual(
            "set applications application peruntcp22 protocol tcp destination-port 22",
            list_of_settings[1])
        self.assertEqual(
            "set applications application peruntcp8082 protocol tcp destination-port 8082",
            list_of_settings[2])
        self.assertEqual(
            "set applications application peruntcp6633 protocol tcp destination-port 6633",
            list_of_settings[3])
        applications = self.json_applications['application']
        self.assertEqual(len(list_of_settings),
                         len(applications))

    def test_get_addresses_from_address_book(self):
        address_book = {'name': 'BOOK1',
                        'address': [{'name': 'myAddress1InTrust', 'ip-prefix': '10.1.1.15/32'},
                                    {'name': 'perunObj10.1.1.2slash32', 'ip-prefix': '10.1.1.2/32'},
                                    {'name': 'perunObj10.1.1.10slash32',
                                     'ip-prefix': '10.1.1.10/32'},
                                    {'name': 'perunObj10.1.1.5slash32', 'ip-prefix': '10.1.1.5/32'},
                                    {'name': 'perunObj10.1.2.2slash32',
                                     'ip-prefix': '10.1.2.2/32'}],
                        'address-set': [
                            {'name': 'obj2-1', 'address': [{'name': 'perunObj10.1.1.5slash32'},
                                                           {'name': 'perunObj10.1.1.2slash32'},
                                                           {'name': 'perunObj10.1.1.10slash32'}]},
                            {'name': 'obj0-1', 'address': [{'name': 'perunObj10.1.1.10slash32'},
                                                           {'name': 'perunObj10.1.1.2slash32'}]},
                            {'name': 'obj1-1', 'address': [{'name': 'perunObj10.1.1.2slash32'},
                                                           {'name': 'perunObj10.1.1.5slash32'}]},
                            {'name': 'obj3-1', 'address': {'name': 'perunObj10.1.2.2slash32'}}],
                        'attach': {'zone': {'name': 'inside'}}}
        self.setUp()
        net_objects = self.juniper_parser._FirewallParserJuniper__get_addresses_from_address_book(address_book)
        self.assertEqual(len(address_book['address']), len(net_objects))
        self.assertEqual(net_objects[list(net_objects)[0]].name, address_book['address'][0]['name'])
        address = net_objects[list(net_objects)[0]].address
        self.assertEqual(address.get_str_with_mask(), address_book['address'][0]['ip-prefix'])

    def test_get_address_sets_from_address_book(self):
        address_book = {'name': 'BOOK1',
                        'address': [{'name': 'myAddress1InTrust', 'ip-prefix': '10.1.1.15/32'},
                                    {'name': 'perunObj10.1.1.2slash32', 'ip-prefix': '10.1.1.2/32'},
                                    {'name': 'perunObj10.1.1.10slash32',
                                     'ip-prefix': '10.1.1.10/32'},
                                    {'name': 'perunObj10.1.1.5slash32', 'ip-prefix': '10.1.1.5/32'},
                                    {'name': 'perunObj10.1.2.2slash32',
                                     'ip-prefix': '10.1.2.2/32'}],
                        'address-set': [
                            {'name': 'obj2-1', 'address': [{'name': 'perunObj10.1.1.5slash32'},
                                                           {'name': 'perunObj10.1.1.2slash32'},
                                                           {'name': 'perunObj10.1.1.10slash32'}]},
                            {'name': 'obj0-1', 'address': [{'name': 'perunObj10.1.1.10slash32'},
                                                           {'name': 'perunObj10.1.1.2slash32'}]},
                            {'name': 'obj1-1', 'address': [{'name': 'perunObj10.1.1.2slash32'},
                                                           {'name': 'perunObj10.1.1.5slash32'}]},
                            {'name': 'obj3-1', 'address': {'name': 'perunObj10.1.2.2slash32'}}],
                        'attach': {'zone': {'name': 'inside'}}}
        self.setUp()
        address_dict = {}
        for address in address_book['address']:
            address_name = address['name']
            ip_address = address['ip-prefix']
            address_dict[address_name] = NetObject(address_name, ip_address)
        net_objects = self.juniper_parser._FirewallParserJuniper__get_address_sets_from_address_book(address_book,
                                                                             address_dict)
        self.assertEqual(len(address_book['address-set']), len(net_objects))
        self.assertEqual(net_objects[list(net_objects)[0]].name,
                         address_book['address-set'][0]['name'])
        address = net_objects[list(net_objects)[0]].get_net_object_names()[0]
        expected = address_dict[address_book['address-set'][0]['address'][0]['name']]
        self.assertEqual(address,
                         address_dict[address_book['address-set'][0]['address'][0]['name']].name)
        # test last element because there is the case where are not square brackets
        self.assertEqual(net_objects[list(net_objects)[3]].name,
                         address_book['address-set'][3]['name'])
        address = net_objects[list(net_objects)[3]].get_net_object_names()[0]
        self.assertEqual(address,
                         address_dict[address_book['address-set'][3]['address']['name']].name)

    def test_parse_application_protocol_service(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_applications_and_application_sets()
        original_applications = self.json_applications['application']
        # + 1 because there is default any application
        self.assertEqual(len(self.juniper_parser.applications_holder.get_applications()),
                         len(self.json_applications['application']) + 1)
        for application in original_applications:
            app = self.juniper_parser.applications_holder.find_application_by_name(
                application['name'])
            protocol = app.protocol
            service = app.service_protocol
            self.assertEqual(str(protocol), application['protocol'])
            self.assertEqual(service.port_number, int(application['destination-port']))
            self.assertEqual(app.name, application['name'])

    def test_parse_juniper_match_rule(self):
        self.setUp()
        match = {"source-address": "any",
                 "destination-address": "142.0.0.10",
                 "application": "myApp"}
        self.juniper_parser._FirewallParserJuniper__get_applications_and_application_sets()
        self.juniper_parser._FirewallParserJuniper__get_zones()
        match_rule = self.juniper_parser._FirewallParserJuniper__parse_match_rule(match)
        self.assertEqual(match_rule.application.name, match["application"])
        self.assertEqual(match_rule.src_net_obj_name, AnyIP.ANYIP)
        self.assertEqual(match_rule.dst_net_obj_name, "142.0.0.10")
        expected_service = self.juniper_parser.applications_holder.find_application_by_name(
            match["application"]).service_protocol
        self.assertEqual(match_rule.application.service_protocol, expected_service)

    def test_get_zone_net_objects(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_net_objects_holder()
        net_object_holder = self.juniper_parser.net_object_holder
        # plus one because any net object
        self.assertEqual(len(net_object_holder.get_net_objects()),
                         len(self.juniper_parser._address_book['address']) + 1)
        self.assertEqual(len(net_object_holder.get_net_object_groups()),
                         len(self.juniper_parser._address_book['address-set']))

        # single address
        self.assertEqual(list(net_object_holder.get_net_objects().values())[1].name,
                         "myAddress1InTrust")
        self.assertEqual((list(net_object_holder.get_net_objects().values())[1].address).get_str_without_mask(),
                         "10.1.1.15")
        # address set
        self.assertEqual(list(net_object_holder.get_net_object_groups().values())[0].name,
                         "perunObjectGroup0")
        self.assertEqual(
            list(net_object_holder.get_net_object_groups().values())[0].get_net_object_names()[0],
            "perunObj10.1.1.5slash32")
        # global address
        self.assertEqual(list(net_object_holder.get_net_objects().values())[15].name,
                         "perunObject3")
        self.assertEqual(list(net_object_holder.get_net_objects().values())[16].address.get_str_without_mask(),
                         "147.250.1.225")

    def test_convert_applications_back_to_json(self):
        expected_app = {
            "applications": {
                "application": [
                    {
                        "name": "myApp",
                        "protocol": "tcp",
                        "destination-port": "6633"
                    },
                    {
                        "name": "peruntcp22",
                        "protocol": "tcp",
                        "destination-port": "22"
                    }
                ]
            }
        }
        self.setUp()
        parsed_policies = self.juniper_parser.parse()
        application_json = self.juniper_parser.abstract_to_applications(parsed_policies)
        app1 = application_json["applications"]["application"][0]
        app2 = application_json["applications"]["application"][1]
        self.assertEqual(app1['name'], "myApp")
        self.assertEqual(app1['protocol'], "tcp")
        self.assertEqual(app1['destination-port'], '6633')
        self.assertEqual(app2['name'], "peruntcp22")
        self.assertEqual(app2['protocol'], "tcp")
        self.assertEqual(app2['destination-port'], '22')
        self.assertEqual(expected_app, application_json)

    def test_convert_address_books_back_to_json(self):
        self.setUp()
        self.juniper_parser._FirewallParserJuniper__get_net_objects_holder()
        json_add_book = self.juniper_parser.abstract_to_address_book(
            self.juniper_parser.net_object_holder)
        self.assertEqual(self.json_address_book["address-set"],
                         json_add_book["address-book"][0]["address-set"])

    def test_cisco_get_address_book(self):
        with open(os.path.join(self.this_folder,
                               './resources/expected_address_book.json')) as json_file:
            expected = json.load(json_file)
        fw_parser_cisco = self.prepare_cisco()
        fw_parser_cisco.parse()
        self.setUp()
        back_to_json = self.juniper_parser.abstract_to_address_book(
            fw_parser_cisco.net_object_holder)
        self.assertEqual(expected, back_to_json)

    def test_merge_juniper_rule_set_with_perun_rule_set(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/perun_rules_testing.json')) as json_file:
            perun_json_file = json.load(json_file)

        original_rule_set = copy.deepcopy(self.juniper_parser.parse())
        perun_parser = PerunParser(perun_json_file, self.juniper_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        original_perun_rule_set = copy.deepcopy(perun_parser.rule_sets)
        merged_rule_sets, application_holder, net_object_holder = merger.merge_rule_sets_all(
            self.juniper_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.juniper_parser.applications_holder,
            self.juniper_parser.net_object_holder,
            perun_parser.net_object_holder)

        # check merged_rule_sets
        for juniper_rule_set in original_rule_set:
            for perun_rule_set in perun_parser.rule_sets:
                for merged_rule_set in merged_rule_sets:
                    if juniper_rule_set.from_object == perun_rule_set.from_object:
                        if perun_rule_set.from_object == merged_rule_set.from_object:
                            if juniper_rule_set.to_object == perun_rule_set.to_object:
                                if perun_rule_set.to_object == merged_rule_set.to_object:
                                    merged_rules = len(merged_rule_set.rules)
                                    to_delete = len(merged_rule_set.to_delete)
                                    perun_rules = len(perun_rule_set.rules)
                                    juniper_rules = len(juniper_rule_set.rules)
                                    self.assertEqual(merged_rules + to_delete,
                                                     perun_rules + juniper_rules)

        # check merged_net_object_holder
        for rule_set in original_perun_rule_set:
            for rule in rule_set.rules:
                dst_object_name = rule.match_rule.dst_net_obj_name
                src_object_name = rule.match_rule.src_net_obj_name
                self.assertTrue(
                    self.check_net_object_contains_address(net_object_holder, perun_parser,
                                                           dst_object_name))
                self.assertTrue(
                    self.check_net_object_contains_address(net_object_holder, perun_parser,
                                                           src_object_name))

        # check application_holder
        for rule_set in perun_parser.rule_sets:
            for rule in rule_set.rules:
                application = rule.match_rule.application
                self.assertIn(application.name, application_holder.application_dict)

        print(json.dumps(self.juniper_parser.abstract_to_policies(merged_rule_sets), indent=4,
                         sort_keys=False))
        print(json.dumps(self.juniper_parser.abstract_to_address_book(net_object_holder), indent=4,
                         sort_keys=False))
        self.juniper_parser.deploy_policies(merged_rule_sets)
        self.juniper_parser.deploy_services(application_holder)
        self.juniper_parser.deploy_net_objects(net_object_holder)

    def check_net_object_contains_address(self, net_object_holder, perun_parser, net_object_name):
        net_object = perun_parser.net_object_holder.find_net_obj_by_name(net_object_name)
        if isinstance(net_object, NetObject):
            address = net_object.address
            found_obj = net_object_holder.find_net_obj_or_group_name_by_ip_address_objects(address)
        else:
            net_obj_names = net_object.get_net_object_names()
            searched_ips = []
            for net_obj_name in net_obj_names:
                searched_ips.append(
                    perun_parser.net_object_holder.get_net_objects()[net_obj_name].address)
            found_obj = net_object_holder.find_net_object_group_name_by_ip_address_objects(
                searched_ips)
        if found_obj is None:
            print(net_object_name + " False")
            return False
        print(net_object_name + " True")
        return True

    def test_merge_juniper_rule_set_with_perun_rule_set_difference(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/perun_rules_testing.json')) as json_file:
            perun_json_file = json.load(json_file)
        self.juniper_parser.parse()
        perun_parser = PerunParser(perun_json_file, self.juniper_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
            self.juniper_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.juniper_parser.applications_holder,
            self.juniper_parser.net_object_holder,
            perun_parser.net_object_holder)

        self.assertEqual(len(diff_rule_sets), 3)
        self.assertEqual(len(diff_rule_sets[0].rules), 2)
        self.assertEqual(len(diff_rule_sets[1].rules), 2)
        self.assertEqual(len(diff_rule_sets[2].rules), 2)
        self.assertEqual(len(diff_app_holder.get_applications()), 0)
        self.assertEqual(len(diff_net_obj_holder.get_net_objects()), 4)
        self.assertEqual(len(diff_net_obj_holder.get_net_object_groups()), 1)

        print(json.dumps(self.juniper_parser.abstract_to_policies(diff_rule_sets), indent=4,
                         sort_keys=False))
        print(
            json.dumps(self.juniper_parser.abstract_to_address_book(diff_net_obj_holder), indent=4,
                       sort_keys=False))
        self.juniper_parser.deploy_policies(diff_rule_sets)
        self.juniper_parser.deploy_services(diff_app_holder)
        self.juniper_parser.deploy_net_objects(diff_net_obj_holder)

    def test_parse_from_real_configuration(self):
        with open(os.path.join(self.this_folder,
                               './resources/juniper_configuration_from_real_production.json')) as json_file:
            config_json_file = json.load(json_file)

        with open(os.path.join(self.this_folder,
                               './resources/juniper_rout_info_from_ral_production.json')) as json_file:
            rout_info_json = json.load(json_file)
        exec_dir = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        add_book = config_json_file[0]["rpc-reply"]["configuration"]["security"]["address-book"]
        juniper_parser = FirewallParserJuniper(
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["policies"],
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["zones"],
            config_json_file[0]["rpc-reply"]["configuration"]["applications"],
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["address-book"],
            config_json_file[0]["rpc-reply"]["configuration"]["interfaces"]["interface"],
            rout_info_json,
            exec_dir)
        juniper_parser.parse()
        with open(os.path.join(self.this_folder,
                               './resources/inputTestingFromRealProduction.json')) as json_file:
            perun_json_file = json.load(json_file)
        perun_parser = PerunParser(perun_json_file, juniper_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        merged_rule_set, merged_application_holder, merged_net_objects_holder = \
            merger.merge_rule_sets_all(juniper_parser.parsed_rule_sets,
                                         perun_parser.rule_sets,
                                         juniper_parser.applications_holder,
                                         juniper_parser.net_object_holder,
                                         perun_parser.net_object_holder)
        print(json.dumps(self.juniper_parser.abstract_to_policies(
            merged_rule_set), indent=4, sort_keys=False))
        juniper_parser.deploy_policies(merged_rule_set)

    def test_deploy_policies_from_real_configuration(self):
        with open(os.path.join(self.this_folder,
                               './resources/juniper_configuration_from_real_production.json')) as json_file:
            config_json_file = json.load(json_file)

        with open(os.path.join(self.this_folder,
                               './resources/juniper_rout_info_from_ral_production.json')) as json_file:
            rout_info_json = json.load(json_file)
        exec_dir = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        add_book = config_json_file[0]["rpc-reply"]["configuration"]["security"]["address-book"]
        juniper_parser = FirewallParserJuniper(
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["policies"],
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["zones"],
            config_json_file[0]["rpc-reply"]["configuration"]["applications"],
            config_json_file[0]["rpc-reply"]["configuration"]["security"]["address-book"],
            config_json_file[0]["rpc-reply"]["configuration"]["interfaces"]["interface"],
            rout_info_json,
            exec_dir)
        juniper_parser.parse()
        juniper_parser.deploy_policies(juniper_parser.parsed_rule_sets)
        self.compare_deploy_files(self.juniper_parser.exec_dir + "/roles/deploy-all-srx/vars/rules.yml",
                     "./resources/juniper_expected_deploy_policies_from_real_production",
                     "all_policies")
        juniper_parser.deploy_services(juniper_parser.applications_holder)
        self.compare_deploy_files(self.juniper_parser.exec_dir + "/roles/deploy-all-srx/vars/services.yml",
                     "./resources/juniper_expected_deploy_services_from_real_production", "services")
        juniper_parser.deploy_net_objects(juniper_parser.net_object_holder)
        self.compare_deploy_files(self.juniper_parser.exec_dir + "/roles/deploy-all-srx/vars/net_objs.yml",
                     "./resources/juniper_expected_deploy_net_objs_from_real_production",
                     "perun_processed_net_objs")

    def compare_deploy_files(self, policies_json_output_file_path, expected_output_file_path, get_key):
        logger = logging.getLogger('fist')
        logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler()  # sys.stderr
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(logging.Formatter('[%(levelname)s](%(name)s): %(message)s'))
        logger.addHandler(console_handler)

        with open(policies_json_output_file_path) as rules_yaml:
            policies_json_output = yaml.load(rules_yaml, Loader=yaml.FullLoader)
        policies = policies_json_output.get(get_key)
        parsed_policies_json_output = policies

        # going to expected result
        with open(os.path.join(self.this_folder,expected_output_file_path), 'r') as file:
            expected_data = file.read().split("\n")
        # assert len(parsed_policies_json_output) == len(expected_data)
        # in the output from juniper, there is an empty first line
        for x in range(0, len(parsed_policies_json_output)):
            logger.info("Checking line " + str(x))
            logger.info("Actual: " + parsed_policies_json_output[x])
            logger.info("Expected: " + expected_data[x])

            assert parsed_policies_json_output[x] == expected_data[x]


def parse_policies_output(acls_json):
    acls_json = re.sub(" \", ", "\n", acls_json)
    acls_json = re.sub("\", ", "\n", acls_json)
    acls_json = re.sub("\",", "\n", acls_json)
    acls_json = re.sub("\[", "", acls_json)
    acls_json = re.sub("\]", "", acls_json)
    acls_json = re.sub("<", "", acls_json)
    acls_json = re.sub("\"", "", acls_json)
    return acls_json