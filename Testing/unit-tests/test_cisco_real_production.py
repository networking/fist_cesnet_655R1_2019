import json
import os
import pathlib
import unittest
import logging

import yaml

from Implementation.enums import RuleType
from Implementation.firewall_cisco_parser import FirewallParserCisco
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.perun_parser import PerunParser
from Implementation.service_holder import ServiceEnum


class FirewallCiscoRealConfigTests(unittest.TestCase):
    this_folder = os.path.dirname(os.path.abspath(__file__))
    cisco_parser = None
    maxDiff = None

    def setUp(self):
        logger = logging.getLogger('fist-test')
        logger.setLevel(logging.DEBUG)
        this_folder = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(this_folder,
                               './resources/cisco_policies_real_config.json')) as json_file:
            self.cisco_policies = json.load(json_file)
        with open(
                os.path.join(this_folder, './resources/route-info_real_config.json')) as json_file:
            self.cisco_routes = json.load(json_file)
        with open(os.path.join(this_folder,
                               './resources/address-book_real_config.json')) as json_file:
            self.cisco_address_book = json.load(json_file)
        with open(os.path.join(this_folder, './resources/zones_real_config.json')) as json_file:
            self.cisco_zones = json.load(json_file)
        exec_path = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        self.cisco_parser = FirewallParserCisco(self.cisco_policies, self.cisco_address_book,
                                                self.cisco_routes,
                                                self.cisco_zones,
                                                exec_path)

    def test_parse_address_book(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()

        # test simple object
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name("Vlan499").name,
                         "Vlan499")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "Vlan499").address.ip_address.with_prefixlen, "10.56.159.224/27")

        # test nested object-groups
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_net_obj_by_name("NTP-SERVERS").name,
            "NTP-SERVERS")
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_net_obj_by_name("NTP-SERVERS").description,
            "NTP servery MU")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "NTP-SERVERS").get_net_object_names()[0], "NTP1.MUNI.CZ")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "NTP-SERVERS").get_net_object_names()[1], "NTP2.MUNI.CZ")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "NTP1.MUNI.CZ").get_net_object_names()[0], "147.251.4.33/32")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "NTP2.MUNI.CZ").get_net_object_names()[0], "147.251.4.37/32")

        # test some object service
        self.assertEqual(self.cisco_parser.applications_holder.application_dict["DHCP-RENEW"].name,
                         "DHCP-RENEW")
        self.assertEqual(
            self.cisco_parser.applications_holder.application_dict["DHCP-RENEW"].protocol,
            self.cisco_parser._FirewallParserCisco__find_service("udp"))
        self.assertEqual(
            self.cisco_parser.applications_holder.application_dict["DHCP-RENEW"].service_protocol,
            self.cisco_parser._FirewallParserCisco__find_service("67"))
        self.assertEqual(
            self.cisco_parser.applications_holder.application_dict["DHCP-RENEW"].source_port,
            self.cisco_parser._FirewallParserCisco__find_service("68"))

        # test host
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_net_obj_by_name("JAROS-MIKRO-UPDATY").name,
            "JAROS-MIKRO-UPDATY")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "JAROS-MIKRO-UPDATY").get_net_object_names()[0], "71.185.66.71/32")
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_tmp_net_obj("71.185.66.71/32").name,
            "71.185.66.71/32")
        self.assertEqual(self.cisco_parser.net_object_holder.find_tmp_net_obj(
            "71.185.66.71/32").address.ip_address.with_prefixlen, "71.185.66.71/32")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "JAROS-MIKRO-UPDATY").get_net_object_names()[1], "71.185.66.68/32")
        self.assertEqual(self.cisco_parser.net_object_holder.find_tmp_net_obj(
            "71.185.66.68/32").address.ip_address.with_prefixlen, "71.185.66.68/32")

        # test subnet
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name("MUNI-VPN").name,
                         "MUNI-VPN")
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_net_obj_by_name("MUNI-VPN").description,
            "RT#655494")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "MUNI-VPN").get_net_object_names()[0], "147.251.68.0/23")
        self.assertEqual(
            self.cisco_parser.net_object_holder.find_tmp_net_obj("147.251.68.0/23").name,
            "147.251.68.0/23")
        self.assertEqual(self.cisco_parser.net_object_holder.find_tmp_net_obj(
            "147.251.68.0/23").address.ip_address.with_prefixlen, "147.251.68.0/23")
        self.assertEqual(self.cisco_parser.net_object_holder.find_net_obj_by_name(
            "MUNI-VPN").get_net_object_names()[3], "147.251.232.0/21")
        self.assertEqual(self.cisco_parser.net_object_holder.find_tmp_net_obj(
            "147.251.232.0/21").address.ip_address.with_prefixlen, "147.251.232.0/21")

        # test some object-group service
        self.assertEqual(self.cisco_parser.applications_holder.application_groups_dict["SSH"].name,
                         "SSH")
        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["SSH"].applications[
                0].protocol, self.cisco_parser._FirewallParserCisco__find_service("tcp"))
        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["SSH"].applications[
                0].service_protocol, self.cisco_parser._FirewallParserCisco__find_service("SSH"))

        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["D_WEB"].name, "D_WEB")
        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["D_WEB"].applications[
                0].protocol, self.cisco_parser._FirewallParserCisco__find_service("tcp"))
        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["D_WEB"].applications[
                0].service_protocol, self.cisco_parser._FirewallParserCisco__find_service("www"))
        self.assertEqual(
            self.cisco_parser.applications_holder.application_groups_dict["D_WEB"].applications[
                1].service_protocol, self.cisco_parser._FirewallParserCisco__find_service("https"))

    def test_parse_policies(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        parsed_rule_sets = self.cisco_parser._FirewallParserCisco__parse_acls()

        # basic
        self.assertEqual(parsed_rule_sets[0].rule_set_name, "From_Vlan301")
        self.assertEqual(parsed_rule_sets[0].rules[0].rule_name, "From_Vlan301")
        self.assertEqual(parsed_rule_sets[0].rules[0].rule_set_name, "From_Vlan301")
        self.assertEqual(parsed_rule_sets[0].rules[0].match_rule.src_net_obj_name, "any")
        self.assertEqual(parsed_rule_sets[0].rules[0].match_rule.dst_net_obj_name, "any")
        self.assertEqual(parsed_rule_sets[0].rules[0].match_rule.application.protocol,
                         self.cisco_parser._FirewallParserCisco__find_service("ip"))
        self.assertEqual(parsed_rule_sets[0].rules[0].match_rule.application.service_protocol,
                         self.cisco_parser._FirewallParserCisco__find_service("any"))

        self.assertEqual(parsed_rule_sets[8].rules[2].rule_set_name, "From_Vlan433")
        self.assertEqual(parsed_rule_sets[8].rules[2].match_rule.src_net_obj_name, "Vlan433")
        self.assertEqual(parsed_rule_sets[8].rules[2].match_rule.dst_net_obj_name, "DNS-SERVERS")
        self.assertEqual(parsed_rule_sets[8].rules[2].match_rule.application.protocol,
                         self.cisco_parser._FirewallParserCisco__find_service("udp"))
        self.assertEqual(parsed_rule_sets[8].rules[2].match_rule.application.service_protocol,
                         self.cisco_parser._FirewallParserCisco__find_service("domain"))

        # test with ApplicationGroup
        self.assertEqual(parsed_rule_sets[11].rule_set_name, "To_Vlan436")
        self.assertEqual(parsed_rule_sets[11].rules[0].rule_name, "To_Vlan436")
        self.assertEqual(parsed_rule_sets[11].rules[0].rule_set_name, "To_Vlan436")
        self.assertEqual(parsed_rule_sets[11].rules[0].match_rule.src_net_obj_name, "MUNI")
        self.assertEqual(parsed_rule_sets[11].rules[0].match_rule.dst_net_obj_name, "Vlan436")
        self.assertEqual(
            parsed_rule_sets[11].rules[0].match_rule.application.applications[0].protocol,
            self.cisco_parser._FirewallParserCisco__find_service("tcp"))
        self.assertEqual(parsed_rule_sets[11].rules[0].match_rule.application.name, "RDP")
        self.assertEqual(
            parsed_rule_sets[11].rules[0].match_rule.application.applications[0].service_protocol,
            self.cisco_parser._FirewallParserCisco__find_service("3389"))

        # test with Application object
        self.assertEqual(parsed_rule_sets[4].rule_set_name, "From_Vlan404")
        self.assertEqual(parsed_rule_sets[4].rules[1].rule_name, "From_Vlan404")
        self.assertEqual(parsed_rule_sets[4].rules[1].rule_set_name, "From_Vlan404")
        self.assertEqual(parsed_rule_sets[4].rules[1].match_rule.src_net_obj_name, "Vlan404")
        self.assertEqual(parsed_rule_sets[4].rules[1].match_rule.dst_net_obj_name,
                         "D_UKB-DHCP-SERVERS")

        self.assertEqual(
            parsed_rule_sets[4].rules[1].match_rule.application.service_protocol.port_number,
            self.cisco_parser._FirewallParserCisco__find_service("67").port_number)
        self.assertEqual(
            parsed_rule_sets[4].rules[1].match_rule.application.source_port.port_number,
            self.cisco_parser._FirewallParserCisco__find_service("68").port_number)
        self.assertEqual(parsed_rule_sets[4].rules[1].match_rule.application.protocol.port_number,
                         self.cisco_parser._FirewallParserCisco__find_service("udp").port_number)

    def test_abstract_to_address_book_simple_objects(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        simple_objects_with_app, group_objects = self.cisco_parser.abstract_to_address_book()

        self.assertEqual(simple_objects_with_app[6], 'object network Vlan643')
        self.assertEqual(simple_objects_with_app[7], 'subnet 147.251.158.112 255.255.255.240')
        self.assertEqual(simple_objects_with_app[52], "object network DEVICE-10.56.159.234")
        self.assertEqual(simple_objects_with_app[53], "host 10.56.159.234")
        self.assertEqual(simple_objects_with_app[66], "object network Vlan302")
        self.assertEqual(simple_objects_with_app[67], "subnet 147.251.141.0 255.255.255.0")
        self.assertEqual(simple_objects_with_app[68], "object service DHCP-RENEW")
        self.assertEqual(simple_objects_with_app[69], "service udp source eq 68 destination eq 67")

    def test_abstract_to_address_book_group_objects(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        simple_objects_with_app, group_objects = self.cisco_parser.abstract_to_address_book()

        self.assertEqual(group_objects[0], "object-group network MUNI")
        self.assertEqual(group_objects[1], "network-object 147.251.0.0 255.255.0.0")
        self.assertEqual(group_objects[2], "network-object 10.0.0.0 255.0.0.0")
        self.assertEqual(group_objects[7], "object-group network DNS-SERVERS")
        self.assertEqual(group_objects[8], "description DNS servery")
        self.assertEqual(group_objects[9], "group-object DIOR")
        self.assertEqual(group_objects[12], "network-object host 147.251.106.73")
        self.assertEqual(group_objects[59], "object-group network MUNI-VPN")
        self.assertEqual(group_objects[61], "network-object 147.251.68.0 255.255.254.0")
        self.assertEqual(group_objects[64], "network-object 147.251.232.0 255.255.248.0")
        self.assertEqual(group_objects[86], "object-group network C_DEVICE-10.56.159.234")
        self.assertEqual(group_objects[87], "network-object object Vlan437")
        self.assertEqual(group_objects[-1], "port-object eq 22")

        address_book = [["object network perunObject0", "host 147.251.186.2",
                         "object network perunObject148.1.1.3slash32", "host 148.1.1.3",
                         "object network perunObject148.1.1.4slash32", "host 148.1.1.4",
                         "object network Vlan643", "subnet 147.251.158.112 255.255.255.240",
                         "object network Vlan433", "subnet 147.251.130.96 255.255.255.248",
                         "object network Vlan435", "subnet 147.251.130.112 255.255.255.248",
                         "object network Vlan603", "subnet 147.251.158.128 255.255.255.224",
                         "object network Vlan635", "subnet 147.251.157.0 255.255.255.128",
                         "object network Vlan636", "subnet 147.251.157.128 255.255.255.192",
                         "object network Vlan637", "subnet 147.251.157.192 255.255.255.224",
                         "object network Vlan638", "subnet 147.251.157.224 255.255.255.224",
                         "object network Vlan639", "subnet 147.251.158.0 255.255.255.224",
                         "object network Vlan693", "subnet 10.56.95.64 255.255.255.224",
                         "object network Vlan436", "subnet 147.251.128.128 255.255.255.192",
                         "object network Vlan678", "subnet 10.56.93.192 255.255.255.224",
                         "object network Vlan129", "subnet 10.56.4.0 255.255.254.0",
                         "object network Vlan462", "subnet 10.56.140.0 255.255.255.0",
                         "object network Vlan700", "subnet 147.251.156.64 255.255.255.192",
                         "object network Vlan437", "subnet 147.251.130.32 255.255.255.224",
                         "object network Vlan701", "subnet 147.251.158.192 255.255.255.192",
                         "object network Vlan675", "subnet 10.56.93.144 255.255.255.240",
                         "object network Vlan499", "subnet 10.56.159.224 255.255.255.224",
                         "object network Vlan438", "subnet 147.251.130.128 255.255.255.192",
                         "object network Vlan439", "subnet 147.251.130.192 255.255.255.224",
                         "object network Vlan440", "subnet 147.251.130.224 255.255.255.240",
                         "object network Vlan662", "subnet 10.56.76.0 255.255.255.0",
                         "object network DEVICE-10.56.159.234", "host 10.56.159.234",
                         "object network Vlan614", "subnet 147.251.155.80 255.255.255.240",
                         "object network Vlan604", "subnet 147.251.154.160 255.255.255.240",
                         "object network Vlan301", "subnet 147.251.186.0 255.255.255.0",
                         "object network HA-KAT.ICS.MUNI.CZ", "host 147.251.6.39",
                         "object network Vlan428", "subnet 147.251.131.128 255.255.255.224",
                         "object network Vlan404", "subnet 147.251.138.0 255.255.255.0",
                         "object network Vlan302", "subnet 147.251.141.0 255.255.255.0",
                         "object service DHCP-RENEW",
                         "service udp source eq 68 destination eq 67", ],
                        ["object-group network MUNI",
                         "network-object 147.251.0.0 255.255.0.0",
                         "network-object 10.0.0.0 255.0.0.0",
                         "object-group network DIOR",
                         "network-object host 147.251.6.10",
                         "object-group network NS.MUNI.CZ",
                         "network-object host 147.251.4.33",
                         "object-group network DNS-SERVERS",
                         "description DNS servery",
                         "group-object DIOR",
                         "group-object NS.MUNI.CZ",
                         "object-group network COPY",
                         "network-object host 147.251.106.73",
                         "network-object host 147.251.151.20",
                         "network-object host 147.251.106.77",
                         "object-group network SAM.ICS.MUNI.CZ",
                         "network-object host 147.251.6.104",
                         "object-group network ANTIVIRUS",
                         "network-object host 84.233.195.57",
                         "network-object host 89.202.149.45",
                         "network-object host 89.202.149.47",
                         "network-object host 89.202.149.49",
                         "network-object host 89.202.157.135",
                         "network-object host 89.202.157.137",
                         "network-object host 90.183.101.15",
                         "network-object host 90.183.101.17",
                         "network-object host 93.184.71.10",
                         "network-object host 93.184.71.21",
                         "network-object host 93.184.71.27",
                         "network-object host 91.228.165.69",
                         "network-object host 212.73.202.109",
                         "network-object host 212.73.202.103",
                         "network-object host 91.228.165.70",
                         "network-object host 84.233.195.61",
                         "network-object host 84.233.195.62",
                         "network-object host 91.228.166.13",
                         "network-object host 91.228.166.14",
                         "network-object host 91.228.166.15",
                         "network-object host 91.228.166.16",
                         "network-object host 91.228.167.21",
                         "network-object host 91.228.167.26",
                         "network-object host 91.228.165.81",
                         "network-object host 91.228.167.125",
                         "object-group network NTP1.MUNI.CZ",
                         "network-object host 147.251.4.33",
                         "object-group network NTP2.MUNI.CZ",
                         "network-object host 147.251.4.37",
                         "object-group network NTP-SERVERS",
                         "description NTP servery MU",
                         "group-object NTP1.MUNI.CZ",
                         "group-object NTP2.MUNI.CZ",
                         "object-group network PROXY.ICS.MUNI.CZ",
                         "network-object host 147.251.6.170",
                         "object-group network WSUS.ICS.MUNI.CZ",
                         "network-object host 147.251.12.197",
                         "object-group network IS",
                         "network-object host 147.251.49.10",
                         "object-group network A1-100TB-ULOZISTE",
                         "network-object host 147.251.150.150",
                         "object-group network MUNI-VPN",
                         "description RT#655494",
                         "network-object 147.251.68.0 255.255.254.0",
                         "network-object 147.251.55.0 255.255.255.0",
                         "network-object 147.251.56.0 255.255.255.0",
                         "network-object 147.251.232.0 255.255.248.0",
                         "object-group network C_ICS-MGMT",
                         "network-object 147.251.7.0 255.255.255.192",
                         "object-group network JAROS-MIKRO-UPDATY",
                         "network-object host 71.185.66.71",
                         "network-object host 71.185.66.68",
                         "object-group network VPN-HOST-56.186",
                         "description description pristup na BAY-DA* RT#181690,208952,653831",
                         "network-object host 147.251.56.186",
                         "object-group network HA-BAY",
                         "description uloziste",
                         "network-object host 147.251.106.77",
                         "object-group network TS.UKB.MUNI.CZ",
                         "network-object host 147.251.158.164",
                         "object-group network UHEPOLE.UKB.MUNI.CZ",
                         "network-object host 147.251.130.116",
                         "object-group network UHE-TISK",
                         "network-object host 10.56.140.4",
                         "network-object host 10.56.140.5",
                         "network-object host 10.56.140.6",
                         "object-group network UHE-ORIG",
                         "network-object 147.251.150.0 255.255.255.0",
                         "object-group network C_DEVICE-10.56.159.234",
                         "network-object object Vlan437",
                         "network-object object Vlan438",
                         "network-object object Vlan439",
                         "object-group network CIT-PRINTSERVER",
                         "description RT#206825,RT#213324",
                         "network-object host 147.251.19.70",
                         "object-group network D_UKB-DHCP-SERVERS",
                         "network-object host 147.251.6.144",
                         "network-object host 147.251.128.11",
                         "object-group network D_UCN_SCCM",
                         "network-object host 147.251.12.210",
                         "network-object host 147.251.12.88",
                         "object-group network perunObjectGroup5",
                         "network-object object perunObject148.1.1.3slash32",
                         "network-object object perunObject148.1.1.4slash32",
                         "object-group service SSH tcp",
                         "port-object eq 22",
                         "object-group service WWW tcp",
                         "port-object eq 80",
                         "object-group service S_H225_T_1 tcp",
                         "description VoIP signalizace",
                         "port-object eq h323",
                         "object-group service RDP tcp-udp",
                         "port-object eq 3389",
                         "object-group service D_PROXY.ICS.MUNI.CZ_T_1 tcp",
                         "port-object eq 3128",
                         "object-group service D_WSUS.ICS.MUNI.CZ_T_1 tcp",
                         "port-object eq 80",
                         "port-object eq 443",
                         "port-object eq 2222",
                         "port-object eq 445",
                         "object-group service RDESKTOP tcp",
                         "port-object eq 3389",
                         "object-group service S_NFS tcp-udp",
                         "description NFS porty",
                         "port-object eq nfs",
                         "port-object eq 111",
                         "object-group service S_SAMBA_T tcp",
                         "description SMB porty",
                         "port-object eq 445",
                         "object-group service HTTP tcp",
                         "port-object eq 80",
                         "object-group service D_WEB tcp",
                         "description spolecna definice standardnich portu pro web",
                         "port-object eq 80",
                         "port-object eq 443",
                         "object-group service S_DEVICE-10.56.159.234_T_1 tcp",
                         "port-object eq 22"
                         ]]

        self.cisco_parser.parse()
        test_cisco_policies, test_cisco_address_book = self.cisco_parser. \
            _FirewallParserCisco__parse_cisco_rule_set_to_policies_and_address_book()

        self.assertEqual(test_cisco_address_book, address_book)

    def test_abstract_to_policies(self):
        cisco_policies = [["access-group From_Vlan301 in interface Vlan301",
                           "access-group To_Vlan301 out interface Vlan301",
                           "access-group From_Vlan302 in interface Vlan302",
                           "access-group To_Vlan302 out interface Vlan302",
                           "access-group From_Vlan404 in interface Vlan404",
                           "access-group To_Vlan404 out interface Vlan404",
                           "access-group From_Vlan428 in interface Vlan428",
                           "access-group To_Vlan428 out interface Vlan428",
                           "access-group From_Vlan433 in interface Vlan433",
                           "access-group To_Vlan433 out interface Vlan433",
                           "access-group From_Vlan436 in interface Vlan436",
                           "access-group To_Vlan436 out interface Vlan436",
                           "access-group From_Vlan437 in interface Vlan437",
                           "access-group To_Vlan437 out interface Vlan437",
                           "access-group From_Vlan438 in interface Vlan438",
                           "access-group To_Vlan438 out interface Vlan438",
                           "access-group From_Vlan439 in interface Vlan439",
                           "access-group To_Vlan439 out interface Vlan439",
                           "access-group From_Vlan440 in interface Vlan440",
                           "access-group To_Vlan440 out interface Vlan440",
                           "access-group From_Vlan462 in interface Vlan462",
                           "access-group To_Vlan462 out interface Vlan462",
                           "access-group From_Vlan499 in interface Vlan499",
                           "access-group To_Vlan499 out interface Vlan499"],
                          ["access-list From_Vlan301 remark 120613",
                           "access-list From_Vlan301 extended permit ip any any",
                           "access-list To_Vlan301 remark 120613",
                           "access-list To_Vlan301 extended permit tcp object-group MUNI object Vlan301 object-group RDP",
                           "access-list To_Vlan301 extended deny tcp any object Vlan301 object-group RDP",
                           "access-list To_Vlan301 remark Perun generated rules will follow",
                           "access-list To_Vlan301 extended permit ip object-group perunObjectGroup5 object perunObject0",
                           "access-list To_Vlan301 remark Perun generated rules end",
                           "access-list To_Vlan301 extended permit ip any any",
                           "access-list From_Vlan302 remark RT#490019",
                           "access-list From_Vlan302 extended permit ip any any",
                           "access-list To_Vlan302 remark RT490019 nastavit ako Vlan301",
                           "access-list To_Vlan302 extended permit tcp object-group MUNI object Vlan302 object-group RDP",
                           "access-list To_Vlan302 extended deny tcp any object Vlan302 object-group RDP",
                           "access-list To_Vlan302 extended permit ip any any",
                           "access-list From_Vlan404 extended permit icmp any any echo",
                           "access-list From_Vlan404 extended permit object DHCP-RENEW object Vlan404 object-group D_UKB-DHCP-SERVERS",
                           "access-list From_Vlan404 remark prisne omezeni pristroje doc.Tomandl jen VPN/RDP (Siroky 31.3.2015)",
                           "access-list From_Vlan404 extended deny ip host 147.251.138.6 any",
                           "access-list From_Vlan404 remark zbytek povolit",
                           "access-list From_Vlan404 extended permit ip any any",
                           "access-list To_Vlan404 extended permit icmp any any echo",
                           "access-list To_Vlan404 remark prisne omezeni pristroje doc.Tomandl jen VPN/RDP (Siroky 31.3.2015)",
                           "access-list To_Vlan404 extended permit tcp object-group MUNI-VPN host 147.251.138.6 object-group RDP",
                           "access-list To_Vlan404 extended deny ip any host 147.251.138.6",
                           "access-list To_Vlan404 remark povoleni spojeni z vpn na rdesktop",
                           "access-list To_Vlan404 extended permit tcp object-group MUNI-VPN host 147.251.138.5 object-group RDP",
                           "access-list To_Vlan404 remark omezeni pristupu k 147.251.138.107 RT#228678",
                           "access-list To_Vlan404 extended permit tcp host 147.251.186.17 host 147.251.138.107 object-group RDP",
                           "access-list To_Vlan404 extended deny ip any host 147.251.138.107",
                           "access-list To_Vlan404 remark povoleni sdilene slozky mezi pc#201843",
                           "access-list To_Vlan404 extended permit tcp host 147.251.186.23 host 147.251.138.8 object-group S_SAMBA_T",
                           "access-list To_Vlan404 extended deny ip any any",
                           "access-list From_Vlan428 extended permit ip any any",
                           "access-list To_Vlan428 extended permit ip any any",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object Vlan435",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object-group ANTIVIRUS",
                           "access-list From_Vlan433 extended permit udp object Vlan433 object-group DNS-SERVERS eq 53",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 object-group DNS-SERVERS eq 53",
                           "access-list From_Vlan433 extended permit udp object Vlan433 object-group NTP-SERVERS eq 123",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object-group COPY",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object-group A1-100TB-ULOZISTE",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 any eq 5938",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 host 195.113.161.30",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 any eq 80",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 any eq 443",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object-group JAROS-MIKRO-UPDATY",
                           "access-list From_Vlan433 extended permit tcp object Vlan433 any eq 22",
                           "access-list From_Vlan433 extended permit ip object Vlan433 object-group HA-BAY",
                           "access-list From_Vlan433 extended deny ip any any log",
                           "access-list To_Vlan433 extended deny ip any any log",
                           "access-list From_Vlan436 extended permit ip any any",
                           "access-list To_Vlan436 extended permit tcp object-group MUNI object Vlan436 object-group RDP",
                           "access-list To_Vlan436 extended deny tcp any object Vlan436 object-group RDP",
                           "access-list To_Vlan436 extended permit ip any any",
                           "access-list From_Vlan437 remark RT#264643",
                           "access-list From_Vlan437 extended permit ip any any",
                           "access-list To_Vlan437 remark RT#264643",
                           "access-list To_Vlan437 extended permit tcp object-group MUNI object Vlan437 object-group RDP",
                           "access-list To_Vlan437 extended deny tcp any object Vlan437 object-group RDP",
                           "access-list To_Vlan437 extended permit ip any any",
                           "access-list From_Vlan438 extended permit ip any any",
                           "access-list To_Vlan438 extended permit icmp object-group C_ICS-MGMT object Vlan438 echo",
                           "access-list To_Vlan438 extended permit ip object-group D_UCN_SCCM object Vlan438",
                           "access-list To_Vlan438 extended deny ip any any",
                           "access-list From_Vlan439 extended permit ip any any",
                           "access-list To_Vlan439 extended permit icmp object-group C_ICS-MGMT object Vlan439 echo",
                           "access-list To_Vlan439 remark 112391",
                           "access-list To_Vlan439 extended permit tcp object Vlan438 object Vlan439 eq 16020",
                           "access-list To_Vlan439 extended deny ip any any",
                           "access-list From_Vlan440 extended permit ip any any",
                           "access-list To_Vlan440 extended permit icmp object-group C_ICS-MGMT object Vlan440 echo",
                           "access-list To_Vlan440 remark 112392",
                           "access-list To_Vlan440 extended permit tcp object Vlan438 object Vlan440 eq 16020",
                           "access-list To_Vlan440 extended deny ip any any",
                           "access-list From_Vlan462 extended permit ip any any",
                           "access-list To_Vlan462 extended permit ip object Vlan437 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object Vlan438 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object Vlan439 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object Vlan440 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object Vlan499 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object-group UHE-ORIG object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object-group CIT-PRINTSERVER object-group UHE-TISK",
                           "access-list To_Vlan462 remark Pristup na scanner z Vlan{301427} RT# 148231",
                           "access-list To_Vlan462 extended permit ip object Vlan301 object-group UHE-TISK",
                           "access-list To_Vlan462 extended permit ip object Vlan302 object-group UHE-TISK",
                           "access-list To_Vlan462 remark Pristup na tiskarny z MUNI RT# 153993",
                           "access-list To_Vlan462 extended permit ip object-group MUNI object Vlan462",
                           "access-list To_Vlan462 extended deny ip any any log",
                           "access-list From_Vlan499 remark OUT-STD",
                           "access-list From_Vlan499 remark 112387",
                           "access-list From_Vlan499 extended permit udp object Vlan499 object-group DNS-SERVERS eq 53",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group DNS-SERVERS eq 53",
                           "access-list From_Vlan499 extended permit udp object Vlan499 object-group COPY object-group S_NFS",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY object-group S_NFS",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY object-group S_SAMBA_T",
                           "access-list From_Vlan499 extended permit udp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_NFS",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_NFS",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_SAMBA_T",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group WSUS.ICS.MUNI.CZ object-group D_WEB",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group TS.UKB.MUNI.CZ object-group RDESKTOP",
                           "access-list From_Vlan499 extended permit icmp object Vlan499 object-group MUNI echo",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group PROXY.ICS.MUNI.CZ object-group D_PROXY.ICS.MUNI.CZ_T_1",
                           "access-list From_Vlan499 remark 112390",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ eq 22",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ object-group S_SAMBA_T",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ object-group S_NFS",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY eq 22",
                           "access-list From_Vlan499 extended permit ip object Vlan499 object-group COPY log",
                           "access-list From_Vlan499 remark RT#219383",
                           "access-list From_Vlan499 extended permit tcp object Vlan499 object HA-KAT.ICS.MUNI.CZ object-group S_SAMBA_T",
                           "access-list From_Vlan499 extended deny ip any any",
                           "access-list To_Vlan499 extended permit icmp object-group C_ICS-MGMT object Vlan499 echo",
                           "access-list To_Vlan499 remark 112393",
                           "access-list To_Vlan499 extended permit tcp object Vlan438 object Vlan499 eq 16020",
                           "access-list To_Vlan499 remark Tonda povoleni scp na pristroj dodelat ticket.",
                           "access-list To_Vlan499 extended permit tcp object-group C_DEVICE-10.56.159.234 object DEVICE-10.56.159.234 object-group S_DEVICE-10.56.159.234_T_1",
                           "access-list To_Vlan499 extended deny ip any any"
                           ]]

        self.cisco_parser.parse()
        test_cisco_policies, test_cisco_address_book = self.cisco_parser. \
            _FirewallParserCisco__parse_cisco_rule_set_to_policies_and_address_book()

        self.assertEqual(test_cisco_policies, cisco_policies)

    def test_convert_to_yml(self):
        group_objects = ['object-group service SSH tcp', 'port-object eq 22',
                         'object-group service WWW tcp', 'port-object eq 80',
                         'object-group service S_H225_T_1 tcp', 'description VoIP signalizace',
                         'port-object eq h323', 'object-group service RDP tcp-udp',
                         'port-object eq 3389', 'object-group service D_PROXY.ICS.MUNI.CZ_T_1 tcp',
                         'port-object eq 3128', 'object-group service D_WSUS.ICS.MUNI.CZ_T_1 tcp',
                         'port-object eq 80', 'port-object eq 443', 'port-object eq 2222',
                         'port-object eq 445', 'object-group service RDESKTOP tcp',
                         'port-object eq 3389', 'object-group service S_NFS tcp-udp',
                         'description NFS porty', 'port-object eq nfs', 'port-object eq 111',
                         'object-group service S_SAMBA_T tcp', 'description SMB porty',
                         'port-object eq 445', 'object-group service HTTP tcp', 'port-object eq 80',
                         'object-group service D_WEB tcp',
                         'description spolecna definice standardnich portu pro web',
                         'port-object eq 80', 'port-object eq 443',
                         'object-group service S_DEVICE-10.56.159.234_T_1 tcp', 'port-object eq 22']
        simple_objects = ['object service DHCP-RENEW', 'service udp source eq 68 destination eq 67']
        objects = FirewallParserCisco._FirewallParserCisco__convert_objects_to_yml(simple_objects,
                                                                                   group_objects)

        expected_objects = [{'lines': ['service udp source eq 68 destination eq 67'],
                             'parents': ['object service DHCP-RENEW']},
                            {'lines': ['port-object eq 22'],
                             'parents': ['object-group service SSH tcp']},
                            {'lines': ['port-object eq 80'],
                             'parents': ['object-group service WWW tcp']},
                            {'lines': ['description VoIP signalizace', 'port-object eq h323'],
                             'parents': ['object-group service S_H225_T_1 tcp']},
                            {'lines': ['port-object eq 3389'],
                             'parents': ['object-group service RDP tcp-udp']},
                            {'lines': ['port-object eq 3128'],
                             'parents': ['object-group service D_PROXY.ICS.MUNI.CZ_T_1 tcp']}, {
                                'lines': ['port-object eq 80', 'port-object eq 443',
                                          'port-object eq 2222', 'port-object eq 445'],
                                'parents': ['object-group service D_WSUS.ICS.MUNI.CZ_T_1 tcp']},
                            {'lines': ['port-object eq 3389'],
                             'parents': ['object-group service RDESKTOP tcp']}, {
                                'lines': ['description NFS porty', 'port-object eq nfs',
                                          'port-object eq 111'],
                                'parents': ['object-group service S_NFS tcp-udp']},
                            {'lines': ['description SMB porty', 'port-object eq 445'],
                             'parents': ['object-group service S_SAMBA_T tcp']},
                            {'lines': ['port-object eq 80'],
                             'parents': ['object-group service HTTP tcp']}, {'lines': [
                'description spolecna definice standardnich portu pro web', 'port-object eq 80',
                'port-object eq 443'], 'parents': ['object-group service D_WEB tcp']},
                            {'lines': ['port-object eq 22'],
                             'parents': ['object-group service S_DEVICE-10.56.159.234_T_1 tcp']}]

        self.assertEqual(expected_objects, objects)

    def test_convert_to_yml_empty(self):
        group_objects = []
        simple_objects = []
        objects = FirewallParserCisco._FirewallParserCisco__convert_objects_to_yml(simple_objects,
                                                                                   group_objects)

        expected_objects = []

        self.assertEqual(expected_objects, objects)

    def test_add_line_numbers_opt0(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/inputTestingFromRealProductioncisco.json')) as json_file:
            perun_json_file = json.load(json_file)

        self.cisco_parser.parse()
        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_all(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)
        self.cisco_parser.deploy_policies(diff_rule_sets)

        with open(self.cisco_parser.exec_dir + 'roles/deploy-all-asa/vars/rules.yml') as yaml_file:
            rules = yaml.load(yaml_file, Loader=yaml.FullLoader)
        expected = {'all_groups': ['access-group From_Vlan301 in interface Vlan301',
                                   'access-group To_Vlan301 out interface Vlan301',
                                   'access-group From_Vlan302 in interface Vlan302',
                                   'access-group To_Vlan302 out interface Vlan302',
                                   'access-group From_Vlan404 in interface Vlan404',
                                   'access-group To_Vlan404 out interface Vlan404',
                                   'access-group From_Vlan428 in interface Vlan428',
                                   'access-group To_Vlan428 out interface Vlan428',
                                   'access-group From_Vlan433 in interface Vlan433',
                                   'access-group To_Vlan433 out interface Vlan433',
                                   'access-group From_Vlan436 in interface Vlan436',
                                   'access-group To_Vlan436 out interface Vlan436',
                                   'access-group From_Vlan437 in interface Vlan437',
                                   'access-group To_Vlan437 out interface Vlan437',
                                   'access-group From_Vlan438 in interface Vlan438',
                                   'access-group To_Vlan438 out interface Vlan438',
                                   'access-group From_Vlan439 in interface Vlan439',
                                   'access-group To_Vlan439 out interface Vlan439',
                                   'access-group From_Vlan440 in interface Vlan440',
                                   'access-group To_Vlan440 out interface Vlan440',
                                   'access-group From_Vlan462 in interface Vlan462',
                                   'access-group To_Vlan462 out interface Vlan462',
                                   'access-group From_Vlan499 in interface Vlan499',
                                   'access-group To_Vlan499 out interface Vlan499',
                                   'access-group from_outside in interface outside',
                                   'access-group from_Vlan701 in interface Vlan701'],
                    'all_policies': ['access-list From_Vlan301 remark 120613',
                                     'access-list From_Vlan301 extended permit ip any any',
                                     'access-list To_Vlan301 remark 120613',
                                     'access-list To_Vlan301 extended permit tcp object-group MUNI object Vlan301 object-group RDP',
                                     'access-list To_Vlan301 extended deny tcp any object Vlan301 object-group RDP',
                                     'access-list To_Vlan301 extended permit ip any any',
                                     'access-list To_Vlan301 line 4 remark Perun generated rules will follow',
                                     'access-list To_Vlan301 line 5 extended permit object peruntcp80 object-group perunObjectGroup0 object perunObject0',
                                     'access-list To_Vlan301 line 6 extended permit object peruntcp80 object-group perunObjectGroup1 object perunObject1',
                                     'access-list To_Vlan301 line 7 remark Perun generated rules end',
                                     'access-list From_Vlan302 remark RT#490019',
                                     'access-list From_Vlan302 extended permit ip any any',
                                     'access-list To_Vlan302 remark RT490019 nastavit ako Vlan301',
                                     'access-list To_Vlan302 extended permit tcp object-group MUNI object Vlan302 object-group RDP',
                                     'access-list To_Vlan302 extended deny tcp any object Vlan302 object-group RDP',
                                     'access-list To_Vlan302 extended permit ip any any',
                                     'access-list From_Vlan404 extended permit icmp any any echo',
                                     'access-list From_Vlan404 extended permit object DHCP-RENEW object Vlan404 object-group D_UKB-DHCP-SERVERS',
                                     'access-list From_Vlan404 remark prisne omezeni pristroje doc.Tomandl jen VPN/RDP (Siroky 31.3.2015)',
                                     'access-list From_Vlan404 extended deny ip host 147.251.138.6 any',
                                     'access-list From_Vlan404 remark zbytek povolit',
                                     'access-list From_Vlan404 extended permit ip any any',
                                     'access-list To_Vlan404 extended permit icmp any any echo',
                                     'access-list To_Vlan404 remark prisne omezeni pristroje doc.Tomandl jen VPN/RDP (Siroky 31.3.2015)',
                                     'access-list To_Vlan404 extended permit tcp object-group MUNI-VPN host 147.251.138.6 object-group RDP',
                                     'access-list To_Vlan404 extended deny ip any host 147.251.138.6',
                                     'access-list To_Vlan404 remark povoleni spojeni z vpn na rdesktop',
                                     'access-list To_Vlan404 extended permit tcp object-group MUNI-VPN host 147.251.138.5 object-group RDP',
                                     'access-list To_Vlan404 remark omezeni pristupu k 147.251.138.107 RT#228678',
                                     'access-list To_Vlan404 extended permit tcp host 147.251.186.17 host 147.251.138.107 object-group RDP',
                                     'access-list To_Vlan404 extended deny ip any host 147.251.138.107',
                                     'access-list To_Vlan404 remark povoleni sdilene slozky mezi pc#201843',
                                     'access-list To_Vlan404 extended permit tcp host 147.251.186.23 host 147.251.138.8 object-group S_SAMBA_T',
                                     'access-list To_Vlan404 extended deny ip any any',
                                     'access-list From_Vlan428 extended permit ip any any',
                                     'access-list To_Vlan428 extended permit ip any any',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object Vlan435',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object-group ANTIVIRUS',
                                     'access-list From_Vlan433 extended permit udp object Vlan433 object-group DNS-SERVERS eq 53',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 object-group DNS-SERVERS eq 53',
                                     'access-list From_Vlan433 extended permit udp object Vlan433 object-group NTP-SERVERS eq 123',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object-group COPY',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object-group A1-100TB-ULOZISTE',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 any eq 5938',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 host 195.113.161.30',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 any eq 80',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 any eq 443',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object-group JAROS-MIKRO-UPDATY',
                                     'access-list From_Vlan433 extended permit tcp object Vlan433 any eq 22',
                                     'access-list From_Vlan433 extended permit ip object Vlan433 object-group HA-BAY',
                                     'access-list From_Vlan433 extended deny ip any any log',
                                     'access-list To_Vlan433 extended deny ip any any log',
                                     'access-list From_Vlan436 extended permit ip any any',
                                     'access-list To_Vlan436 extended permit tcp object-group MUNI object Vlan436 object-group RDP',
                                     'access-list To_Vlan436 extended deny tcp any object Vlan436 object-group RDP',
                                     'access-list To_Vlan436 extended permit ip any any',
                                     'access-list From_Vlan437 remark RT#264643',
                                     'access-list From_Vlan437 extended permit ip any any',
                                     'access-list To_Vlan437 remark RT#264643',
                                     'access-list To_Vlan437 extended permit tcp object-group MUNI object Vlan437 object-group RDP',
                                     'access-list To_Vlan437 extended deny tcp any object Vlan437 object-group RDP',
                                     'access-list To_Vlan437 extended permit ip any any',
                                     'access-list From_Vlan438 extended permit ip any any',
                                     'access-list To_Vlan438 extended permit icmp object-group C_ICS-MGMT object Vlan438 echo',
                                     'access-list To_Vlan438 extended permit ip object-group D_UCN_SCCM object Vlan438',
                                     'access-list To_Vlan438 extended deny ip any any',
                                     'access-list From_Vlan439 extended permit ip any any',
                                     'access-list To_Vlan439 extended permit icmp object-group C_ICS-MGMT object Vlan439 echo',
                                     'access-list To_Vlan439 remark 112391',
                                     'access-list To_Vlan439 extended permit tcp object Vlan438 object Vlan439 eq 16020',
                                     'access-list To_Vlan439 extended deny ip any any',
                                     'access-list From_Vlan440 extended permit ip any any',
                                     'access-list To_Vlan440 extended permit icmp object-group C_ICS-MGMT object Vlan440 echo',
                                     'access-list To_Vlan440 remark 112392',
                                     'access-list To_Vlan440 extended permit tcp object Vlan438 object Vlan440 eq 16020',
                                     'access-list To_Vlan440 extended deny ip any any',
                                     'access-list From_Vlan462 extended permit ip any any',
                                     'access-list To_Vlan462 extended permit ip object Vlan437 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object Vlan438 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object Vlan439 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object Vlan440 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object Vlan499 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object-group UHE-ORIG object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object-group CIT-PRINTSERVER object-group UHE-TISK',
                                     'access-list To_Vlan462 remark Pristup na scanner z Vlan{301427} RT# 148231',
                                     'access-list To_Vlan462 extended permit ip object Vlan301 object-group UHE-TISK',
                                     'access-list To_Vlan462 extended permit ip object Vlan302 object-group UHE-TISK',
                                     'access-list To_Vlan462 remark Pristup na tiskarny z MUNI RT# 153993',
                                     'access-list To_Vlan462 extended permit ip object-group MUNI object Vlan462',
                                     'access-list To_Vlan462 extended deny ip any any log',
                                     'access-list From_Vlan499 remark OUT-STD',
                                     'access-list From_Vlan499 remark 112387',
                                     'access-list From_Vlan499 extended permit udp object Vlan499 object-group DNS-SERVERS eq 53',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group DNS-SERVERS eq 53',
                                     'access-list From_Vlan499 extended permit udp object Vlan499 object-group COPY object-group S_NFS',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY object-group S_NFS',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY object-group S_SAMBA_T',
                                     'access-list From_Vlan499 extended permit udp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_NFS',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_NFS',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group SAM.ICS.MUNI.CZ object-group S_SAMBA_T',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group WSUS.ICS.MUNI.CZ object-group D_WEB',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group TS.UKB.MUNI.CZ object-group RDESKTOP',
                                     'access-list From_Vlan499 extended permit icmp object Vlan499 object-group MUNI echo',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group PROXY.ICS.MUNI.CZ object-group D_PROXY.ICS.MUNI.CZ_T_1',
                                     'access-list From_Vlan499 remark 112390',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ eq 22',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ object-group S_SAMBA_T',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group UHEPOLE.UKB.MUNI.CZ object-group S_NFS',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object-group COPY eq 22',
                                     'access-list From_Vlan499 extended permit ip object Vlan499 object-group COPY log',
                                     'access-list From_Vlan499 remark RT#219383',
                                     'access-list From_Vlan499 extended permit tcp object Vlan499 object HA-KAT.ICS.MUNI.CZ object-group S_SAMBA_T',
                                     'access-list From_Vlan499 extended deny ip any any',
                                     'access-list To_Vlan499 extended permit icmp object-group C_ICS-MGMT object Vlan499 echo',
                                     'access-list To_Vlan499 remark 112393',
                                     'access-list To_Vlan499 extended permit tcp object Vlan438 object Vlan499 eq 16020',
                                     'access-list To_Vlan499 remark Tonda povoleni scp na pristroj dodelat ticket.',
                                     'access-list To_Vlan499 extended permit tcp object-group C_DEVICE-10.56.159.234 object DEVICE-10.56.159.234 object-group S_DEVICE-10.56.159.234_T_1',
                                     'access-list To_Vlan499 extended deny ip any any',
                                     'access-list from_outside remark Perun generated rules will follow',
                                     'access-list from_outside extended permit object peruntcp80 object-group perunObjectGroup0 object perunObject0',
                                     'access-list from_outside remark Perun generated rules end',
                                     'access-list from_outside extended deny ip any any',
                                     'access-list from_Vlan701 remark Perun generated rules will follow',
                                     'access-list from_Vlan701 extended permit object peruntcp80 object-group perunObjectGroup1 object perunObject1',
                                     'access-list from_Vlan701 remark Perun generated rules end',
                                     'access-list from_Vlan701 extended deny ip any any']}


        self.assertEqual(expected, rules)

    def test_add_line_numbers_opt1(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/inputTestingFromRealProductioncisco.json')) as json_file:
            perun_json_file = json.load(json_file)

        self.cisco_parser.parse()
        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)
        self.cisco_parser.deploy_policies(diff_rule_sets)

        with open(self.cisco_parser.exec_dir + 'roles/deploy-all-asa/vars/rules.yml') as yaml_file:
            rules = yaml.load(yaml_file, Loader=yaml.FullLoader)

        expected = {'all_groups': ['access-group To_Vlan301 out interface Vlan301',
                                   'access-group from_outside in interface outside',
                                   'access-group from_Vlan701 in interface Vlan701'],
                    'all_policies': ['access-list To_Vlan301 line 5 extended permit object '
                  'peruntcp80 object-group perunObjectGroup0 object '
                  'perunObject0',
                  'access-list To_Vlan301 line 6 extended permit object '
                  'peruntcp80 object-group perunObjectGroup1 object '
                  'perunObject1',
                  'access-list from_outside remark Perun generated rules will '
                  'follow',
                  'access-list from_outside extended permit object peruntcp80 '
                  'object-group perunObjectGroup0 object perunObject0',
                  'access-list from_outside remark Perun generated rules end',
                  'access-list from_outside extended deny ip any any',
                  'access-list from_Vlan701 remark Perun generated rules will '
                  'follow',
                  'access-list from_Vlan701 extended permit object peruntcp80 '
                  'object-group perunObjectGroup1 object perunObject1',
                  'access-list from_Vlan701 remark Perun generated rules end',
                  'access-list from_Vlan701 extended deny ip any any']}

        self.assertEqual(expected, rules)

    def test_merge_with_old_perun(self):
        self.cisco_parser.parse()

        self.assertEqual(self.cisco_parser.parsed_rule_sets[1].rule_set_name, "To_Vlan301")
        self.assertEqual(self.cisco_parser.parsed_rule_sets[1].rules[2].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[1].rules[3].rule_type, RuleType.CISCO)

        with open(os.path.join(self.this_folder,
                               './resources/inputTestingFromRealProductioncisco.json')) as json_file:
            perun_json_file = json.load(json_file)

        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)

        self.cisco_parser.deploy_policies(diff_rule_sets)

        with open(
                self.cisco_parser.exec_dir + 'roles/deploy-all-asa/vars/to_delete.yml') as yaml_file:
            rules = yaml.load(yaml_file, Loader=yaml.FullLoader)

        expected = {'delete_commands': ["no access-list To_Vlan301 extended permit ip object-group perunObjectGroup5 object perunObject0"]}
        self.assertEqual(expected, rules)


def test_merge_cisco_rule_set_with_perun_rule_set_difference(self):
    self.setUp()
    with open(os.path.join(self.this_folder,
                           './resources/inputTestingFromRealProductioncisco.json')) as json_file:
        perun_json_file = json.load(json_file)

    self.cisco_parser.parse()
    perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
    perun_parser.parse()
    merger = RuleSetMerger()
    diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
        self.cisco_parser.parsed_rule_sets,
        perun_parser.rule_sets,
        self.cisco_parser.applications_holder,
        self.cisco_parser.net_object_holder,
        perun_parser.net_object_holder)
    print(json.dumps(self.cisco_parser.abstract_to_policies(diff_rule_sets), indent=4,
                     sort_keys=False))
    print(json.dumps(self.cisco_parser.abstract_to_address_book(diff_net_obj_holder), indent=4,
                     sort_keys=False))


def test_merge_cisco_rule_set_with_perun_rule_set_merge(self):
    self.setUp()
    with open(os.path.join(self.this_folder,
                           './resources/inputTestingFromRealProductioncisco.json')) as json_file:
        perun_json_file = json.load(json_file)

    self.cisco_parser.parse()
    perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
    perun_parser.parse()
    merger = RuleSetMerger()
    diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_all(
        self.cisco_parser.parsed_rule_sets,
        perun_parser.rule_sets,
        self.cisco_parser.applications_holder,
        self.cisco_parser.net_object_holder,
        perun_parser.net_object_holder)
    print(json.dumps(self.cisco_parser.abstract_to_policies(diff_rule_sets), indent=4,
                     sort_keys=False))
    print(json.dumps(self.cisco_parser.abstract_to_address_book(diff_net_obj_holder), indent=4,
                     sort_keys=False))