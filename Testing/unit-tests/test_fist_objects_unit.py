import unittest

from Implementation.service_holder import service_holder, ServiceEnum


class NewFistObjectsTestCases(unittest.TestCase):

    def test_service_holder_find_by_service_name(self):
        ports = [20, 21, 22, 23, 25, 53, 67, 68, 69, 80, 88, 109, 110, 111
            , 123, 143, 161, 162, 179, 220, 389, 443, 445, 465, 500, 514, 993, 636]
        tcp = service_holder.find_by_name(ServiceEnum.TCP)
        udp = service_holder.find_by_name(ServiceEnum.UDP)
        parents = [tcp, tcp, tcp, tcp, tcp, tcp, udp, udp, udp, tcp, udp, tcp, tcp, tcp, udp, tcp,
                   udp, udp, tcp, tcp, tcp, tcp, tcp, tcp, udp, udp, tcp, tcp]
        counter = 0
        for service_name in ServiceEnum:
            if service_name == ServiceEnum.UNKNOWN or service_name == ServiceEnum.ANY:
                continue
            service = service_holder.find_by_name(service_name.value)
            if service.port_number is None:
                continue
            self.assertEqual(service.port_number, ports[counter])
            self.assertEqual(parents[counter] in service.parents, True)
            counter += 1

    def test_service_holder_find_by_port_number(self):
        ports = [20, 21, 22, 23, 25, 53, 67, 68, 69, 80, 88, 109, 110, 111
            , 123, 143, 161, 162, 179, 220, 389, 443, 445, 465, 500, 514, 993, 636]
        parents = [ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.TCP,
                   ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.UDP,
                   ServiceEnum.UDP, ServiceEnum.UDP, ServiceEnum.TCP, ServiceEnum.UDP,
                   ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.UDP, ServiceEnum.TCP,
                   ServiceEnum.UDP, ServiceEnum.UDP, ServiceEnum.TCP, ServiceEnum.TCP,
                   ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.TCP, ServiceEnum.TCP,
                   ServiceEnum.UDP, ServiceEnum.UDP, ServiceEnum.TCP, ServiceEnum.TCP]
        counter = 0
        for port in ports:
            service = service_holder.find_by_port_number(port)
            self.assertEqual(service.parents[0].name, parents[counter])
            self.assertEqual(service.port_number, ports[counter])
            counter += 1

        unknown = service_holder.find_by_port_number(8080)
        self.assertEqual(unknown.port_number, 8080)
        self.assertEqual(unknown.name, ServiceEnum.UNKNOWN)

