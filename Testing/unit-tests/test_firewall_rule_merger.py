import unittest
import os
import logging
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.firewall_rule_set import *
from Implementation.enums import ActionsEnum, RuleType
from Implementation.network_object_holder import *
from Implementation.service_holder import *


class FirewallRuleMergerTests(unittest.TestCase):
    this_folder = os.path.dirname(os.path.abspath(__file__))
    merger = None

    def setUp(self):
        logger = logging.getLogger('fist-test')
        logger.setLevel(logging.DEBUG)
        this_folder = os.path.dirname(os.path.abspath(__file__))
        self.merger = RuleSetMerger()

    def test_juniper_merge_rulesets_duplicity(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.JUNIPER)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.11"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.11"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="rule1", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        with self.assertRaisesRegex(Exception, 'DUPLICITY'):
            self.merger.merge_rule_sets_all(rule_sets1=[original], rule_sets2=[perun], applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

    def test_juniper_merge_rulesets_no_duplicity(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.JUNIPER)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.13"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.11"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="rule1", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        rule_sets1, applications_holder, net_object_holder1 = self.merger.merge_rule_sets_all(rule_sets1=[original], rule_sets2=[perun], applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)
        # this means 1 rule was added
        self.assertEqual(len(rule_sets1), 1)
        self.assertEqual(len(rule_sets1[0].rules), 4)

    def test_juniper_merge_rulesets_shadow(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.JUNIPER)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.13"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.11"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="rule1", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        with self.assertRaisesRegex(Exception, 'DUPLICITY'):
            rule_sets1, applications_holder, net_object_holder1 = self.merger.merge_rule_sets_all(rule_sets1=[original], rule_sets2=[perun], applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

    def test_cisco_merge_rulesets_no_duplicity(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=None,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        original2 = FirewallRuleSet(rule_set_name="original", from_object=None, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.13"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.13"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="ruleX", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        rule_sets1, applications_holder, net_object_holder1 = self.merger.merge_rule_sets_all(rule_sets1=[original, original2], rule_sets2=[perun], applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)
        # this means 1 rule was added
        self.assertEqual(len(rule_sets1), 2)
        self.assertEqual(len(rule_sets1[0].rules), 4)
        self.assertEqual(len(rule_sets1[1].rules), 4)


    def test_cisco_merge_rulesets_duplicity(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=None,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        original2 = FirewallRuleSet(rule_set_name="original", from_object=None, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.11"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.11"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="ruleX", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        with self.assertRaisesRegex(Exception, 'DUPLICITY'):
            self.merger.merge_rule_sets_all(rule_sets1=[original, original2], rule_sets2=[perun], applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

    def test_cisco_merge_rulesets_shadow(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=None,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        original2 = FirewallRuleSet(rule_set_name="original", from_object=None, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.12"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.12"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="ruleX", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        with self.assertRaisesRegex(Exception, 'DUPLICITY'):
            rule_sets1, applications_holder, net_object_holder1 = self.merger.merge_rule_sets_all(rule_sets1=[original, original2], rule_sets2=[perun],
                                        applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

    def test_cisco_merge_rulesets_shadow_app_action(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=None,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        original2 = FirewallRuleSet(rule_set_name="original", from_object=None, to_object=to_object,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.12"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.12"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="ruleX", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.DENY))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        with self.assertRaisesRegex(Exception, 'Unable to add a rule.*'):
            self.merger.merge_rule_sets_all(rule_sets1=[original, original2], rule_sets2=[perun],
                                        applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

    def test_cisco_merge_rulesets_shadow_app_different(self):
        app_holder = ApplicationHolder()
        from_object = ZoneObject("zone1", [IPAddress("10.1.1.1/24")])
        to_object = ZoneObject("zone2", [IPAddress("192.168.1.1/24")])
        obj_holder = NetObjectHolder()
        src_net_obj = NetObject("client", IPAddress("10.1.1.10/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server", IPAddress("192.168.1.10/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        application = Application(name=None, protocol=service_holder.find_by_port_number(22), service_protocol=22)
        app_holder.add_application(application)
        rule = FirewallRule(rule_name="rule1", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        src_net_obj = NetObject("client2", IPAddress("10.1.1.11/24"))
        obj_holder.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server2", IPAddress("192.168.1.11/24"))
        obj_holder.add_net_obj(dst_net_obj.name, dst_net_obj)
        rule2 = FirewallRule(rule_name="rule2", rule_set_name="original", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                            action=Action(ActionsEnum.PERMIT))

        application = Application(None, service_holder.find_by_name("ip"),
                                  service_holder.find_by_name("any"))
        app_holder.add_application(application)

        match_rule = MatchRule(AnyIP.ANYIP, AnyIP.ANYIP, application)
        implicit_deny = FirewallRule("implicit_deny", "original", match_rule, action=Action(ActionsEnum.DENY))

        original = FirewallRuleSet(rule_set_name="original", from_object=from_object, to_object=None,
                                   rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        original2 = FirewallRuleSet(rule_set_name="original", from_object=None, to_object=to_object,
                                    rules=[rule, rule2, implicit_deny], rule_type=RuleType.CISCO)

        application = Application(name=None, protocol=service_holder.find_by_port_number(80), service_protocol=22)
        obj_holder_perun = NetObjectHolder()
        src_net_obj = NetObject("client3", IPAddress("10.1.1.12"))
        obj_holder_perun.add_net_obj(src_net_obj.name, src_net_obj)
        dst_net_obj = NetObject("server3", IPAddress("192.168.1.12"))
        obj_holder_perun.add_net_obj(dst_net_obj.name, dst_net_obj)
        perun_rule = FirewallRule(rule_name="ruleX", rule_set_name="perun", match_rule=MatchRule(
            src_net_obj_name=src_net_obj.name, dst_net_obj_name=dst_net_obj.name, application=application),
                             action=Action(ActionsEnum.PERMIT))

        perun = FirewallRuleSet(rule_set_name="perun", from_object=from_object, to_object=to_object,
                                   rules=[perun_rule], rule_type=RuleType.NEW_PERUN)
        rule_sets1, applications_holder, net_object_holder1 = self.merger.merge_rule_sets_all(rule_sets1=[original, original2], rule_sets2=[perun],
                                        applications_holder=app_holder,
                                        net_object_holder1=obj_holder, net_object_holder2=obj_holder_perun)

        # this means 1 rule was added
        self.assertEqual(len(rule_sets1), 2)
        self.assertEqual(len(rule_sets1[0].rules), 4)
        self.assertEqual(len(rule_sets1[1].rules), 4)