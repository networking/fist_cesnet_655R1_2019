import copy
import json
import os
import pathlib
import unittest
import logging

from Implementation.firewall_cisco_parser import FirewallParserCisco
from Implementation.firewall_rule_set import IPAddress, FirewallRuleSet
from Implementation.enums import AnyIP, ActionsEnum, RuleType
from Implementation.firewall_rule_set_merger import RuleSetMerger
from Implementation.network_object_holder import NetObject, NetObjectGroup
from Implementation.perun_parser import PerunParser
from Implementation.service_holder import service_holder, Application, ServiceEnum


class FirewallCiscoTests(unittest.TestCase):
    this_folder = os.path.dirname(os.path.abspath(__file__))
    cisco_parser = None

    def setUp(self):
        logger = logging.getLogger('fist-test')
        logger.setLevel(logging.DEBUG)
        this_folder = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(this_folder, './resources/cisco_policies.json')) as json_file:
            self.cisco_policies = json.load(json_file)
        with open(os.path.join(this_folder, './resources/route-info.json')) as json_file:
            self.cisco_routes = json.load(json_file)
        with open(os.path.join(this_folder, './resources/address-book.json')) as json_file:
            self.cisco_address_book = json.load(json_file)
        with open(os.path.join(this_folder, './resources/zones.json')) as json_file:
            self.cisco_zones = json.load(json_file)

        exec_path = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'

        self.cisco_parser = FirewallParserCisco(self.cisco_policies, self.cisco_address_book,
                                                self.cisco_routes,
                                                self.cisco_zones,
                                                exec_path)

    def test_cisco_parse_policies(self):
        firewall_rule_set = self.cisco_parser.parse()
        # TODO opravit?
        network1 = NetObject("10.1.2.3/32", IPAddress("10.1.2.3/32"))
        network2 = NetObject("10.1.2.4/32", IPAddress("10.1.2.4/32"))
        network3 = NetObject("10.1.1.10/32", IPAddress("10.1.1.10/32"))
        network4 = NetObject("10.1.1.2/32", IPAddress("10.1.1.2/32"))
        network5 = NetObject("10.1.2.2/32", IPAddress("10.1.2.2/32"))
        network6 = NetObject("10.1.1.5/32", IPAddress("10.1.1.5/32"))
        network7 = NetObject("102.1.2.3/32", IPAddress("102.1.2.3/32"))
        network8 = NetObject("190.1.1.10/32", IPAddress("190.1.1.10/32"))
        network9 = NetObject("170.1.1.2/32", IPAddress("170.1.1.2/32"))
        network10 = NetObject("147.251.22.212/32", IPAddress("147.251.22.212/32"))
        network11 = NetObject("147.251.22.214/32", IPAddress("147.251.22.214/32"))
        network12 = NetObject("147.251.54.223/32", IPAddress("147.251.54.223/32"))
        network13 = NetObject("147.251.54.225/32", IPAddress("147.251.54.225/32"))
        network14 = NetObject("147.250.1.225/32", IPAddress("147.250.1.225/32"))
        obj0_0 = NetObjectGroup("obj0-0", ["10.1.2.3/32", "10.1.2.4/32"])
        obj0_1 = NetObjectGroup("obj0-1", ["10.1.1.10/32", "10.1.1.2/32"])
        obj1_0 = NetObjectGroup("obj1-0", ["10.1.2.2/32"])
        obj1_1 = NetObjectGroup("obj1-1", ["10.1.1.2/32", "10.1.1.5/32"])
        obj2_0 = NetObjectGroup("obj2-0", ["10.1.2.2/32", "10.1.2.3/32",
                                           "10.1.2.4/32"])
        obj2_1 = NetObjectGroup("obj2-1", ["10.1.1.10/32", "10.1.1.2/32",
                                           "10.1.1.5/32"])
        obj3_0 = NetObjectGroup("obj3-0", ["102.1.2.3/32", "10.1.2.4/32"])
        obj3_1 = NetObjectGroup("obj3-1", ["10.1.2.2/32"])
        obj3_2 = NetObjectGroup("obj3-2", ["190.1.1.10/32", "170.1.1.2/32",
                                           "10.1.1.5/32"])
        new_net_obj = NetObject(name="192.4.1.0/24",
                                address=IPAddress("192.4.1.0/24"))

        any_net_obj = [network1, network2, network3, network4, network5, network6, network7,
                       network8, network9,
                       network10, network11, network12, network13, network14, obj0_0, obj0_1,
                       obj1_0, obj1_1, obj2_0,
                       obj2_1, obj3_0, obj3_1, obj3_2, new_net_obj]

        """any_test_objects = {}
        for d in any_net_obj:
            for k, v in d.items():
                any_test_objects.setdefault(k, []).append(v)"""

        self.assertEqual(len(firewall_rule_set), 4)

        # AC1
        self.assertEqual(firewall_rule_set[0].rule_set_name, "AC1")
        self.assertEqual(firewall_rule_set[0].from_object.name, "dmz1")
        self.assertEqual(firewall_rule_set[0].from_object.ips[0], IPAddress("147.251.22.0/24"))
        self.assertEqual(firewall_rule_set[0].to_object, None)
        self.assertEqual(firewall_rule_set[0].rule_type, RuleType.CISCO)

        # rules AC1
        self.assertEqual(firewall_rule_set[0].rules[0].rule_name, "AC1")
        self.assertEqual(firewall_rule_set[0].rules[0].rule_set_name, "AC1")
        self.assertEqual(firewall_rule_set[0].rules[0].match_rule.src_net_obj_name, AnyIP.ANYIP)
        self.assertEqual(firewall_rule_set[0].rules[0].match_rule.dst_net_obj_name, AnyIP.ANYIP)
        self.assertEqual(str(firewall_rule_set[0].rules[0].match_rule.application.protocol),
                         "ip")
        self.assertEqual(
            firewall_rule_set[0].rules[0].match_rule.application.service_protocol.port_number, 0)
        self.assertEqual(firewall_rule_set[0].rules[0].action.action, ActionsEnum.DENY)
        # rules AC1-2
        self.assertEqual(firewall_rule_set[0].rules[1].rule_name, "AC1")
        self.assertEqual(firewall_rule_set[0].rules[1].rule_set_name, "AC1")
        # src
        self.assertEqual(firewall_rule_set[0].rules[1].match_rule.src_net_obj_name, network12.name)
        # dst
        self.assertEqual(firewall_rule_set[0].rules[1].match_rule.dst_net_obj_name, AnyIP.ANYIP)
        self.assertEqual(str(firewall_rule_set[0].rules[1].match_rule.application.protocol),
                         "udp")
        self.assertEqual(
            firewall_rule_set[0].rules[1].match_rule.application.service_protocol.port_number, 53)
        self.assertEqual(firewall_rule_set[0].rules[1].action.action, ActionsEnum.PERMIT)
        # rules AC1-3
        self.assertEqual(firewall_rule_set[0].rules[2].rule_name, "AC1")
        self.assertEqual(firewall_rule_set[0].rules[2].rule_set_name, "AC1")
        # src
        self.assertEqual(firewall_rule_set[0].rules[2].match_rule.src_net_obj_name, network12.name)
        # dst
        self.assertEqual(firewall_rule_set[0].rules[2].match_rule.dst_net_obj_name,
                         new_net_obj.name)
        self.assertEqual(str(firewall_rule_set[0].rules[2].match_rule.application.protocol),
                         "udp")
        self.assertEqual(
            firewall_rule_set[0].rules[2].match_rule.application.service_protocol.port_number, 22)
        self.assertEqual(firewall_rule_set[0].rules[2].action.action, ActionsEnum.PERMIT)
        # rules AC1-4
        self.assertEqual(firewall_rule_set[0].rules[3].rule_name, "AC1")
        self.assertEqual(firewall_rule_set[0].rules[3].rule_set_name, "AC1")
        # src
        self.assertEqual(firewall_rule_set[0].rules[3].match_rule.src_net_obj_name,
                         new_net_obj.name)
        # dst
        self.assertEqual(firewall_rule_set[0].rules[3].match_rule.dst_net_obj_name, AnyIP.ANYIP)
        self.assertEqual(str(firewall_rule_set[0].rules[3].match_rule.application.protocol),
                         "udp")
        self.assertEqual(str(firewall_rule_set[0].rules[3].match_rule.application.service_protocol),
                         "any")
        self.assertEqual(firewall_rule_set[0].rules[3].action.action, ActionsEnum.PERMIT)

        # AC2
        self.assertEqual(firewall_rule_set[1].rule_set_name, "AC2")
        self.assertEqual(firewall_rule_set[1].from_object.name, "inside")
        self.assertEqual(len(firewall_rule_set[1].from_object.ips), 2)
        self.assertEqual(firewall_rule_set[1].from_object.ips[0], IPAddress("10.1.1.0/24"))
        self.assertEqual(firewall_rule_set[1].to_object, None)
        self.assertEqual(firewall_rule_set[1].rule_type, RuleType.CISCO)

        # rules AC2
        self.assertEqual(len(firewall_rule_set[1].rules), 2)

        # rules AC2-1
        self.assertEqual(firewall_rule_set[1].rules[0].rule_name, "AC2")
        self.assertEqual(firewall_rule_set[1].rules[0].rule_set_name, "AC2")
        # src
        self.assertEqual(firewall_rule_set[1].rules[0].match_rule.src_net_obj_name, AnyIP.ANYIP)
        # dst
        self.assertEqual(
            firewall_rule_set[1].rules[0].match_rule.dst_net_obj_name,
            network3.name)
        self.assertEqual(firewall_rule_set[1].rules[0].match_rule.dst_net_obj_name,
                         network3.name)
        self.assertEqual(str(firewall_rule_set[1].rules[0].match_rule.application.protocol),
                         "tcp")
        self.assertEqual(str(firewall_rule_set[1].rules[0].match_rule.application.service_protocol),
                         "any")
        self.assertEqual(firewall_rule_set[1].rules[0].action.action, ActionsEnum.PERMIT)
        # rules AC2-2
        self.assertEqual(firewall_rule_set[1].rules[1].rule_name, "AC2")
        self.assertEqual(firewall_rule_set[1].rules[1].rule_set_name, "AC2")
        # src
        self.assertEqual(firewall_rule_set[1].rules[1].match_rule.src_net_obj_name,
                         new_net_obj.name)
        # dst
        self.assertEqual(firewall_rule_set[1].rules[1].match_rule.dst_net_obj_name,
                         new_net_obj.name)
        self.assertEqual(str(firewall_rule_set[1].rules[1].match_rule.application.protocol),
                         "tcp")
        self.assertEqual(
            firewall_rule_set[1].rules[1].match_rule.application.service_protocol.port_number, 4)
        self.assertEqual(firewall_rule_set[1].rules[1].action.action, ActionsEnum.DENY)

        # AC3
        self.assertEqual(firewall_rule_set[2].rule_set_name, "AC3")
        self.assertEqual(firewall_rule_set[2].from_object, None)
        self.assertEqual(firewall_rule_set[2].to_object.name, "management")
        self.assertEqual(len(firewall_rule_set[2].to_object.ips), 1)
        self.assertEqual(firewall_rule_set[2].to_object.ips[0], IPAddress("192.168.1.0/24"))
        self.assertEqual(firewall_rule_set[2].rule_type, RuleType.CISCO)

        # rules AC3
        self.assertEqual(len(firewall_rule_set[2].rules), 1)

        # rules AC3-1
        self.assertEqual(firewall_rule_set[2].rules[0].rule_name, "AC3")
        self.assertEqual(firewall_rule_set[2].rules[0].rule_set_name, "AC3")
        # src
        self.assertEqual(firewall_rule_set[2].rules[0].match_rule.src_net_obj_name,
                         new_net_obj.name)
        # dst
        self.assertEqual(firewall_rule_set[2].rules[0].
                         match_rule.dst_net_obj_name,
                         network6.name)
        """self.assertEqual(firewall_rule_set[2].rules[0].
                         match_rules.dst_net_obj.addresses[2].addresses[0].ip_address,
                         obj1_1.addresses[2].ip_address)"""
        self.assertEqual(str(firewall_rule_set[2].rules[0].match_rule.application.protocol),
                         "any")
        self.assertEqual(str(firewall_rule_set[2].rules[0].match_rule.application.service_protocol),
                         "any")
        self.assertEqual(firewall_rule_set[2].rules[0].action.action, ActionsEnum.DENY)

        # AC4
        self.assertEqual(firewall_rule_set[3].rule_set_name, "AC4")
        self.assertEqual(firewall_rule_set[3].from_object.name, "dmz2")
        self.assertEqual(len(firewall_rule_set[3].from_object.ips), 2)
        self.assertEqual(firewall_rule_set[3].from_object.ips[0], IPAddress("147.251.50.0/24"))
        self.assertEqual(firewall_rule_set[3].from_object.ips[1], IPAddress("147.251.54.0/24"))
        self.assertEqual(firewall_rule_set[3].to_object, None)
        self.assertEqual(firewall_rule_set[3].rule_type, RuleType.CISCO)

        # rules AC4
        self.assertEqual(len(firewall_rule_set[3].rules), 2)

        # rules AC4-1
        self.assertEqual(firewall_rule_set[3].rules[0].rule_name, "AC4")
        self.assertEqual(firewall_rule_set[3].rules[0].rule_set_name, "AC4")
        # src
        self.assertEqual(firewall_rule_set[3].rules[0].match_rule.src_net_obj_name, network12.name)
        # dst
        self.assertEqual(firewall_rule_set[3].rules[0].match_rule.dst_net_obj_name,
                         new_net_obj.name)
        self.assertEqual(str(firewall_rule_set[3].rules[0].match_rule.application.protocol),
                         "tcp")
        self.assertEqual(
            firewall_rule_set[3].rules[
                0].match_rule.application.service_protocol.name.value.lower(), "ssh")
        self.assertEqual(firewall_rule_set[3].rules[0].action.action, ActionsEnum.PERMIT)
        # rules AC4-2
        self.assertEqual(firewall_rule_set[3].rules[1].rule_name, "AC4")
        self.assertEqual(firewall_rule_set[3].rules[1].rule_set_name, "AC4")
        # src
        self.assertEqual(firewall_rule_set[3].rules[1].match_rule.src_net_obj_name, AnyIP.ANYIP)
        # dst
        self.assertEqual(firewall_rule_set[3].rules[0].match_rule.dst_net_obj_name,
                         new_net_obj.name)
        self.assertEqual(str(firewall_rule_set[3].rules[1].match_rule.application.protocol),
                         "tcp")
        self.assertEqual(
            firewall_rule_set[3].rules[1].match_rule.application.service_protocol.port_number, 105)
        self.assertEqual(firewall_rule_set[3].rules[1].action.action, ActionsEnum.DENY)

    def test_parse_group(self):
        group = 'access-group AC1 out interface inside'
        self.cisco_parser._FirewallParserCisco__parse_routes()

        name, zone_name, direction = self.cisco_parser._FirewallParserCisco__parse_group(group)
        self.assertEqual(name, "AC1")
        self.assertEqual(zone_name, "inside")
        self.assertEqual(direction, "out")

    def test_parse_group_error(self):
        group = 'access-group AC1 out interface'
        self.cisco_parser._FirewallParserCisco__parse_routes()

        thrown = False
        try:
            name, interface, direction = self.cisco_parser._FirewallParserCisco__parse_group(group)
        except:
            thrown = True

        self.assertTrue(thrown)

    def test_find_net_object(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        net_obj_name = self.cisco_parser.net_object_holder.find_net_object_name_by_ip_address_objects(
            IPAddress("10.1.2.3/32"))
        net_obj = self.cisco_parser.net_object_holder.find_net_obj_by_name(net_obj_name)
        # test net objects
        test_net_obj = NetObject(name="perunObj10.1.2.3slash32", address=IPAddress("10.1.2.3/32"))
        self.assertEqual(test_net_obj, net_obj)

    def test_find_net_obj_2(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        net_obj_name = self.cisco_parser.net_object_holder.find_net_object_name_by_ip_address_objects(
            IPAddress(
                "any"))
        net_obj = self.cisco_parser.net_object_holder.find_net_obj_by_name(net_obj_name)
        self.assertEqual(net_obj.address, IPAddress(AnyIP.ANYIP))

    def test_find_net_obj(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        net_obj_name = self.cisco_parser.net_object_holder.find_net_object_name_by_ip_address_objects(
            IPAddress("147.251.54.225/32"))
        net_obj = self.cisco_parser.net_object_holder.find_net_obj_by_name(net_obj_name)
        test_net_obj = NetObject(name="perunObj147.251.54.225slash32",
                                 address=IPAddress("147.251.54.225/32"))
        self.assertEqual(net_obj, test_net_obj)

    def test_get_list_ips_and_service_1(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'permit', 'tcp',
                         'any', 'host', '10.1.2.3', 'eq', '6633']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "any")
        self.assertEqual(destinatio_net_obj, "10.1.2.3/32")

        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)

        # service
        self.assertEqual(6633, service.service_protocol.port_number)
        self.assertEqual(ServiceEnum.TCP, service.protocol.name)
        self.assertEqual(ServiceEnum.UNKNOWN, service.service_protocol.name)

    def test_get_list_ips_and_service_2(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'any', 'any']

        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "any")
        self.assertEqual(destinatio_net_obj, "any")

        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)

        # service
        self.assertEqual(None, service.name)
        self.assertEqual("any", str(service.service_protocol))
        self.assertEqual("ip", str(service.protocol))

    def test_get_list_ips_and_service_3(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ["access-list", "AC1", "extended", "permit", "tcp", "host", "190.1.1.10",
                         "host",
                         "102.1.2.3", "eq", "ssh"]
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "190.1.1.10/32")
        self.assertEqual(destinatio_net_obj, "102.1.2.3/32")

        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)

        # service
        self.assertEqual("ssh", service.service_protocol.name.value.lower())
        self.assertEqual(None, service.name)

    def test_get_list_ips_and_service_4(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'host', '190.1.1.10', '192.4.2.0', '255.255.255.0', 'eq', 'ssh']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "190.1.1.10/32")
        self.assertEqual(destinatio_net_obj, "192.4.2.0/24")
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual("ssh", service.service_protocol.name.value.lower())
        print(service.name)
        self.assertEqual(None, service.name)

    def test_get_list_ips_and_service_5(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'any', '192.4.2.0', '255.255.255.0']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, AnyIP.ANYIP)
        self.assertEqual(destinatio_net_obj, "192.4.2.0/24")
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual(splited_group[4], str(service.protocol))

    def test_get_list_ips_and_service_6(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'host', '102.1.2.3', 'host', '190.1.1.10', 'eq', '8082']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "102.1.2.3/32")
        self.assertEqual(destinatio_net_obj, "190.1.1.10/32")
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual(8082, service.service_protocol.port_number)
        self.assertEqual(ServiceEnum.IP, service.protocol.name)
        self.assertEqual(None, service.name)

    def test_get_list_ips_and_service_7(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'host', '102.1.2.3', 'host', '147.251.54.225']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "102.1.2.3/32")
        self.assertEqual(destinatio_net_obj, "147.251.54.225/32")
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual("any", service.service_protocol.name.value.lower())

    def test_get_list_ips_and_service_8(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'host', '102.1.2.3', 'any', 'eq', '80']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "102.1.2.3/32")
        self.assertEqual(destinatio_net_obj, AnyIP.ANYIP)
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual(80, service.service_protocol.port_number)
        self.assertEqual(None, service.name)

    def test_get_list_ips_and_service_9(self):
        self.cisco_parser._FirewallParserCisco__parse_address_book()
        splited_group = ['access-list', 'AC3', 'extended', 'deny', 'ip',
                         'host', '102.1.2.3', 'any']
        protocol, source_net_obj, destinatio_net_obj, service = self.cisco_parser._FirewallParserCisco__get_protocol_src_dst_port_names(
            splited_group)
        self.assertEqual(source_net_obj, "102.1.2.3/32")
        self.assertEqual(destinatio_net_obj, AnyIP.ANYIP)
        service = self.cisco_parser._FirewallParserCisco__get_application(protocol, service)
        # service
        self.assertEqual("any", service.service_protocol.name.value.lower())

    def test_get_from_to_object_1(self):
        self.cisco_parser._FirewallParserCisco__parse_routes()
        from_object, to_object = self.cisco_parser._FirewallParserCisco__get_from_to_object("dmz1", "in")

        self.assertEqual(to_object, None)
        self.assertEqual(from_object.name, "dmz1")
        self.assertEqual(len(from_object.ips), 1)
        self.assertEqual(from_object.ips[0], IPAddress("147.251.22.0/24"))

    def test_get_from_to_object_2(self):
        self.cisco_parser._FirewallParserCisco__parse_routes()
        from_object, to_object = self.cisco_parser._FirewallParserCisco__get_from_to_object("dmz1", "out")
        self.assertEqual(from_object, None)
        self.assertEqual(len(to_object.ips), 1)
        self.assertEqual(to_object.ips[0], IPAddress("147.251.22.0/24"))

    def test_parse_routes(self):
        self.cisco_parser._FirewallParserCisco__parse_routes()

        self.assertEqual(len(self.cisco_parser._zones_objects), 5)

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("inside").ips), 2)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("inside").ips[0],
                         IPAddress("10.1.1.0/24"))

        '''L route types are skipped in parse_routes() so they cannot be in result dict'''
        '''self.assertEqual(self.cisco_parser.routes_holder.routes_dict["inside"].ip[1].ip_address,
                         IPAddress("10.1.1.10/32").ip_address)'''

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("outside").ips), 1)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("outside").ips[0],
                         IPAddress("10.1.2.0/24"))
        '''
        self.assertEqual(self.cisco_parser.routes_holder.routes_dict["outside"].ip[1].ip_address,
                         IPAddress("10.1.2.10/32").ip_address)'''

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("dmz1").ips), 1)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("dmz1").ips[0],
                         IPAddress("147.251.22.0/24"))

        '''self.assertEqual(self.cisco_parser.routes_holder.routes_dict["dmz1"].ip[1].ip_address,
                         IPAddress("147.251.22.2/32").ip_address)'''

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("dmz2").ips), 2)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("dmz2").ips[0],
                         IPAddress("147.251.50.0/24"))
        '''self.assertEqual(self.cisco_parser.routes_holder.routes_dict["dmz2"].ip[1].ip_address,
                         IPAddress("147.251.50.2/32").ip_address)'''
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("dmz2").ips[1],
                         IPAddress("147.251.54.0/24"))

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("management").ips), 1)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("management").ips[0],
                         IPAddress("192.168.1.0/24"))
        '''self.assertEqual(self.cisco_parser.routes_holder.routes_dict["management"].ip[1].ip_address,
                         IPAddress("192.168.1.1/32").ip_address)'''

    def test_get_name_interface_direction_from_rule_set_with_none_objects(self):
        thrown = False
        try:
            acl_direction, acl_interfaces, acl_name = self.cisco_parser._FirewallParserCisco__get_name_interface_direction_from_rule_set(FirewallRuleSet("", None, None, None, None))
        except:
            thrown = True
        self.assertTrue(thrown)

    def test_without_gateway_msg(self):
        this_folder = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(this_folder,
                               './resources/route-info-without-gateway-msg.json')) as json_file:
            self.cisco_routes = json.load(json_file)

        self.cisco_parser = FirewallParserCisco(self.cisco_policies, self.cisco_address_book,
                                                self.cisco_routes, self.cisco_zones,
                                                'Implementation/manage-fw/roles/deploy-all-srx/vars')

        self.cisco_parser._FirewallParserCisco__parse_routes()

        self.assertEqual(len(self.cisco_parser._zones_objects), 4)

        self.assertEqual(len(self.cisco_parser._FirewallParserCisco__get_zone_by_name("inside").ips), 1)
        self.assertEqual(self.cisco_parser._FirewallParserCisco__get_zone_by_name("inside").ips[0],
                         IPAddress("10.1.1.0/24"))

    def test_empty_address_book(self):
        # Testing if empty address book is parsed without issue
        self.cisco_parser = FirewallParserCisco(self.cisco_policies, [[''], ['']],
                                                self.cisco_routes, self.cisco_zones,
                                                'Implementation/manage-fw/roles/deploy-all-srx/vars')
        self.cisco_parser._FirewallParserCisco__parse_address_book()

    def test_parse_firewall_rule_set(self):
        cisco_policies = [["access-group AC1 in interface dmz1",
                           "access-group AC2 in interface inside",
                           "access-group AC3 out interface management",
                           "access-group AC4 in interface dmz2"],
                          ["access-list AC1 extended deny ip any any eq 0",
                           "access-list AC1 extended permit udp host 147.251.54.223 any eq 53",
                           "access-list AC1 extended permit udp host 147.251.54.223 192.4.1.0 255.255.255.0 eq 22",
                           "access-list AC1 extended permit udp 192.4.1.0 255.255.255.0 any",
                           "access-list AC2 extended permit tcp any host 10.1.1.10",
                           "access-list AC2 extended deny tcp 192.4.1.0 255.255.255.0 192.4.1.0 255.255.255.0 eq 4 log",
                           "access-list AC3 extended deny any 192.4.1.0 255.255.255.0 host 10.1.1.5",
                           "access-list AC4 extended permit tcp host 147.251.54.223 192.4.1.0 255.255.255.0 eq 22",
                           "access-list AC4 extended deny tcp any 192.4.1.0 255.255.255.0 eq 105"]]

        address_book = [["object network perunObj10.1.2.3slash32", "host 10.1.2.3",
                         "object network perunObj10.1.2.4slash32", "host 10.1.2.4",
                         "object network perunObj10.1.1.10slash32", "host 10.1.1.10",
                         "object network perunObj10.1.1.2slash32", "host 10.1.1.2",
                         "object network perunObj10.1.2.2slash32", "host 10.1.2.2",
                         "object network perunObj10.1.1.5slash32", "host 10.1.1.5",
                         "object network perunObj102.1.2.3slash32", "host 102.1.2.3",
                         "object network perunObj190.1.1.10slash32", "host 190.1.1.10",
                         "object network perunObj170.1.1.2slash32", "host 170.1.1.2",
                         "object network perunObj147.251.22.212slash32", "host 147.251.22.212",
                         "object network perunObj147.251.22.214slash32", "host 147.251.22.214",
                         "object network perunObj147.251.54.223slash32", "host 147.251.54.223",
                         "object network perunObj147.251.54.225slash32", "host 147.251.54.225",
                         "object network perunObj147.250.1.225slash32", "host 147.250.1.225",
                         "object service peruntcp22", "service tcp destination eq 22",
                         "object service peruntcp8082", "service tcp destination eq 8082",
                         "object service peruntcp6633", "service tcp destination eq 6633"],
                        ["object-group network obj0-0",
                         "network-object object perunObj10.1.2.3slash32",
                         "network-object object perunObj10.1.2.4slash32",
                         "object-group network obj0-1",
                         "network-object object perunObj10.1.1.10slash32",
                         "network-object object perunObj10.1.1.2slash32",
                         "object-group network obj1-0",
                         "network-object object perunObj10.1.2.2slash32",
                         "object-group network obj1-1",
                         "network-object object perunObj10.1.1.2slash32",
                         "network-object object perunObj10.1.1.5slash32",
                         "object-group network obj2-0",
                         "network-object object perunObj10.1.2.2slash32",
                         "network-object object perunObj10.1.2.3slash32",
                         "network-object object perunObj10.1.2.4slash32",
                         "object-group network obj2-1",
                         "network-object object perunObj10.1.1.10slash32",
                         "network-object object perunObj10.1.1.2slash32",
                         "network-object object perunObj10.1.1.5slash32",
                         "object-group network obj3-0",
                         "network-object object perunObj102.1.2.3slash32",
                         "network-object object perunObj10.1.2.4slash32",
                         "object-group network obj3-1",
                         "network-object object perunObj10.1.2.2slash32",
                         "object-group network obj3-2",
                         "network-object object perunObj190.1.1.10slash32",
                         "network-object object perunObj170.1.1.2slash32",
                         "network-object object perunObj10.1.1.5slash32"]]

        self.cisco_parser.parse()
        test_cisco_policies, test_cisco_address_book = self.cisco_parser.\
           _FirewallParserCisco__parse_cisco_rule_set_to_policies_and_address_book()

        self.assertEqual(test_cisco_policies, cisco_policies)
        self.assertEqual(test_cisco_address_book, address_book)

        if __name__ == '__main__':
            unittest.main()

    def test_merge_cisco_rule_set_with_perun_rule_set(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/perun_rules_testing.json')) as json_file:
            perun_json_file = json.load(json_file)
        with open(os.path.join(self.this_folder,
                               './resources/expected_cisco_merged_policies.json')) as json_file:
            expected_policies = json.load(json_file)

        original_rule_set = copy.deepcopy(self.cisco_parser.parse())
        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        merged_rule_sets, application_holder, net_object_holder = merger.merge_rule_sets_all(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)

        # check merged_rule_sets
        policies = self.cisco_parser.abstract_to_policies(merged_rule_sets)
        self.assertEqual(policies[0], expected_policies[0])
        self.assertEqual(policies[1], expected_policies[1])

        # check merged_net_object_holder
        for rule_set in perun_parser.rule_sets:
            for rule in rule_set.rules:
                self.assertIsNotNone(net_object_holder.find_net_obj_by_name(rule.match_rule.src_net_obj_name))
                self.assertIsNotNone(net_object_holder.find_net_obj_by_name(rule.match_rule.dst_net_obj_name))


        # check application_holder
        for rule_set in perun_parser.rule_sets:
            for rule in rule_set.rules:
                self.assertIsNotNone(self.cisco_parser.applications_holder
                              .find_application_or_application_group_by_name(rule.match_rule.application.name))

        address_book = self.cisco_parser.abstract_to_address_book(net_object_holder,
                                                                  application_holder)
        print(json.dumps(policies, indent=4,
                         sort_keys=False))
        print(json.dumps(address_book, indent=4,
                         sort_keys=False))
        # self.cisco_parser.deploy_policies(merged_rule_set)

    def test_merge_cisco_rule_set_with_perun_rule_set_difference(self):
        self.setUp()
        with open(os.path.join(self.this_folder,
                               './resources/perun_rules_testing.json')) as json_file:
            perun_json_file = json.load(json_file)

        self.cisco_parser.parse()
        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)

        # check merged_rule_sets
        policies = self.cisco_parser.abstract_to_policies(diff_rule_sets)

        self.assertEqual(len(diff_rule_sets), 4)
        self.assertEqual(len(diff_rule_sets[0].rules), 4)
        self.assertEqual(len(diff_rule_sets[1].rules), 3)
        self.assertEqual(len(diff_rule_sets[2].rules), 5)
        self.assertEqual(len(diff_rule_sets[3].rules), 3)
        self.assertEqual(len(diff_app_holder.get_applications()), 0)
        self.assertEqual(len(diff_app_holder.get_applications_groups()), 0)
        self.assertEqual(len(diff_net_obj_holder.get_net_objects()), 2)
        self.assertEqual(len(diff_net_obj_holder.get_net_object_groups()), 1)
        self.assertEqual(len(diff_net_obj_holder.get_tmp_net_objects()), 0)

        self.cisco_parser.deploy_policies(diff_rule_sets)
        self.cisco_parser.deploy_services(diff_app_holder)
        self.cisco_parser.deploy_net_objects(diff_net_obj_holder)
        # self.cisco_parser.deploy_policies(merged_rule_set)

    def test_merge_with_old_perun(self):
        exec_path = str(pathlib.Path(__file__).parent.absolute()) + '/manage-fw/'
        this_folder = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(this_folder, './resources/cisco_policies_new.json')) as json_file:
            self.cisco_policies = json.load(json_file)
        with open(os.path.join(this_folder, './resources/address-book_new.json')) as json_file:
            self.cisco_address_book = json.load(json_file)

        self.cisco_parser = FirewallParserCisco(self.cisco_policies, self.cisco_address_book,
                                                self.cisco_routes,
                                                self.cisco_zones,
                                                exec_path)
        self.cisco_parser.parse()

        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rule_set_name, "from_inside")
        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rules[0].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rules[1].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rules[2].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rules[3].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[0].rules[4].rule_type, RuleType.CISCO)

        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rule_set_name, "AC3")
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[0].rule_type, RuleType.CISCO)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[1].rule_type, RuleType.CISCO)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[2].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[3].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[4].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[5].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[3].rules[6].rule_type, RuleType.CISCO)

        self.assertEqual(self.cisco_parser.parsed_rule_sets[2].rule_set_name, "AC2")
        self.assertEqual(self.cisco_parser.parsed_rule_sets[2].rules[0].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[2].rules[1].rule_type, RuleType.OLD_PERUN)
        self.assertEqual(self.cisco_parser.parsed_rule_sets[2].rules[2].rule_type, RuleType.CISCO)

        with open(os.path.join(self.this_folder,
                               './resources/perun_rules_testing.json')) as json_file:
            perun_json_file = json.load(json_file)

        perun_parser = PerunParser(perun_json_file, self.cisco_parser._zones_objects)
        perun_parser.parse()
        merger = RuleSetMerger()
        diff_rule_sets, diff_app_holder, diff_net_obj_holder = merger.merge_rule_sets_perun(
            self.cisco_parser.parsed_rule_sets,
            perun_parser.rule_sets,
            self.cisco_parser.applications_holder,
            self.cisco_parser.net_object_holder,
            perun_parser.net_object_holder)
